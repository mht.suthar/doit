package myapp.com.doit.prelogin.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.WindowManager;
import android.widget.LinearLayout;

import myapp.com.doit.AdminDashboardActivity;
import myapp.com.doit.R;


public class Signup extends AppCompatActivity {

    LinearLayout btn_Login,login;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);


        btn_Login = (LinearLayout) findViewById(R.id.btn_Login);
        btn_Login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent it = new Intent(Signup.this, AdminDashboardActivity.class);
                it.putExtra("keyName", "0");
                startActivity(it);

            }
        });


        login  = (LinearLayout) findViewById(R.id.login);
        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent it = new Intent(Signup.this, Login.class);
                it.putExtra("keyName", "0");
                startActivity(it);

            }
        });


    }

//    @Override
//    public void onBackPressed() {
//
//        moveTaskToBack(true);
//        android.os.Process.killProcess(android.os.Process.myPid());
//        System.exit(1);
//
//        finish();
//
//    }
}
