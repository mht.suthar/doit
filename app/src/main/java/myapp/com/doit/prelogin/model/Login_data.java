package myapp.com.doit.prelogin.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by zero on 11/11/16.
 */
public class Login_data {

    @SerializedName("UserLoginName")
    private String UserLoginName;

    @SerializedName("Password")
    private String Password;


    public Login_data(String userLoginName, String password) {
        UserLoginName = userLoginName;
        Password = password;
    }


    public String getUserLoginName() {
        return UserLoginName;
    }

    public void setUserLoginName(String userLoginName) {
        UserLoginName = userLoginName;
    }

    public String getPassword() {
        return Password;
    }

    public void setPassword(String password) {
        Password = password;
    }
}
