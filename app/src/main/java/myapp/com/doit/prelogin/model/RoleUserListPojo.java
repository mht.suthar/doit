package myapp.com.doit.prelogin.model;

import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RoleUserListPojo {

    @SerializedName("RoleList")
    @Expose
    private List<RoleList> roleList = null;
    @SerializedName("RoleID")
    @Expose
    private Integer roleID;

    public List<RoleList> getRoleList() {
        return roleList;
    }

    public void setRoleList(List<RoleList> roleList) {
        this.roleList = roleList;
    }

    public Integer getRoleID() {
        return roleID;
    }

    public void setRoleID(Integer roleID) {
        this.roleID = roleID;
    }

    public class RoleList {

        @SerializedName("RoleIDP")
        @Expose
        private Integer roleIDP;
        @SerializedName("RoleName")
        @Expose
        private String roleName;

        public Integer getRoleIDP() {
            return roleIDP;
        }

        public void setRoleIDP(Integer roleIDP) {
            this.roleIDP = roleIDP;
        }

        public String getRoleName() {
            return roleName;
        }

        public void setRoleName(String roleName) {
            this.roleName = roleName;
        }

    }

}