package myapp.com.doit.prelogin.ui;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.AppCompatSpinner;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import myapp.com.doit.AdminDashboardActivity;
import myapp.com.doit.R;
import myapp.com.doit.base.BaseActivity;
import myapp.com.doit.engineer.ui.EngineerDashboardAct;
import myapp.com.doit.helper.LoginData;
import myapp.com.doit.prelogin.model.RolePojo;
import myapp.com.doit.prelogin.model.RoleUserListPojo;
import myapp.com.doit.utils.PrefUtils;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedByteArray;


public class Login extends BaseActivity {

    TextView signup;
    AppCompatSpinner mSpnrRole;
    LinearLayout btn_Login;
    String status, message;
    private ProgressDialog loading;
    EditText mobileno, password;
    String f_mobileno, f_password,f_Role_Id, mRoleName;
    private static final String TAG = "Login";
    private List<RolePojo> mRoleList = new ArrayList<>();
    private  List<RoleUserListPojo.RoleList> mRoleUserList = new ArrayList<>();

    @Override
    protected void initView() {
        mobileno = (EditText) findViewById(R.id.mobileno);
        mSpnrRole = (AppCompatSpinner) findViewById(R.id.spnr_role);
        password = (EditText) findViewById(R.id.password);
        signup = (TextView) findViewById(R.id.signup);
        btn_Login = (LinearLayout) findViewById(R.id.btn_Login);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        initView();

        setListeners();

        GetRol();
    }

    private void setListeners() {
        signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent it = new Intent(Login.this, Signup.class);
                it.putExtra("keyName", "0");
                startActivity(it);

            }
        });

        btn_Login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

//                Intent it = new Intent(Login.this, AdminDashboardActivity.class);
//                it.putExtra("keyName", "0");
//                startActivity(it);

                f_mobileno = mobileno.getText().toString();
                f_password = password.getText().toString();

                if (f_mobileno.equalsIgnoreCase("") || f_mobileno.equalsIgnoreCase(null)) {
                    Toast.makeText(Login.this, "Please enter User Name", Toast.LENGTH_SHORT).show();
                } else if (f_password.equalsIgnoreCase("") || f_password.equalsIgnoreCase(null)) {
                    Toast.makeText(Login.this, "Please enter Password", Toast.LENGTH_SHORT).show();
                } else {
                    SendLogin();
                }
                SendLogin();
            }
        });

        mobileno.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) { }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) { }

            @Override
            public void afterTextChanged(Editable editable) {
                GetUserRol();
            }
        });

        mSpnrRole.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int pos, long l) {
                Log.e(TAG, "onItemSelected: use role "+mRoleUserList.size()+"   role " +mRoleList.size());
                if(mRoleUserList.size() > 0){
                    f_Role_Id = ""+mRoleUserList.get(pos).getRoleIDP();
                    mRoleName = ""+mRoleUserList.get(pos).getRoleName();
                }else{
                    f_Role_Id = ""+mRoleList.get(pos).getRoleId();
                    mRoleName = ""+mRoleList.get(pos).getRoleName();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    private void GetUserRol() {
        getRestrofitInterface().GetUserRoleList(""+mobileno.getText().toString().trim(), new Callback<Response>() {
            @Override
            public void success(Response findChildArray, Response response) {
                String result = new String(((TypedByteArray) findChildArray.getBody()).getBytes());
                try {
                    RoleUserListPojo posts = new Gson().fromJson(result, RoleUserListPojo.class);
                    mRoleUserList.clear();
                    if(posts.getRoleList() != null)
                        mRoleUserList = posts.getRoleList();
                    setUserRoleAdapter();
                }catch (Exception e){
                    e.printStackTrace();
                }
            }

            @Override
            public void failure(RetrofitError error) {
                Log.e("error", error.getMessage());
            }
        });
    }

    private void setUserRoleAdapter(){
        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(
                this, android.R.layout.simple_spinner_item, getRole());
        spinnerArrayAdapter.setDropDownViewResource( android.R.layout.simple_spinner_dropdown_item );
        mSpnrRole.setAdapter(spinnerArrayAdapter);
    }

    private ArrayList<String> getRole() {
        ArrayList<String> list = new ArrayList<>();
        if(mRoleUserList.size() > 0){
            for (int i = 0; i < mRoleUserList.size(); i++) {
                list.add(mRoleUserList.get(i).getRoleName());
            }
        }else {
            for (int i = 0; i < mRoleList.size(); i++) {
                list.add(mRoleList.get(i).getRoleName());
            }
        }
        return list;
    }


    private void SendLogin() {
        try {
            loading = new ProgressDialog(Login.this);
            loading.setMessage("Please Wait Loading data ....");
            loading.show();
            loading.setCancelable(false);

        } catch (Exception e) {

        }

        JSONObject paramObject = new JSONObject();
        try {
            paramObject.put("UserLoginName", f_mobileno);
            paramObject.put("Password", f_password);
            paramObject.put("RoleIDF", f_Role_Id);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.e("Jon :======  ", "" + paramObject.toString());
        getRestrofitInterface().Get_normalLogin(new LoginData(f_mobileno, f_password,f_Role_Id), new Callback<Response>() {
            @Override
            public void success(Response findChildArray, Response response) {
                //  Log.e("status&&&", "stat" + response.getReason() + "response=" + response.getUrl() + "status" + response.getStatus());
                Log.e("status", new String(((TypedByteArray) findChildArray.getBody()).getBytes()));
                String result = new String(((TypedByteArray) findChildArray.getBody()).getBytes());
                JSONObject jObj = null;
                try {
                    jObj = new JSONObject(result);
                    status = jObj.getString("IsSuccess");
                    message = jObj.getString("ErrorMessage");
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                if (status.equals("true")) {
                    try {
                        String User_Id = jObj.getString("User_Id");
                        String Employee_IDF = jObj.getString("Employee_IDF");
                        String RoleId = jObj.getString("RoleId");
                        String Created_By = jObj.getString("Created_By");

                        PrefUtils.set_User_Id(User_Id, Login.this);
                        PrefUtils.set_Employee_IDF(Employee_IDF, Login.this);
                        PrefUtils.set_RoleId(RoleId, Login.this);
                        PrefUtils.set_Created_By(Created_By, Login.this);
                        PrefUtils.setRoleName(mRoleName, Login.this);

                        Intent it = null;
                        Log.e("Role Name", ""+mRoleName);
                        if(mRoleName.equalsIgnoreCase("Client"))
                            it = new Intent(Login.this, AdminDashboardActivity.class);
                        else if(mRoleName.equalsIgnoreCase("Engineer"))
                            it = new Intent(Login.this, EngineerDashboardAct.class);
                        else
                            it = new Intent(Login.this, EngineerDashboardAct.class);

                        it.putExtra("keyName", "0");
                        startActivity(it);
                        finish();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    Toast.makeText(Login.this, message, Toast.LENGTH_SHORT).show();
                }
                loading.dismiss();
            }

            @Override
            public void failure(RetrofitError error) {
                loading.dismiss();
                String merror = error.getMessage();
                Log.d("error", merror);
                Toast.makeText(Login.this, merror, Toast.LENGTH_LONG).show();

            }
        });


    }

    private void GetRol() {
        getRestrofitInterface().Get_RoleList(new Callback<Response>() {

            @Override
            public void success(Response findChildArray, Response response) {
                //  Log.e("status&&&", "stat" + response.getReason() + "response=" + response.getUrl() + "status" + response.getStatus());

                Log.e("status", new String(((TypedByteArray) findChildArray.getBody()).getBytes()));

                String result = new String(((TypedByteArray) findChildArray.getBody()).getBytes());


                JSONArray jObj = null;

                try {
                    Type listType = new TypeToken<List<RolePojo>>(){}.getType();
                    List<RolePojo> posts = new Gson().fromJson(result, listType);
                    mRoleList.clear();
                    mRoleList = posts;
                    setUserRoleAdapter();
                    Log.e(TAG, "success: size "+posts.size());

                    /*jObj = new JSONArray(result);


                    if (jObj.length() > 0) {

                        PrefUtils.set_RoleID("",Login.this);

                        for (int i = 0; i < jObj.length(); i++) {

                            JSONObject jsonObj = jObj.getJSONObject(i);

                            String Role_Id = jsonObj.getString("Role_Id");
                            String Role_name = jsonObj.getString("Role_name");


                            if(Role_name.equalsIgnoreCase("Client")){

                                Log.e("Role_Id :====  ",""+Role_Id);

                                PrefUtils.set_RoleID(Role_Id,Login.this);

                                f_Role_Id = ""+Role_Id;
                            }
                        }
                    } else {
                        Toast.makeText(Login.this, message, Toast.LENGTH_SHORT).show();
                    }
*/

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void failure(RetrofitError error) {
                String merror = error.getMessage();
                Log.d("error", merror);
                Toast.makeText(Login.this, merror, Toast.LENGTH_LONG).show();
            }
        });
    }
}
