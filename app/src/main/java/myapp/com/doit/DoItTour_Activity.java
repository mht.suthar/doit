package myapp.com.doit;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import myapp.com.doit.base.BaseActivity;
import myapp.com.doit.helper.DoItTour_List;
import myapp.com.doit.helper.DoItTour_List_Array;
import myapp.com.doit.utils.PrefUtils;
import myapp.com.doit.rest.RetrfitInterface;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedByteArray;


public class DoItTour_Activity extends BaseActivity {

    LinearLayout btn_Login, login;
    String status, message;
    private ProgressDialog loading;
    ListView list_view;
    ImageView left_arrow;
    TextView title_;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.complaint);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        initView();


        left_arrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });


        Get_ServiceList();
    }

//    @Override
//    public void onBackPressed() {
//
//        moveTaskToBack(true);
//        android.os.Process.killProcess(android.os.Process.myPid());
//        System.exit(1);
//
//        finish();
//
//    }


    private void Get_ServiceList() {
        try {
            loading = new ProgressDialog(DoItTour_Activity.this);
            loading.setMessage("Please Wait Loading data ....");
            loading.show();
            loading.setCancelable(false);

        } catch (Exception e) {

        }
        getRestrofitInterface().Get_DoItTour(new Callback<Response>()
        {
            @Override
            public void success(Response findChildArray, Response response) {

                Log.e("status", new String(((TypedByteArray) findChildArray.getBody()).getBytes()));

                String result1 = new String(((TypedByteArray) findChildArray.getBody()).getBytes());

                JSONArray jObj = null;
                try {
                    jObj = new JSONArray(result1);

                    Log.e("Size", "" + jObj.length());


                    if (jObj.length() > 0) {
                        ArrayList<DoItTour_List> list = new ArrayList<>();
                        for (int i = 0; i < jObj.length(); i++) {

                            JSONObject jsonObj = jObj.getJSONObject(i);

                            String FAQ_Name = jsonObj.getString("FAQ_Name");
                            String Description = jsonObj.getString("Description");

                            DoItTour_List_Array doItTour_List_Array = new DoItTour_List_Array();
                            doItTour_List_Array.category_List = new ArrayList<DoItTour_List>();

                            DoItTour_List doItTour_List = new DoItTour_List(FAQ_Name,Description);
                            list.add(doItTour_List);

                            doItTour_List_Array.category_List.add(doItTour_List);
                            PrefUtils.set_doItTour_List_Array(doItTour_List_Array, DoItTour_Activity.this);

                            /*if (PrefUtils.get_doItTour_List_Array(DoItTour_Activity.this) != null) {

                                DoItTour_List_Array doItTour_List_Array = PrefUtils.get_doItTour_List_Array(DoItTour_Activity.this);
                                DoItTour_List doItTour_List = new DoItTour_List(FAQ_Name,Description);
                                doItTour_List_Array.getCategory_List().add(doItTour_List);
                                PrefUtils.set_doItTour_List_Array(doItTour_List_Array, DoItTour_Activity.this);

                            } else {

                                DoItTour_List_Array doItTour_List_Array = new DoItTour_List_Array();
                                doItTour_List_Array.category_List = new ArrayList<DoItTour_List>();
                                DoItTour_List doItTour_List = new DoItTour_List(FAQ_Name,Description);
                                doItTour_List_Array.category_List.add(doItTour_List);
                                PrefUtils.set_doItTour_List_Array(doItTour_List_Array, DoItTour_Activity.this);

                            }*/


                        }


                        //DoItTour_List_Array doItTour_List_Array = PrefUtils.get_doItTour_List_Array(DoItTour_Activity.this);

                        Adapter_Service adapter_Service = new Adapter_Service(DoItTour_Activity.this, list);

                        list_view.setAdapter(adapter_Service);


                    } else {


                        Toast.makeText(DoItTour_Activity.this, "Username or Password Incorrect", Toast.LENGTH_SHORT).show();

                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }

                loading.dismiss();

            }

            @Override
            public void failure(RetrofitError error) {

                String merror = error.getMessage();
                Log.d("error", merror);
                Toast.makeText(DoItTour_Activity.this, merror, Toast.LENGTH_LONG).show();

            }
        });


    }

    @Override
    protected void initView() {
        list_view  = (ListView) findViewById(R.id.list_view);
        left_arrow = (ImageView) findViewById(R.id.left_arrow);
        title_ = (TextView) findViewById(R.id.title_);
        title_.setText("DoItTour");
    }


    public class Adapter_Service extends BaseAdapter {


        ArrayList<DoItTour_List> result;
        Context context;

        private LayoutInflater inflater = null;

        public Adapter_Service(DoItTour_Activity second, ArrayList<DoItTour_List> details) {
            result = details;
            context = second;
        }



        /*private view holder class*/
        private class ViewHolder {

            TextView txt_Code, txt_Date,txt_ItemDesc, txtTitle, txtDesc;



        }

        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder holder = null;

            LayoutInflater mInflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);

            if (convertView == null) {
                convertView = mInflater.inflate(R.layout.noti_row, null);
                holder = new ViewHolder();

                holder.txt_Code = (TextView) convertView.findViewById(R.id.txt_Code);
                holder.txt_Date = (TextView) convertView.findViewById(R.id.txt_Date);
                holder.txt_ItemDesc = (TextView) convertView.findViewById(R.id.txt_ItemDesc);
                holder.txtDesc = (TextView) convertView.findViewById(R.id.txt_desc);
                holder.txtTitle = (TextView) convertView.findViewById(R.id.txt_title);
                holder.txtTitle.setText("FAQ_Name");
                holder.txtDesc.setText("Description");


                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }


            holder.txt_Code.setText(result.get(position).getFAQ_Name());
            holder.txt_Date.setText(result.get(position).getDescription());
            //holder.txt_ItemDesc.setText(result.get(position).getItemDesc());




            return convertView;
        }

        @Override
        public int getCount() {
            return result.size();
        }

        @Override
        public Object getItem(int position) {
            return result.get(position);
        }

        @Override
        public long getItemId(int position) {
            return result.indexOf(getItem(position));
        }
    }


}
