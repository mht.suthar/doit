package myapp.com.doit;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.GsonBuilder;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;

import myapp.com.doit.dashboard.ui.EnquiryListAct;
import myapp.com.doit.helper.Custmar_List;
import myapp.com.doit.helper.Custmar_List_Array;
import myapp.com.doit.helper.Enquiry_type_List;
import myapp.com.doit.helper.Enquiry_type_List_Array;
import myapp.com.doit.helper.MachineDetailList_List_Array;
import myapp.com.doit.helper.Ms_List_Array;
import myapp.com.doit.helper.Mslist_List;
import myapp.com.doit.utils.PrefUtils;
import myapp.com.doit.helper.Problem_type_List;
import myapp.com.doit.helper.Problem_type_List_Array;
import myapp.com.doit.helper.RM_List;
import myapp.com.doit.helper.RM_List_Array;
import myapp.com.doit.rest.RetrfitInterface;
import myapp.com.doit.helper.Sub_type_List;
import myapp.com.doit.helper.Sub_type_List_Array;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedByteArray;


public class Addnew_Enqury extends AppCompatActivity {

    LinearLayout btn_Login, login, btn_submit;
    String status, message, customer_ID, RM_ID, EnquiryType_ID, Machine_ID, Problem_Type_ID, subtype_ID, f_enq_desc, f_Problem_Type_ID, f_subtype_ID;
    private ProgressDialog loading;
    ListView list_view;
    ImageView left_arrow;
    TextView title_, select_date, select_customer, contactPerson, designation, address, p_mobileno, landLineNo, selectrm, select_EnquiryType;
    Dialog dialogcat, dialogrm, dilogselect_EnquiryType, dilog_malist, dilog_Problem_Type, dilog_sType;
    LinearLayout rm_lay, Serial_no_lay, msdesc_lay, Problem_Type_lay, Problem_sType_lay;
    TextView select_Machine_Name, model_No, serial_No, msdesc, select_Problem_Type, problem_sname, select_Problem_sType,wortext;
    EditText enq_desc;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_new_enqury);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);


        title_ = (TextView) findViewById(R.id.title_);
        title_.setText("Add New Enquiry");

        wortext = (TextView)findViewById(R.id.wortext);


        contactPerson = (TextView) findViewById(R.id.contactPerson);
        designation = (TextView) findViewById(R.id.designation);
        address = (TextView) findViewById(R.id.address);
        p_mobileno = (TextView) findViewById(R.id.p_mobileno);
        landLineNo = (TextView) findViewById(R.id.landLineNo);
        selectrm = (TextView) findViewById(R.id.selectrm);
        select_EnquiryType = (TextView) findViewById(R.id.select_EnquiryType);
        select_Problem_Type = (TextView) findViewById(R.id.select_Problem_Type);
        problem_sname = (TextView) findViewById(R.id.problem_sname);
        select_Problem_sType = (TextView) findViewById(R.id.select_Problem_sType);


        rm_lay = (LinearLayout) findViewById(R.id.rm_lay);
        Serial_no_lay = (LinearLayout) findViewById(R.id.Serial_no_lay);
        msdesc_lay = (LinearLayout) findViewById(R.id.msdesc_lay);
        Problem_Type_lay = (LinearLayout) findViewById(R.id.Problem_Type_lay);
        Problem_sType_lay = (LinearLayout) findViewById(R.id.Problem_sType_lay);

        select_Machine_Name = (TextView) findViewById(R.id.select_Machine_Name);
        model_No = (TextView) findViewById(R.id.model_No);
        serial_No = (TextView) findViewById(R.id.serial_No);
        msdesc = (TextView) findViewById(R.id.msdesc);

        select_date = (TextView) findViewById(R.id.select_date);

        long date = System.currentTimeMillis();
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
        SimpleDateFormat timeonly = new SimpleDateFormat("hh:mm");
        String dateString = sdf.format(date);
        String timeString = timeonly.format(date);
        //show present date
        select_date.setText(dateString);

        //show present time
        //TextView.setText(timeString);


        select_customer = (TextView) findViewById(R.id.select_customer);
        enq_desc = (EditText) findViewById(R.id.enq_desc);


        btn_submit = (LinearLayout) findViewById(R.id.btn_submit);


        btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(customer_ID.equalsIgnoreCase("")){
                    Toast.makeText(Addnew_Enqury.this, "Following fields are compulsory !\n" +
                            "Select Customer .\n" +
                            "Select RM .\n" +
                            "Select Machine .\n" +
                            "Select Enquiry Type .", Toast.LENGTH_LONG).show();

                }

                else if(Machine_ID.equalsIgnoreCase("")){

                    Toast.makeText(Addnew_Enqury.this, "Following fields are compulsory !\n" +
                            "Select Machine .\n" +
                            "SerialNo Compulsary.", Toast.LENGTH_LONG).show();
                }
                else{
                    if (EnquiryType_ID.equalsIgnoreCase("3")) {

                        f_enq_desc = enq_desc.getText().toString();
                        f_Problem_Type_ID = "0"; //Problem_Type_ID;
                        f_subtype_ID = "0";   //subtype_ID;

                        Get_Save_Enquiry();



                    } else if (EnquiryType_ID.equalsIgnoreCase("2")) {


                        if(Problem_Type_ID.equalsIgnoreCase("")){

                            Toast.makeText(Addnew_Enqury.this, "Please Select Problem type ", Toast.LENGTH_LONG).show();

                        }
                        else  if(Problem_Type_ID.equalsIgnoreCase("1")){


                            if(f_subtype_ID.equalsIgnoreCase("")) {

                                Toast.makeText(Addnew_Enqury.this, "Please Select Mechanical Problem  ", Toast.LENGTH_LONG).show();

                            }
                            else{

                                f_enq_desc = enq_desc.getText().toString();
                                f_Problem_Type_ID = Problem_Type_ID;
                                f_subtype_ID = subtype_ID;

                                Get_Save_Enquiry();

                            }

                        }
                        else if(Problem_Type_ID.equalsIgnoreCase("2")){

                            if(f_subtype_ID.equalsIgnoreCase("")) {

                                Toast.makeText(Addnew_Enqury.this, "Please Select Chemical Problem ", Toast.LENGTH_LONG).show();

                            }
                            else{

                                f_enq_desc = enq_desc.getText().toString();
                                f_Problem_Type_ID = Problem_Type_ID;
                                f_subtype_ID = subtype_ID;

                                Get_Save_Enquiry();

                            }

                        }
                    } else if (EnquiryType_ID.equalsIgnoreCase("1")) {

                        f_enq_desc = enq_desc.getText().toString();
                        f_Problem_Type_ID = "0"; //Problem_Type_ID;
                        f_subtype_ID = "0";   //subtype_ID;

                        Get_Save_Enquiry();

                    }

                }


            }
        });

        select_customer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialogcat = new Dialog(Addnew_Enqury.this);
                dialogcat.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialogcat.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                dialogcat.setContentView(R.layout.dialog_option);

                list_view = (ListView) dialogcat.findViewById(R.id.lis_emp);

                Custmar_List_Array custmar_List_Array = PrefUtils.get_custmar_List_Array(Addnew_Enqury.this);

                Adapter_Service adapter_Service = new Adapter_Service(Addnew_Enqury.this, custmar_List_Array.getCategory_List());

                list_view.setAdapter(adapter_Service);


                list_view.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                        Custmar_List_Array custmar_List_Array = PrefUtils.get_custmar_List_Array(Addnew_Enqury.this);
                        select_customer.setText("" + custmar_List_Array.getCategory_List().get(i).getCustName());
                        //  Toast.makeText(Inquire_Activity.this, ""+state_list_array.getStateArrayList().get(i).getStateName(), Toast.LENGTH_LONG).show();

                        customer_ID = "" + custmar_List_Array.getCategory_List().get(i).getCustId();

                        //  Toast.makeText(Addnew_Enqury.this, ""+customer_ID, Toast.LENGTH_LONG).show();

                        dialogcat.dismiss();
                        get_cust_detail();

                    }
                });

                dialogcat.show();


            }
        });


        selectrm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (customer_ID.equalsIgnoreCase("")) {

                    Toast.makeText(Addnew_Enqury.this, "Please select Customer", Toast.LENGTH_SHORT).show();


                } else {

                    dialogrm = new Dialog(Addnew_Enqury.this);
                    dialogrm.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    dialogrm.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                    dialogrm.setContentView(R.layout.dialogrm_option);

                    list_view = (ListView) dialogrm.findViewById(R.id.lis_emp);

                    RM_List_Array rm_List_Array = PrefUtils.get_rm_List_Array(Addnew_Enqury.this);

                    Adapter_Servicerm adapter_Servicerm = new Adapter_Servicerm(Addnew_Enqury.this, rm_List_Array.getCategory_List());

                    list_view.setAdapter(adapter_Servicerm);


                    list_view.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                            RM_List_Array rm_List_Array = PrefUtils.get_rm_List_Array(Addnew_Enqury.this);
                            selectrm.setText("" + rm_List_Array.getCategory_List().get(i).getEngName());
                            //  Toast.makeText(Inquire_Activity.this, ""+state_list_array.getStateArrayList().get(i).getStateName(), Toast.LENGTH_LONG).show();

                            RM_ID = "" + rm_List_Array.getCategory_List().get(i).getEngId();
                            dialogrm.dismiss();
                            /// get_cust_detail();

                        }
                    });

                    dialogrm.show();

                }


            }
        });


        select_EnquiryType.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dilogselect_EnquiryType = new Dialog(Addnew_Enqury.this);
                dilogselect_EnquiryType.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dilogselect_EnquiryType.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                dilogselect_EnquiryType.setContentView(R.layout.dialogenqtyp_option);

                list_view = (ListView) dilogselect_EnquiryType.findViewById(R.id.lis_emp);

                Enquiry_type_List_Array enquiry_type_List_Array = PrefUtils.get_enquiry_type_List_Array(Addnew_Enqury.this);

                Adapter_Servicenq adapter_Servicenq = new Adapter_Servicenq(Addnew_Enqury.this, enquiry_type_List_Array.getCategory_List());

                list_view.setAdapter(adapter_Servicenq);


                list_view.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                        Enquiry_type_List_Array enquiry_type_List_Array = PrefUtils.get_enquiry_type_List_Array(Addnew_Enqury.this);
                        select_EnquiryType.setText("" + enquiry_type_List_Array.getCategory_List().get(i).getEnquiry());
                        //  Toast.makeText(Inquire_Activity.this, ""+state_list_array.getStateArrayList().get(i).getStateName(), Toast.LENGTH_LONG).show();

                        EnquiryType_ID = "" + enquiry_type_List_Array.getCategory_List().get(i).getID();

                        String EnquiryType_IDname = "" + enquiry_type_List_Array.getCategory_List().get(i).getEnquiry();

                        dilogselect_EnquiryType.dismiss();


                        select_Machine_Name.setText("");
                        Machine_ID = "";

                        model_No.setText("");
                        serial_No.setText("");
                        msdesc.setText("");

                        if (EnquiryType_IDname.equals("General")) {

                            rm_lay.setVisibility(View.GONE);
                            Serial_no_lay.setVisibility(View.GONE);
                            msdesc_lay.setVisibility(View.VISIBLE);
                            Problem_Type_lay.setVisibility(View.GONE);
                            Problem_sType_lay.setVisibility(View.GONE);


                        } else if (EnquiryType_IDname.equals("Complaint")) {

                            rm_lay.setVisibility(View.VISIBLE);
                            Serial_no_lay.setVisibility(View.VISIBLE);
                            msdesc_lay.setVisibility(View.GONE);
                            Problem_Type_lay.setVisibility(View.VISIBLE);
                            Problem_sType_lay.setVisibility(View.GONE);

                        } else {

                            rm_lay.setVisibility(View.VISIBLE);
                            Serial_no_lay.setVisibility(View.VISIBLE);
                            msdesc_lay.setVisibility(View.GONE);
                            Problem_Type_lay.setVisibility(View.GONE);
                            Problem_sType_lay.setVisibility(View.GONE);

                        }


                        Get_ItemMstList();


                    }
                });

                dilogselect_EnquiryType.show();


            }
        });


        select_Machine_Name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dilog_malist = new Dialog(Addnew_Enqury.this);
                dilog_malist.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dilog_malist.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                dilog_malist.setContentView(R.layout.dialog_mslist);

                list_view = (ListView) dilog_malist.findViewById(R.id.lis_emp);

                Ms_List_Array ms_List_Array = PrefUtils.get_ms_List_Array(Addnew_Enqury.this);

                Adapter_Mslist adapter_Mslist = new Adapter_Mslist(Addnew_Enqury.this, ms_List_Array.getCategory_List());

                list_view.setAdapter(adapter_Mslist);


                list_view.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                        Ms_List_Array ms_List_Array = PrefUtils.get_ms_List_Array(Addnew_Enqury.this);

                        select_Machine_Name.setText("" + ms_List_Array.getCategory_List().get(i).getItemDesc());
                        //  Toast.makeText(Inquire_Activity.this, ""+state_list_array.getStateArrayList().get(i).getStateName(), Toast.LENGTH_LONG).show();

                        Machine_ID = "" + ms_List_Array.getCategory_List().get(i).getItemID();

                        // String   EnquiryType_IDname = "" + enquiry_type_List_Array.getCategory_List().get(i).getEnquiry();

                        dilog_malist.dismiss();


                        Get_ItemMsDetail();


                    }
                });

                dilog_malist.show();


            }
        });


        select_Problem_Type.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dilog_Problem_Type = new Dialog(Addnew_Enqury.this);
                dilog_Problem_Type.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dilog_Problem_Type.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                dilog_Problem_Type.setContentView(R.layout.dialog_problemtype);

                list_view = (ListView) dilog_Problem_Type.findViewById(R.id.lis_emp);

                Problem_type_List_Array problem_type_List_Array = PrefUtils.get_problem_type_List_Array(Addnew_Enqury.this);

                Adapter_Problem_Type adapter_Problem_Type = new Adapter_Problem_Type(Addnew_Enqury.this, problem_type_List_Array.getCategory_List());

                list_view.setAdapter(adapter_Problem_Type);


                list_view.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                        Problem_type_List_Array problem_type_List_Array = PrefUtils.get_problem_type_List_Array(Addnew_Enqury.this);

                        select_Problem_Type.setText("" + problem_type_List_Array.getCategory_List().get(i).getProblem());
                        //  Toast.makeText(Inquire_Activity.this, ""+state_list_array.getStateArrayList().get(i).getStateName(), Toast.LENGTH_LONG).show();

                        Problem_Type_ID = "" + problem_type_List_Array.getCategory_List().get(i).getID();

                        // String   EnquiryType_IDname = "" + enquiry_type_List_Array.getCategory_List().get(i).getEnquiry();

                        dilog_Problem_Type.dismiss();


                        if (Problem_Type_ID.equalsIgnoreCase("1")) {

                            problem_sname.setText("Problem :");
                            select_Problem_sType.setText("Select Problem");

                            Get_Problem_sType();

                        } else if (Problem_Type_ID.equalsIgnoreCase("2")) {

                            problem_sname.setText("Chemical :");
                            select_Problem_sType.setText("Select Chemical");

                            Get_Problem_sType2();

                        }


                        Problem_sType_lay.setVisibility(View.VISIBLE);


                    }
                });

                dilog_Problem_Type.show();


            }
        });


        select_Problem_sType.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dilog_sType = new Dialog(Addnew_Enqury.this);
                dilog_sType.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dilog_sType.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                dilog_sType.setContentView(R.layout.dialog_stype);

                list_view = (ListView) dilog_sType.findViewById(R.id.lis_emp);

                Sub_type_List_Array sub_type_List_Array = PrefUtils.get_sub_type_List_Array(Addnew_Enqury.this);

                Adapter_stype adapter_stype = new Adapter_stype(Addnew_Enqury.this, sub_type_List_Array.getCategory_List());

                list_view.setAdapter(adapter_stype);


                list_view.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                        Sub_type_List_Array sub_type_List_Array = PrefUtils.get_sub_type_List_Array(Addnew_Enqury.this);

                        select_Problem_sType.setText("" + sub_type_List_Array.getCategory_List().get(i).getProblem_Name());
                        //  Toast.makeText(Inquire_Activity.this, ""+state_list_array.getStateArrayList().get(i).getStateName(), Toast.LENGTH_LONG).show();

                        subtype_ID = "" + sub_type_List_Array.getCategory_List().get(i).getProblem_ID();

                        // String   EnquiryType_IDname = "" + enquiry_type_List_Array.getCategory_List().get(i).getEnquiry();

                        dilog_sType.dismiss();


                    }
                });

                dilog_sType.show();


            }
        });


        left_arrow = (ImageView) findViewById(R.id.left_arrow);
        left_arrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                Intent it = new Intent(Addnew_Enqury.this, EnquiryListAct.class);
                it.putExtra("keyName", "0");
                startActivity(it);

            }
        });


        Get_ServiceList();
    }

//    @Override
//    public void onBackPressed() {
//
//        moveTaskToBack(true);
//        android.os.Process.killProcess(android.os.Process.myPid());
//        System.exit(1);
//
//        finish();
//
//    }


    private void Get_ServiceList() {

        PrefUtils.clear_custmar_List_Array(Addnew_Enqury.this);
        RestAdapter adapter = new RestAdapter.Builder().setEndpoint(RetrfitInterface.url).build();

        try {
            loading = new ProgressDialog(Addnew_Enqury.this);
            loading.setMessage("Please Wait Loading data ....");
            loading.show();
            loading.setCancelable(false);

        } catch (Exception e) {

        }

        //Creating Rest Services
        final RetrfitInterface restInterface = adapter.create(RetrfitInterface.class);
        // Log.d("status&&&", "stat1" + restInterface);


        // Log.e("status&&&", "stat1:====  " + PrefUtils.get_Login_List_Array(MyProduct.this).getTncsub_list());


        restInterface.Get_GetCustList(PrefUtils.gete_Employee_IDF(Addnew_Enqury.this), PrefUtils.gete_RoleId(Addnew_Enqury.this), new Callback<Response>()

        {

            @Override
            public void success(Response findChildArray, Response response) {

                Log.e("status", new String(((TypedByteArray) findChildArray.getBody()).getBytes()));

                String result1 = new String(((TypedByteArray) findChildArray.getBody()).getBytes());

                JSONArray jObj = null;
                try {
                    jObj = new JSONArray(result1);

                    Log.e("Size", "" + jObj.length());


                    if (jObj.length() > 0) {
                        for (int i = 0; i < jObj.length(); i++) {

                            JSONObject jsonObj = jObj.getJSONObject(i);

                            String CustId = jsonObj.getString("CustId");
                            String CustName = jsonObj.getString("CustName");

                            if (PrefUtils.get_custmar_List_Array(Addnew_Enqury.this) != null) {

                                Custmar_List_Array custmar_List_Array = PrefUtils.get_custmar_List_Array(Addnew_Enqury.this);
                                Custmar_List custmar_List = new Custmar_List(CustId, CustName);
                                custmar_List_Array.getCategory_List().add(custmar_List);
                                PrefUtils.set_custmar_List_Array(custmar_List_Array, Addnew_Enqury.this);

                            } else {

                                Custmar_List_Array custmar_List_Array = new Custmar_List_Array();
                                custmar_List_Array.category_List = new ArrayList<Custmar_List>();
                                Custmar_List custmar_List = new Custmar_List(CustId, CustName);
                                custmar_List_Array.category_List.add(custmar_List);
                                PrefUtils.set_custmar_List_Array(custmar_List_Array, Addnew_Enqury.this);

                            }


                        }

                        Get_EnquiryTypeList();


                    } else {


                        Toast.makeText(Addnew_Enqury.this, "Username or Password Incorrect", Toast.LENGTH_SHORT).show();

                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }

                // loading.dismiss();

            }

            @Override
            public void failure(RetrofitError error) {

                String merror = error.getMessage();
                Log.d("error", merror);
                Toast.makeText(Addnew_Enqury.this, merror, Toast.LENGTH_LONG).show();

            }
        });


    }

    public class Adapter_Service extends BaseAdapter {


        ArrayList<Custmar_List> result;
        Context context;

        private LayoutInflater inflater = null;

        public Adapter_Service(Addnew_Enqury second, ArrayList<Custmar_List> details) {
            // TODO Auto-generated constructor stub
            result = details;
            context = second;

        }


        /*private view holder class*/
        private class ViewHolder {

            TextView txt_name;


        }

        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder holder = null;

            LayoutInflater mInflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);

            if (convertView == null) {
                convertView = mInflater.inflate(R.layout.customar_row, null);
                holder = new ViewHolder();

                holder.txt_name = (TextView) convertView.findViewById(R.id.txt_name);


                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }


            holder.txt_name.setText(result.get(position).getCustName());
            holder.txt_name.setTag(result.get(position).getCustId());


            return convertView;
        }

        @Override
        public int getCount() {
            return result.size();
        }

        @Override
        public Object getItem(int position) {
            return result.get(position);
        }

        @Override
        public long getItemId(int position) {
            return result.indexOf(getItem(position));
        }
    }

    private void get_cust_detail() {

        // PrefUtils.clear_rm_List_Array(Addnew_Enqury.this);
        RestAdapter adapter = new RestAdapter.Builder().setEndpoint(RetrfitInterface.url).build();

        try {
            loading = new ProgressDialog(Addnew_Enqury.this);
            loading.setMessage("Please Wait Loading data ....");
            loading.show();
            loading.setCancelable(false);

        } catch (Exception e) {

        }

        //Creating Rest Services
        final RetrfitInterface restInterface = adapter.create(RetrfitInterface.class);
        Log.d("status&&&", "stat1" + restInterface);


        restInterface.Get_MyProfile(customer_ID, new Callback<Response>() {

            @Override
            public void success(Response findChildArray, Response response) {
                //  Log.e("status&&&", "stat" + response.getReason() + "response=" + response.getUrl() + "status" + response.getStatus());

                Log.e("status", new String(((TypedByteArray) findChildArray.getBody()).getBytes()));

                String result1 = new String(((TypedByteArray) findChildArray.getBody()).getBytes());

                JSONObject jObj = null;

                JSONObject jObj1 = null;
                JSONObject json3 = null;

                try {

                    jObj = new JSONObject(result1);

                    Log.e("jObj", "" + jObj);
                    JSONObject json2 = jObj.getJSONObject("result");

                    Log.e("json2", "" + json2);
                    json3 = json2.getJSONObject("ProfileDetail");


                } catch (JSONException e) {
                    e.printStackTrace();
                }

                if (json3.length() > 0) {


                    try {

                        String f_CustId = json3.getString("CustId");
                        String f_CustName = json3.getString("CustName");
                        String f_CustAddr = json3.getString("CustAddr");
                        String f_CustCity = json3.getString("CustCity");
                        String f_CustState = json3.getString("CustState");
                        String f_CustPin = json3.getString("CustPin");
                        String f_CustContact = json3.getString("CustContact");
                        String f_CustPerson = json3.getString("CustPerson");
                        String f_PersonDesc = json3.getString("PersonDesc");
                        String f_CustEmailid = json3.getString("CustEmailid");
                        String f_CustDistrict = json3.getString("CustDistrict");
                        String f_GSTTIN = json3.getString("GSTTIN");
                        String f_CustCode = json3.getString("CustCode");
                        String f_RMID = json3.getString("RMID");


                        contactPerson.setText(f_CustPerson);
                        designation.setText(f_PersonDesc);
                        address.setText(f_CustAddr);
                        p_mobileno.setText(f_CustContact);
                        landLineNo.setText(f_CustPerson);


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                } else {

                    Toast.makeText(Addnew_Enqury.this, message, Toast.LENGTH_SHORT).show();

                }

                Get_RM();
                // loading.dismiss();

            }

            @Override
            public void failure(RetrofitError error) {

                String merror = error.getMessage();
                Log.d("error", merror);
                Toast.makeText(Addnew_Enqury.this, merror, Toast.LENGTH_LONG).show();

            }
        });


    }


    private void Get_RM() {

        PrefUtils.clear_rm_List_Array(Addnew_Enqury.this);
        RestAdapter adapter = new RestAdapter.Builder().setEndpoint(RetrfitInterface.url).build();

        try {
//            loading = new ProgressDialog(Addnew_Enqury.this);
//            loading.setMessage("Please Wait Loading data ....");
//            loading.show();
//            loading.setCancelable(false);

        } catch (Exception e) {

        }

        //Creating Rest Services
        final RetrfitInterface restInterface = adapter.create(RetrfitInterface.class);
        // Log.d("status&&&", "stat1" + restInterface);


        // Log.e("status&&&", "stat1:====  " + PrefUtils.get_Login_List_Array(MyProduct.this).getTncsub_list());


        restInterface.Get_RMList(new Callback<Response>()

        {

            @Override
            public void success(Response findChildArray, Response response) {

                Log.e("status", new String(((TypedByteArray) findChildArray.getBody()).getBytes()));

                String result1 = new String(((TypedByteArray) findChildArray.getBody()).getBytes());

                JSONArray jObj = null;
                try {
                    jObj = new JSONArray(result1);

                    Log.e("Size", "" + jObj.length());


                    if (jObj.length() > 0) {
                        for (int i = 0; i < jObj.length(); i++) {

                            JSONObject jsonObj = jObj.getJSONObject(i);

                            String EngId = jsonObj.getString("EngId");
                            String EngName = jsonObj.getString("EngName");

                            if (PrefUtils.get_rm_List_Array(Addnew_Enqury.this) != null) {

                                RM_List_Array rm_List_Array = PrefUtils.get_rm_List_Array(Addnew_Enqury.this);
                                RM_List rm_List = new RM_List(EngId, EngName);
                                rm_List_Array.getCategory_List().add(rm_List);
                                PrefUtils.set_rm_List_Array(rm_List_Array, Addnew_Enqury.this);

                            } else {

                                RM_List_Array rm_List_Array = new RM_List_Array();
                                rm_List_Array.category_List = new ArrayList<RM_List>();
                                RM_List rm_List = new RM_List(EngId, EngName);
                                rm_List_Array.category_List.add(rm_List);
                                PrefUtils.set_rm_List_Array(rm_List_Array, Addnew_Enqury.this);

                            }


                        }


                    } else {


                        Toast.makeText(Addnew_Enqury.this, "Username or Password Incorrect", Toast.LENGTH_SHORT).show();

                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }

                loading.dismiss();

            }

            @Override
            public void failure(RetrofitError error) {

                String merror = error.getMessage();
                Log.d("error", merror);
                Toast.makeText(Addnew_Enqury.this, merror, Toast.LENGTH_LONG).show();

            }
        });


    }

    public class Adapter_Servicerm extends BaseAdapter {


        ArrayList<RM_List> result;
        Context context;

        private LayoutInflater inflater = null;

        public Adapter_Servicerm(Addnew_Enqury second, ArrayList<RM_List> details) {
            // TODO Auto-generated constructor stub
            result = details;
            context = second;

        }


        /*private view holder class*/
        private class ViewHolder {

            TextView txt_name;


        }

        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder holder = null;

            LayoutInflater mInflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);

            if (convertView == null) {
                convertView = mInflater.inflate(R.layout.customar_row, null);
                holder = new ViewHolder();

                holder.txt_name = (TextView) convertView.findViewById(R.id.txt_name);


                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }


            holder.txt_name.setText(result.get(position).getEngName());
            holder.txt_name.setTag(result.get(position).getEngId());


            return convertView;
        }

        @Override
        public int getCount() {
            return result.size();
        }

        @Override
        public Object getItem(int position) {
            return result.get(position);
        }

        @Override
        public long getItemId(int position) {
            return result.indexOf(getItem(position));
        }
    }


    private void Get_EnquiryTypeList() {

        PrefUtils.clear_enquiry_type_List_Array(Addnew_Enqury.this);
        RestAdapter adapter = new RestAdapter.Builder().setEndpoint(RetrfitInterface.url).build();

        try {
//            loading = new ProgressDialog(Addnew_Enqury.this);
//            loading.setMessage("Please Wait Loading data ....");
//            loading.show();
//            loading.setCancelable(false);

        } catch (Exception e) {

        }

        //Creating Rest Services
        final RetrfitInterface restInterface = adapter.create(RetrfitInterface.class);
        // Log.d("status&&&", "stat1" + restInterface);


        // Log.e("status&&&", "stat1:====  " + PrefUtils.get_Login_List_Array(MyProduct.this).getTncsub_list());


        restInterface.Get_EnquiryTypeList(new Callback<Response>()

        {

            @Override
            public void success(Response findChildArray, Response response) {

                Log.e("status", new String(((TypedByteArray) findChildArray.getBody()).getBytes()));

                String result1 = new String(((TypedByteArray) findChildArray.getBody()).getBytes());

                JSONArray jObj = null;
                try {
                    jObj = new JSONArray(result1);

                    Log.e("Size", "" + jObj.length());


                    if (jObj.length() > 0) {
                        for (int i = 0; i < jObj.length(); i++) {

                            JSONObject jsonObj = jObj.getJSONObject(i);

                            String ID = jsonObj.getString("ID");
                            String Enquiry = jsonObj.getString("Enquiry");

                            if (PrefUtils.get_enquiry_type_List_Array(Addnew_Enqury.this) != null) {

                                Enquiry_type_List_Array enquiry_type_List_Array = PrefUtils.get_enquiry_type_List_Array(Addnew_Enqury.this);
                                Enquiry_type_List enquiry_type_List = new Enquiry_type_List(ID, Enquiry);
                                enquiry_type_List_Array.getCategory_List().add(enquiry_type_List);
                                PrefUtils.set_enquiry_type_List_Array(enquiry_type_List_Array, Addnew_Enqury.this);

                            } else {

                                Enquiry_type_List_Array enquiry_type_List_Array = new Enquiry_type_List_Array();
                                enquiry_type_List_Array.category_List = new ArrayList<Enquiry_type_List>();
                                Enquiry_type_List enquiry_type_List = new Enquiry_type_List(ID, Enquiry);
                                enquiry_type_List_Array.category_List.add(enquiry_type_List);
                                PrefUtils.set_enquiry_type_List_Array(enquiry_type_List_Array, Addnew_Enqury.this);

                            }


                        }


                    } else {


                        Toast.makeText(Addnew_Enqury.this, "Username or Password Incorrect", Toast.LENGTH_SHORT).show();

                    }

                    Get_Problem_Type();
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                loading.dismiss();

            }

            @Override
            public void failure(RetrofitError error) {

                String merror = error.getMessage();
                Log.d("error", merror);
                Toast.makeText(Addnew_Enqury.this, merror, Toast.LENGTH_LONG).show();

            }
        });


    }


    public class Adapter_Servicenq extends BaseAdapter {


        ArrayList<Enquiry_type_List> result;
        Context context;

        private LayoutInflater inflater = null;

        public Adapter_Servicenq(Addnew_Enqury second, ArrayList<Enquiry_type_List> details) {
            // TODO Auto-generated constructor stub
            result = details;
            context = second;

        }


        /*private view holder class*/
        private class ViewHolder {

            TextView txt_name;


        }

        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder holder = null;

            LayoutInflater mInflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);

            if (convertView == null) {
                convertView = mInflater.inflate(R.layout.customar_row, null);
                holder = new ViewHolder();

                holder.txt_name = (TextView) convertView.findViewById(R.id.txt_name);


                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }


            holder.txt_name.setText(result.get(position).getEnquiry());
            holder.txt_name.setTag(result.get(position).getID());


            return convertView;
        }

        @Override
        public int getCount() {
            return result.size();
        }

        @Override
        public Object getItem(int position) {
            return result.get(position);
        }

        @Override
        public long getItemId(int position) {
            return result.indexOf(getItem(position));
        }
    }


    private void Get_ItemMstList() {

        PrefUtils.clear_ms_List_Array(Addnew_Enqury.this);
        RestAdapter adapter = new RestAdapter.Builder().setEndpoint(RetrfitInterface.url).build();

        try {
//            loading = new ProgressDialog(Addnew_Enqury.this);
//            loading.setMessage("Please Wait Loading data ....");
//            loading.show();
//            loading.setCancelable(false);

        } catch (Exception e) {

        }

        //Creating Rest Services
        final RetrfitInterface restInterface = adapter.create(RetrfitInterface.class);
        // Log.d("status&&&", "stat1" + restInterface);


        // Log.e("status&&&", "stat1:====  " + PrefUtils.get_Login_List_Array(MyProduct.this).getTncsub_list());


        restInterface.Get_ItemMstList(PrefUtils.gete_Employee_IDF(Addnew_Enqury.this), EnquiryType_ID, new Callback<Response>()

        {

            @Override
            public void success(Response findChildArray, Response response) {

                Log.e("status", new String(((TypedByteArray) findChildArray.getBody()).getBytes()));

                String result1 = new String(((TypedByteArray) findChildArray.getBody()).getBytes());

                JSONArray jObj = null;
                try {
                    jObj = new JSONArray(result1);

                    Log.e("Size", "" + jObj.length());


                    if (jObj.length() > 0) {
                        for (int i = 0; i < jObj.length(); i++) {

                            JSONObject jsonObj = jObj.getJSONObject(i);

                            String ItemID = jsonObj.getString("ItemID");
                            String ItemDesc = jsonObj.getString("ItemDesc");

                            if (PrefUtils.get_ms_List_Array(Addnew_Enqury.this) != null) {

                                Ms_List_Array ms_List_Array = PrefUtils.get_ms_List_Array(Addnew_Enqury.this);
                                Mslist_List mslist_List = new Mslist_List(ItemID, ItemDesc);
                                ms_List_Array.getCategory_List().add(mslist_List);
                                PrefUtils.set_ms_List_Array(ms_List_Array, Addnew_Enqury.this);

                            } else {

                                Ms_List_Array ms_List_Array = new Ms_List_Array();
                                ms_List_Array.category_List = new ArrayList<Mslist_List>();
                                Mslist_List mslist_List = new Mslist_List(ItemID, ItemDesc);
                                ms_List_Array.category_List.add(mslist_List);
                                PrefUtils.set_ms_List_Array(ms_List_Array, Addnew_Enqury.this);

                            }


                        }


                    } else {


                        Toast.makeText(Addnew_Enqury.this, "Username or Password Incorrect", Toast.LENGTH_SHORT).show();

                    }
                    //  Get_EnquiryTypeList();

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                // loading.dismiss();

            }

            @Override
            public void failure(RetrofitError error) {

                String merror = error.getMessage();
                Log.d("error", merror);
                Toast.makeText(Addnew_Enqury.this, merror, Toast.LENGTH_LONG).show();

            }
        });


    }


    public class Adapter_Mslist extends BaseAdapter {


        ArrayList<Mslist_List> result;
        Context context;

        private LayoutInflater inflater = null;

        public Adapter_Mslist(Addnew_Enqury second, ArrayList<Mslist_List> details) {
            // TODO Auto-generated constructor stub
            result = details;
            context = second;

        }


        /*private view holder class*/
        private class ViewHolder {

            TextView txt_name;


        }

        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder holder = null;

            LayoutInflater mInflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);

            if (convertView == null) {
                convertView = mInflater.inflate(R.layout.customar_row, null);
                holder = new ViewHolder();

                holder.txt_name = (TextView) convertView.findViewById(R.id.txt_name);


                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }


            holder.txt_name.setText(result.get(position).getItemDesc());
            holder.txt_name.setTag(result.get(position).getItemID());


            return convertView;
        }

        @Override
        public int getCount() {
            return result.size();
        }

        @Override
        public Object getItem(int position) {
            return result.get(position);
        }

        @Override
        public long getItemId(int position) {
            return result.indexOf(getItem(position));
        }
    }


    private void Get_ItemMsDetail() {

        //PrefUtils.clear_rm_List_Array(Addnew_Enqury.this);
        RestAdapter adapter = new RestAdapter.Builder().setEndpoint(RetrfitInterface.url).build();

        try {
//            loading = new ProgressDialog(Addnew_Enqury.this);
//            loading.setMessage("Please Wait Loading data ....");
//            loading.show();
//            loading.setCancelable(false);

        } catch (Exception e) {

        }

        //Creating Rest Services
        final RetrfitInterface restInterface = adapter.create(RetrfitInterface.class);
        // Log.d("status&&&", "stat1" + restInterface);


        // Log.e("status&&&", "stat1:====  " + PrefUtils.get_Login_List_Array(MyProduct.this).getTncsub_list());


        restInterface.Get_ItemMsDetail(Machine_ID, PrefUtils.gete_Employee_IDF(Addnew_Enqury.this), EnquiryType_ID, new Callback<Response>()

        {

            @Override
            public void success(Response findChildArray, Response response) {

                Log.e("status", new String(((TypedByteArray) findChildArray.getBody()).getBytes()));

                String result1 = new String(((TypedByteArray) findChildArray.getBody()).getBytes());

                JSONObject jObj = null;
                try {
                    jObj = new JSONObject(result1);
                    Log.e("Size", "" + jObj.length());


                    if (jObj.length() > 0) {
                        JSONArray values = jObj.getJSONArray("result");


                        MachineDetailList_List_Array machineDetailList_List_Array = new GsonBuilder().create().fromJson(result1, MachineDetailList_List_Array.class);
                        PrefUtils.set_machineDetailList_List_Array(machineDetailList_List_Array, Addnew_Enqury.this);

                        model_No.setText(PrefUtils.get_machineDetailList_List_Array(Addnew_Enqury.this).getResult().get(0).getItemModel());
                        serial_No.setText(PrefUtils.get_machineDetailList_List_Array(Addnew_Enqury.this).getResult().get(0).getSerNo());
                        msdesc.setText(PrefUtils.get_machineDetailList_List_Array(Addnew_Enqury.this).getResult().get(0).getItemPrerequest());




                        if (machineDetailList_List_Array.getResult().get(0).getWarrStatus().equalsIgnoreCase("Your Machine in Under Warranty"))
                        {

                            wortext.setText("Your Machine in Under Warranty");

                        }
                        if (machineDetailList_List_Array.getResult().get(0).getWarrStatus().equalsIgnoreCase("Your Machine in NON AMC &amp; Out of Warranty"))
                        {


                            wortext.setText("Your Machine in NON AMC & Out of Warranty");


                            AlertDialog.Builder builder = new AlertDialog.Builder(Addnew_Enqury.this);

                            // builder.setTitle("Confirm");
                            builder.setMessage("Your Machine in NON AMC & Out of Warranty\n" +
                                    "Charges (1500) and Travel & Accommodation : Not Applicable\n" +
                                    "Do you Agree(Yes/No)");

                            builder.setPositiveButton("YES", new DialogInterface.OnClickListener() {

                                public void onClick(DialogInterface dialog, int which) {
                                    // Do nothing but close the dialog

                                    wortext.setText("Your Machine in NON AMC & Out of Warranty \n Agree For Charges");

                                    dialog.dismiss();
                                }
                            });

                            builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {

                                @Override
                                public void onClick(DialogInterface dialog, int which) {


                                    wortext.setText("Your Machine in NON AMC & Out of Warranty \n Not Agree For Charges");

                                    // Do nothing
                                    dialog.dismiss();
                                }
                            });

                            AlertDialog alert = builder.create();
                            alert.show();
                        }




                    } else {


                        Toast.makeText(Addnew_Enqury.this, "Username or Password Incorrect", Toast.LENGTH_SHORT).show();

                    }
                    //Get_EnquiryTypeList();

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                // loading.dismiss();

            }

            @Override
            public void failure(RetrofitError error) {

                String merror = error.getMessage();
                Log.d("error", merror);
                Toast.makeText(Addnew_Enqury.this, merror, Toast.LENGTH_LONG).show();

            }
        });


    }


    private void Get_Problem_Type() {

        PrefUtils.clear_problem_type_List_Array(Addnew_Enqury.this);
        RestAdapter adapter = new RestAdapter.Builder().setEndpoint(RetrfitInterface.url).build();

        try {
//            loading = new ProgressDialog(Addnew_Enqury.this);
//            loading.setMessage("Please Wait Loading data ....");
//            loading.show();
//            loading.setCancelable(false);

        } catch (Exception e) {

        }

        //Creating Rest Services
        final RetrfitInterface restInterface = adapter.create(RetrfitInterface.class);
        // Log.d("status&&&", "stat1" + restInterface);


        // Log.e("status&&&", "stat1:====  " + PrefUtils.get_Login_List_Array(MyProduct.this).getTncsub_list());


        restInterface.Get_ProblemTypeList(new Callback<Response>()

        {

            @Override
            public void success(Response findChildArray, Response response) {

                Log.e("status", new String(((TypedByteArray) findChildArray.getBody()).getBytes()));

                String result1 = new String(((TypedByteArray) findChildArray.getBody()).getBytes());

                JSONArray jObj = null;
                try {
                    jObj = new JSONArray(result1);

                    Log.e("Size", "" + jObj.length());


                    if (jObj.length() > 0) {
                        for (int i = 0; i < jObj.length(); i++) {

                            JSONObject jsonObj = jObj.getJSONObject(i);

                            String ID = jsonObj.getString("ID");
                            String Problem = jsonObj.getString("Problem");

                            if (PrefUtils.get_problem_type_List_Array(Addnew_Enqury.this) != null) {

                                Problem_type_List_Array problem_type_List_Array = PrefUtils.get_problem_type_List_Array(Addnew_Enqury.this);
                                Problem_type_List problem_type_List = new Problem_type_List(ID, Problem);
                                problem_type_List_Array.getCategory_List().add(problem_type_List);
                                PrefUtils.set_problem_type_List_Array(problem_type_List_Array, Addnew_Enqury.this);

                            } else {

                                Problem_type_List_Array problem_type_List_Array = new Problem_type_List_Array();
                                problem_type_List_Array.category_List = new ArrayList<Problem_type_List>();
                                Problem_type_List problem_type_List = new Problem_type_List(ID, Problem);
                                problem_type_List_Array.category_List.add(problem_type_List);
                                PrefUtils.set_problem_type_List_Array(problem_type_List_Array, Addnew_Enqury.this);

                            }


                        }


                    } else {


                        Toast.makeText(Addnew_Enqury.this, "Username or Password Incorrect", Toast.LENGTH_SHORT).show();

                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }

                loading.dismiss();

            }

            @Override
            public void failure(RetrofitError error) {

                String merror = error.getMessage();
                Log.d("error", merror);
                Toast.makeText(Addnew_Enqury.this, merror, Toast.LENGTH_LONG).show();

            }
        });


    }


    public class Adapter_Problem_Type extends BaseAdapter {


        ArrayList<Problem_type_List> result;
        Context context;

        private LayoutInflater inflater = null;

        public Adapter_Problem_Type(Addnew_Enqury second, ArrayList<Problem_type_List> details) {
            // TODO Auto-generated constructor stub
            result = details;
            context = second;

        }


        /*private view holder class*/
        private class ViewHolder {

            TextView txt_name;


        }

        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder holder = null;

            LayoutInflater mInflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);

            if (convertView == null) {
                convertView = mInflater.inflate(R.layout.customar_row, null);
                holder = new ViewHolder();

                holder.txt_name = (TextView) convertView.findViewById(R.id.txt_name);


                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }


            holder.txt_name.setText(result.get(position).getProblem());
            holder.txt_name.setTag(result.get(position).getID());


            return convertView;
        }

        @Override
        public int getCount() {
            return result.size();
        }

        @Override
        public Object getItem(int position) {
            return result.get(position);
        }

        @Override
        public long getItemId(int position) {
            return result.indexOf(getItem(position));
        }
    }


    private void Get_Problem_sType() {

        PrefUtils.clear_sub_type_List_Array(Addnew_Enqury.this);
        RestAdapter adapter = new RestAdapter.Builder().setEndpoint(RetrfitInterface.url).build();

        try {
//            loading = new ProgressDialog(Addnew_Enqury.this);
//            loading.setMessage("Please Wait Loading data ....");
//            loading.show();
//            loading.setCancelable(false);

        } catch (Exception e) {

        }

        //Creating Rest Services
        final RetrfitInterface restInterface = adapter.create(RetrfitInterface.class);
        // Log.d("status&&&", "stat1" + restInterface);


        // Log.e("status&&&", "stat1:====  " + PrefUtils.get_Login_List_Array(MyProduct.this).getTncsub_list());


        restInterface.Get_GetMechanicalList(new Callback<Response>()

        {

            @Override
            public void success(Response findChildArray, Response response) {

                Log.e("status", new String(((TypedByteArray) findChildArray.getBody()).getBytes()));

                String result1 = new String(((TypedByteArray) findChildArray.getBody()).getBytes());

                JSONArray jObj = null;
                try {
                    jObj = new JSONArray(result1);

                    Log.e("Size", "" + jObj.length());


                    if (jObj.length() > 0) {
                        for (int i = 0; i < jObj.length(); i++) {

                            JSONObject jsonObj = jObj.getJSONObject(i);

                            String Problem_ID = jsonObj.getString("Problem_ID");
                            String Problem_Name = jsonObj.getString("Problem_Name");

                            if (PrefUtils.get_sub_type_List_Array(Addnew_Enqury.this) != null) {

                                Sub_type_List_Array sub_type_List_Array = PrefUtils.get_sub_type_List_Array(Addnew_Enqury.this);
                                Sub_type_List sub_type_List = new Sub_type_List(Problem_ID, Problem_Name);
                                sub_type_List_Array.getCategory_List().add(sub_type_List);
                                PrefUtils.set_sub_type_List_Array(sub_type_List_Array, Addnew_Enqury.this);

                            } else {

                                Sub_type_List_Array sub_type_List_Array = new Sub_type_List_Array();
                                sub_type_List_Array.category_List = new ArrayList<Sub_type_List>();
                                Sub_type_List sub_type_List = new Sub_type_List(Problem_ID, Problem_Name);
                                sub_type_List_Array.category_List.add(sub_type_List);
                                PrefUtils.set_sub_type_List_Array(sub_type_List_Array, Addnew_Enqury.this);

                            }


                        }


                    } else {


                        Toast.makeText(Addnew_Enqury.this, "Username or Password Incorrect", Toast.LENGTH_SHORT).show();

                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }

                loading.dismiss();

            }

            @Override
            public void failure(RetrofitError error) {

                String merror = error.getMessage();
                Log.d("error", merror);
                Toast.makeText(Addnew_Enqury.this, merror, Toast.LENGTH_LONG).show();

            }
        });


    }


    public class Adapter_stype extends BaseAdapter {


        ArrayList<Sub_type_List> result;
        Context context;

        private LayoutInflater inflater = null;

        public Adapter_stype(Addnew_Enqury second, ArrayList<Sub_type_List> details) {
            // TODO Auto-generated constructor stub
            result = details;
            context = second;

        }


        /*private view holder class*/
        private class ViewHolder {

            TextView txt_name;


        }

        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder holder = null;

            LayoutInflater mInflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);

            if (convertView == null) {
                convertView = mInflater.inflate(R.layout.customar_row, null);
                holder = new ViewHolder();

                holder.txt_name = (TextView) convertView.findViewById(R.id.txt_name);


                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }


            holder.txt_name.setText(result.get(position).getProblem_Name());
            holder.txt_name.setTag(result.get(position).getProblem_ID());


            return convertView;
        }

        @Override
        public int getCount() {
            return result.size();
        }

        @Override
        public Object getItem(int position) {
            return result.get(position);
        }

        @Override
        public long getItemId(int position) {
            return result.indexOf(getItem(position));
        }
    }


    private void Get_Problem_sType2() {

        PrefUtils.clear_sub_type_List_Array(Addnew_Enqury.this);
        RestAdapter adapter = new RestAdapter.Builder().setEndpoint(RetrfitInterface.url).build();

        try {
//            loading = new ProgressDialog(Addnew_Enqury.this);
//            loading.setMessage("Please Wait Loading data ....");
//            loading.show();
//            loading.setCancelable(false);

        } catch (Exception e) {

        }

        //Creating Rest Services
        final RetrfitInterface restInterface = adapter.create(RetrfitInterface.class);
        // Log.d("status&&&", "stat1" + restInterface);


        // Log.e("status&&&", "stat1:====  " + PrefUtils.get_Login_List_Array(MyProduct.this).getTncsub_list());


        restInterface.Get_ChemicalList(new Callback<Response>()

        {

            @Override
            public void success(Response findChildArray, Response response) {

                Log.e("status", new String(((TypedByteArray) findChildArray.getBody()).getBytes()));

                String result1 = new String(((TypedByteArray) findChildArray.getBody()).getBytes());

                JSONArray jObj = null;
                try {
                    jObj = new JSONArray(result1);

                    Log.e("Size", "" + jObj.length());


                    if (jObj.length() > 0) {
                        for (int i = 0; i < jObj.length(); i++) {

                            JSONObject jsonObj = jObj.getJSONObject(i);

                            String Problem_ID = jsonObj.getString("Problem_ID");
                            String Problem_Name = jsonObj.getString("Problem_Name");

                            if (PrefUtils.get_sub_type_List_Array(Addnew_Enqury.this) != null) {

                                Sub_type_List_Array sub_type_List_Array = PrefUtils.get_sub_type_List_Array(Addnew_Enqury.this);
                                Sub_type_List sub_type_List = new Sub_type_List(Problem_ID, Problem_Name);
                                sub_type_List_Array.getCategory_List().add(sub_type_List);
                                PrefUtils.set_sub_type_List_Array(sub_type_List_Array, Addnew_Enqury.this);

                            } else {

                                Sub_type_List_Array sub_type_List_Array = new Sub_type_List_Array();
                                sub_type_List_Array.category_List = new ArrayList<Sub_type_List>();
                                Sub_type_List sub_type_List = new Sub_type_List(Problem_ID, Problem_Name);
                                sub_type_List_Array.category_List.add(sub_type_List);
                                PrefUtils.set_sub_type_List_Array(sub_type_List_Array, Addnew_Enqury.this);

                            }


                        }


                    } else {


                        Toast.makeText(Addnew_Enqury.this, "Username or Password Incorrect", Toast.LENGTH_SHORT).show();

                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }

                loading.dismiss();

            }

            @Override
            public void failure(RetrofitError error) {

                String merror = error.getMessage();
                Log.d("error", merror);
                Toast.makeText(Addnew_Enqury.this, merror, Toast.LENGTH_LONG).show();

            }
        });


    }


    private void Get_Save_Enquiry() {

        PrefUtils.clear_sub_type_List_Array(Addnew_Enqury.this);
        RestAdapter adapter = new RestAdapter.Builder().setEndpoint(RetrfitInterface.url).build();

        try {

            loading = new ProgressDialog(Addnew_Enqury.this);
            loading.setMessage("Please Wait Loading data ....");
            loading.show();
            loading.setCancelable(false);

        } catch (Exception e) {

        }

        //Creating Rest Services
        final RetrfitInterface restInterface = adapter.create(RetrfitInterface.class);
        // Log.d("status&&&", "stat1" + restInterface);


        // Log.e("status&&&", "stat1:====  " + PrefUtils.get_Login_List_Array(MyProduct.this).getTncsub_list());


        restInterface.Get_SaveEnquiry(PrefUtils.gete_User_Id(Addnew_Enqury.this), f_enq_desc, RM_ID, EnquiryType_ID, f_Problem_Type_ID, f_subtype_ID, "0", "NULL", "10014", PrefUtils.gete_RoleId(Addnew_Enqury.this), new Callback<Response>()

        {

            @Override
            public void success(Response findChildArray, Response response) {

                Log.e("status", new String(((TypedByteArray) findChildArray.getBody()).getBytes()));

                String result1 = new String(((TypedByteArray) findChildArray.getBody()).getBytes());

                JSONObject jObj = null;
                try {
                    jObj = new JSONObject(result1);

                    Log.e("Size", "" + jObj.length());


                    if (jObj.length() > 0) {

                        String IsSuccess = jObj.getString("IsSuccess");


                        if (IsSuccess.equalsIgnoreCase("true")) {

                            Intent it = new Intent(Addnew_Enqury.this, EnquiryListAct.class);
                            it.putExtra("keyName", "0");
                            startActivity(it);
                        }


                    } else {


                        Toast.makeText(Addnew_Enqury.this, "Username or Password Incorrect", Toast.LENGTH_SHORT).show();

                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }

                loading.dismiss();

            }

            @Override
            public void failure(RetrofitError error) {

                String merror = error.getMessage();
                Log.d("error", merror);
                Toast.makeText(Addnew_Enqury.this, merror, Toast.LENGTH_LONG).show();

            }
        });


    }


}
