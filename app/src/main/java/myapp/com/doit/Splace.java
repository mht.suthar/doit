package myapp.com.doit;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.WindowManager;

import myapp.com.doit.engineer.ui.EngineerDashboardAct;
import myapp.com.doit.utils.PrefUtils;
import myapp.com.doit.prelogin.ui.Login;


public class Splace extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splace);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);


        Thread background = new Thread() {
            public void run() {
                try {
                    sleep(3 * 1000);
                    Intent it = null;
                    if (PrefUtils.gete_User_Id(Splace.this).equalsIgnoreCase("")) {
                        it = new Intent(Splace.this, Login.class);
                        it.putExtra("keyName", "0");
                    } else if(PrefUtils.getRoleName(Splace.this).equalsIgnoreCase("Admin")) {
                        it = new Intent(Splace.this, AdminDashboardActivity.class);
                        it.putExtra("keyName", "0");
                    }else if(PrefUtils.getRoleName(Splace.this).equalsIgnoreCase("Engineer"))
                        it = new Intent(Splace.this, EngineerDashboardAct.class);
                    else
                        it = new Intent(Splace.this, EngineerDashboardAct.class);

                    startActivity(it);
                    finish();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };
        background.start();
    }

    @Override
    public void onBackPressed() {
        moveTaskToBack(true);
        android.os.Process.killProcess(android.os.Process.myPid());
        System.exit(1);
        finish();
    }
}
