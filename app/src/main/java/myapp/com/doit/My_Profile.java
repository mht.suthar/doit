package myapp.com.doit;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import myapp.com.doit.utils.PrefUtils;
import myapp.com.doit.rest.RetrfitInterface;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedByteArray;


public class My_Profile extends AppCompatActivity {


    String status, message;
    private ProgressDialog loading;
    TextView c_name, CustPerson, PersonDesc, CustContact, CustEmailid, GSTTIN, RMID, CustCode, CustAddr, CustPin;
    ImageView left_arrow;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        c_name = (TextView) findViewById(R.id.c_name);
        CustPerson = (TextView) findViewById(R.id.CustPerson);
        PersonDesc = (TextView) findViewById(R.id.PersonDesc);
        CustContact = (TextView) findViewById(R.id.CustContact);
        CustEmailid = (TextView) findViewById(R.id.CustEmailid);
        GSTTIN = (TextView) findViewById(R.id.GSTTIN);
        RMID = (TextView) findViewById(R.id.RMID);
        CustCode = (TextView) findViewById(R.id.CustCode);
        CustAddr = (TextView) findViewById(R.id.CustAddr);
        CustPin = (TextView) findViewById(R.id.CustPin);


        left_arrow = (ImageView) findViewById(R.id.left_arrow);

        left_arrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                Intent it = new Intent(My_Profile.this, AdminDashboardActivity.class);
                it.putExtra("keyName", "0");
                startActivity(it);

            }
        });


        SendLogin();
    }


    private void SendLogin() {


        RestAdapter adapter = new RestAdapter.Builder().setEndpoint(RetrfitInterface.url).build();

        try {
            loading = new ProgressDialog(My_Profile.this);
            loading.setMessage("Please Wait Loading data ....");
            loading.show();
            loading.setCancelable(false);

        } catch (Exception e) {

        }

        //Creating Rest Services
        final RetrfitInterface restInterface = adapter.create(RetrfitInterface.class);
        Log.d("status&&&", "stat1" + restInterface);


        restInterface.Get_MyProfile(PrefUtils.gete_Employee_IDF(My_Profile.this), new Callback<Response>() {

            @Override
            public void success(Response findChildArray, Response response) {
                //  Log.e("status&&&", "stat" + response.getReason() + "response=" + response.getUrl() + "status" + response.getStatus());

                Log.e("status", new String(((TypedByteArray) findChildArray.getBody()).getBytes()));

                String result1 = new String(((TypedByteArray) findChildArray.getBody()).getBytes());

                JSONObject jObj = null;

                JSONObject jObj1 = null;
                JSONObject json3 = null;

                try {

                    jObj = new JSONObject(result1);

                    Log.e("jObj", "" + jObj);
                    JSONObject json2 = jObj.getJSONObject("result");

                    Log.e("json2", "" + json2);
                    json3 = json2.getJSONObject("ProfileDetail");


                } catch (JSONException e) {
                    e.printStackTrace();
                }

                if (json3.length() > 0) {


                    try {
                        String f_CustId = json3.getString("CustId");
                        String f_CustName = json3.getString("CustName");
                        String f_CustAddr = json3.getString("CustAddr");
                        String f_CustCity = json3.getString("CustCity");
                        String f_CustState = json3.getString("CustState");
                        String f_CustPin = json3.getString("CustPin");
                        String f_CustContact = json3.getString("CustContact");
                        String f_CustPerson = json3.getString("CustPerson");
                        String f_PersonDesc = json3.getString("PersonDesc");
                        String f_CustEmailid = json3.getString("CustEmailid");
                        String f_CustDistrict = json3.getString("CustDistrict");
                        String f_GSTTIN = json3.getString("GSTTIN");
                        String f_CustCode = json3.getString("CustCode");
                        String f_RMID = json3.getString("RMID");


                       // Toast.makeText(My_Profile.this, f_CustId, Toast.LENGTH_SHORT).show();

                        c_name.setText(""+f_CustName);
                        CustPerson.setText(f_CustPerson);
                        PersonDesc.setText(f_PersonDesc);
                        CustContact.setText(f_CustContact);
                        CustEmailid.setText(f_CustEmailid);
                        GSTTIN.setText(f_GSTTIN);
                        RMID.setText(f_RMID);
                        CustCode.setText(f_CustCode);
                        CustAddr.setText(f_CustAddr);
                        CustPin.setText(f_CustPin);


//                        PrefUtils.set_User_Id(User_Id,My_Profile.this);
//                        PrefUtils.set_Employee_IDF(Employee_IDF,My_Profile.this);
//
//
//
//                        Intent it = new Intent(My_Profile.this, AdminDashboardActivity.class);
//                        it.putExtra("keyName", "0");
//                        startActivity(it);


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                } else {

                    Toast.makeText(My_Profile.this, message, Toast.LENGTH_SHORT).show();

                }


                loading.dismiss();

            }

            @Override
            public void failure(RetrofitError error) {

                String merror = error.getMessage();
                Log.d("error", merror);
                Toast.makeText(My_Profile.this, merror, Toast.LENGTH_LONG).show();

            }
        });


    }


//    @Override
//    public void onBackPressed() {
//
//        moveTaskToBack(true);
//        android.os.Process.killProcess(android.os.Process.myPid());
//        System.exit(1);
//
//        finish();
//
//    }
}
