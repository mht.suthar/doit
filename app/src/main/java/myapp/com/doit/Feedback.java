package myapp.com.doit;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import myapp.com.doit.helper.Callid_List;
import myapp.com.doit.helper.Callid_List_Array;
import myapp.com.doit.utils.PrefUtils;
import myapp.com.doit.rest.RetrfitInterface;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedByteArray;


public class Feedback extends AppCompatActivity {


    String status, message;
    private ProgressDialog loading;
    TextView s_call_no;
    EditText feedback;
    public RatingBar ratingBar;
    Dialog dialogcat;
    String CallId, f_feedback, rating;
    ImageView left_arrow;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feedback);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        ratingBar = (RatingBar) findViewById(R.id.ratingBar);

        s_call_no = (TextView) findViewById(R.id.s_call_no);
        feedback = (EditText) findViewById(R.id.feedback);



        left_arrow = (ImageView) findViewById(R.id.left_arrow);

        left_arrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                Intent it = new Intent(Feedback.this, AdminDashboardActivity.class);
                it.putExtra("keyName", "0");
                startActivity(it);

            }
        });

        s_call_no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                dialogcat = new Dialog(Feedback.this);
                dialogcat.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialogcat.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                dialogcat.setContentView(R.layout.dialog_city_listcat);

                ListView lis_city = (ListView) dialogcat.findViewById(R.id.lis_city);

                Callid_List_Array callid_List_Array = PrefUtils.get_callid_List_Array(Feedback.this);

                Adapter_catlist1 adapter = new Adapter_catlist1(Feedback.this, callid_List_Array.getCallid_List_data());
                lis_city.setAdapter(adapter);

                lis_city.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                        Callid_List_Array callid_List_Array = PrefUtils.get_callid_List_Array(Feedback.this);

                        s_call_no.setText("" + callid_List_Array.getCallid_List_data().get(i).getCallRepNo());

                        CallId = "" + callid_List_Array.getCallid_List_data().get(i).getCallId();
                        dialogcat.dismiss();

                    }
                });

                dialogcat.show();

            }
        });

        SendLogin();
    }


    public void rateMe(View view) {

        f_feedback = feedback.getText().toString();

//        Toast.makeText(getApplicationContext(),
//                String.valueOf(ratingBar.getRating()), Toast.LENGTH_LONG).show();


        if (CallId.equalsIgnoreCase("")) {


            Toast.makeText(Feedback.this, "please select your Call Report No.", Toast.LENGTH_LONG).show();
        } else if (f_feedback.equalsIgnoreCase("")) {

            Toast.makeText(Feedback.this, "please add your Remark", Toast.LENGTH_LONG).show();

        } else {


            rating = String.valueOf(ratingBar.getRating());
            Sendfeedback();

        }


    }

    private void SendLogin() {

        PrefUtils.clear_callid_List_Array(Feedback.this);

        RestAdapter adapter = new RestAdapter.Builder().setEndpoint(RetrfitInterface.url).build();

        try {
            loading = new ProgressDialog(Feedback.this);
            loading.setMessage("Please Wait Loading data ....");
            loading.show();
            loading.setCancelable(false);

        } catch (Exception e) {

        }

        //Creating Rest Services
        final RetrfitInterface restInterface = adapter.create(RetrfitInterface.class);
        Log.d("status&&&", "stat1" + restInterface);


        restInterface.Get_CallReportList(PrefUtils.gete_Employee_IDF(Feedback.this), new Callback<Response>() {

            @Override
            public void success(Response findChildArray, Response response) {
                //  Log.e("status&&&", "stat" + response.getReason() + "response=" + response.getUrl() + "status" + response.getStatus());

                Log.e("status", new String(((TypedByteArray) findChildArray.getBody()).getBytes()));

                String result1 = new String(((TypedByteArray) findChildArray.getBody()).getBytes());

                JSONArray jObj = null;

                try {

                    jObj = new JSONArray(result1);

                    Log.e("jObj", "" + jObj);
                    for (int i = 0; i < jObj.length(); i++) {

                        JSONObject jsonObj = jObj.getJSONObject(i);

                        String CallId = jsonObj.getString("CallId");
                        String CallRepNo = jsonObj.getString("CallRepNo");


                        Log.e("CallId :=== ", CallId);
                        Log.e("CallRepNo :===  ", CallRepNo);

                        if (PrefUtils.get_callid_List_Array(Feedback.this) != null) {

                            Callid_List_Array callid_List_Array = PrefUtils.get_callid_List_Array(Feedback.this);
                            Callid_List callid_List = new Callid_List(CallId, CallRepNo);
                            callid_List_Array.getCallid_List_data().add(callid_List);
                            PrefUtils.set_callid_List_Array(callid_List_Array, Feedback.this);

                        } else {

                            Callid_List_Array callid_List_Array = new Callid_List_Array();
                            callid_List_Array.Callid_List_data = new ArrayList<Callid_List>();
                            Callid_List callid_List = new Callid_List(CallId, CallRepNo);
                            callid_List_Array.Callid_List_data.add(callid_List);
                            PrefUtils.set_callid_List_Array(callid_List_Array, Feedback.this);

                        }


                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }

//                if (json3.length() > 0) {
//
//
//
//                } else {
//
//                    Toast.makeText(Feedback.this, message, Toast.LENGTH_SHORT).show();
//
//                }


                loading.dismiss();

            }

            @Override
            public void failure(RetrofitError error) {

                String merror = error.getMessage();
                Log.d("error", merror);
                Toast.makeText(Feedback.this, merror, Toast.LENGTH_LONG).show();

            }
        });


    }


    private void Sendfeedback() {


        RestAdapter adapter = new RestAdapter.Builder().setEndpoint(RetrfitInterface.url).build();

        try {
            loading = new ProgressDialog(Feedback.this);
            loading.setMessage("Please Wait Loading data ....");
            loading.show();
            loading.setCancelable(false);

        } catch (Exception e) {

        }

        //Creating Rest Services
        final RetrfitInterface restInterface = adapter.create(RetrfitInterface.class);
        Log.d("status&&&", "stat1" + restInterface);


        restInterface.Get_Send_feedback(CallId, f_feedback,"5" , new Callback<Response>() {

            @Override
            public void success(Response findChildArray, Response response) {
                //  Log.e("status&&&", "stat" + response.getReason() + "response=" + response.getUrl() + "status" + response.getStatus());

                Log.e("status", new String(((TypedByteArray) findChildArray.getBody()).getBytes()));

                String result1 = new String(((TypedByteArray) findChildArray.getBody()).getBytes());

                JSONObject jObj = null;

                try {

                    jObj = new JSONObject(result1);

                    JSONObject jsonObj = jObj.getJSONObject("result");

                    String IsSuccess = jsonObj.getString("IsSuccess");

                   if(IsSuccess.equalsIgnoreCase("true")){

                       Toast.makeText(Feedback.this, "Feedback Create Succesfully", Toast.LENGTH_SHORT).show();

                       feedback.setText("");
                       CallId= "";
                       s_call_no.setText("");
                       ratingBar.setRating((float) 0.0);
                       loading.dismiss();
                       SendLogin();



                   }


                } catch (JSONException e) {
                    e.printStackTrace();
                }

//                if (json3.length() > 0) {
//
//
//
//                } else {
//
//                    Toast.makeText(Feedback.this, message, Toast.LENGTH_SHORT).show();
//
//                }


                loading.dismiss();

            }

            @Override
            public void failure(RetrofitError error) {

                String merror = error.getMessage();
                Log.d("error", merror);
                Toast.makeText(Feedback.this, merror, Toast.LENGTH_LONG).show();

            }
        });


    }


    public class Adapter_catlist1 extends BaseAdapter {


        ArrayList<Callid_List> result;
        Context context;

        private LayoutInflater inflater = null;

        public Adapter_catlist1(Feedback mainActivity, ArrayList<Callid_List> city_list) {
            // TODO Auto-generated constructor stub
            result = city_list;
            context = mainActivity;

        }


        /*private view holder class*/
        private class ViewHolder {

            TextView cat_name;


        }

        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder holder = null;

            LayoutInflater mInflater = (LayoutInflater)
                    context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            if (convertView == null) {
                convertView = mInflater.inflate(R.layout.items_listview, null);
                holder = new ViewHolder();

                holder.cat_name = (TextView) convertView.findViewById(R.id.cat_name);

                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }

            //final Sedullist rowItem = (Sedullist) getItem(position);


            holder.cat_name.setText(result.get(position).getCallRepNo());


            return convertView;
        }

        @Override
        public int getCount() {
            return result.size();
        }

        @Override
        public Object getItem(int position) {
            return result.get(position);
        }

        @Override
        public long getItemId(int position) {
            return result.indexOf(getItem(position));
        }
    }


//    @Override
//    public void onBackPressed() {
//
//        moveTaskToBack(true);
//        android.os.Process.killProcess(android.os.Process.myPid());
//        System.exit(1);
//
//        finish();
//
//    }
}
