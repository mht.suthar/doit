package myapp.com.doit.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import myapp.com.doit.helper.Amc_List_Array;
import myapp.com.doit.helper.Callid_List_Array;
import myapp.com.doit.helper.ComplexPreferences;
import myapp.com.doit.helper.Custmar_List_Array;
import myapp.com.doit.helper.DoItTour_List_Array;
import myapp.com.doit.helper.Enquiry1_List_Array;
import myapp.com.doit.helper.Enquiry_type_List_Array;
import myapp.com.doit.helper.Login_List_Array;
import myapp.com.doit.helper.MachineDetailList_List_Array;
import myapp.com.doit.helper.Ms_List_Array;
import myapp.com.doit.helper.Notification_List_Array;
import myapp.com.doit.helper.OpenCalls_List_Array;
import myapp.com.doit.helper.Problem_type_List_Array;
import myapp.com.doit.helper.RM_List_Array;
import myapp.com.doit.helper.Service_List_Array;
import myapp.com.doit.helper.Sub_type_List_Array;

/**
 * Created by zero on 7/11/15.
 */
public class PrefUtils {




    /* ------------ Inq_Login_List_Array  ------------------------------- */

    public static void set_Login_List_Array(Login_List_Array login_List_Array, Context context) {

        ComplexPreferences complexPreferences = ComplexPreferences.getComplexPreferences(context, "my_Login_List_Array", context.MODE_PRIVATE);
        complexPreferences.putObject("value_Login_List_Array", login_List_Array);
        complexPreferences.commit();

    }


    public static Login_List_Array get_Login_List_Array(Context context) {

        ComplexPreferences complexPreferences = ComplexPreferences.getComplexPreferences(context, "my_Login_List_Array", context.MODE_PRIVATE);
        Login_List_Array login_List_Array = complexPreferences.getObject("value_Login_List_Array", Login_List_Array.class);
        return login_List_Array;

    }


    public static void clear_Login_List_Array(Context context) {

        ComplexPreferences complexPreferences = ComplexPreferences.getComplexPreferences(context, "my_Login_List_Array", context.MODE_PRIVATE);
        complexPreferences.clearObject();
        complexPreferences.commit();

    }







    /* ------------ Service_List_Array  ------------------------------- */

    public static void set_service_List_Array(Service_List_Array service_List_Array, Context context) {

        ComplexPreferences complexPreferences = ComplexPreferences.getComplexPreferences(context, "my_service_List_Array", context.MODE_PRIVATE);
        complexPreferences.putObject("value_service_List_Array", service_List_Array);
        complexPreferences.commit();

    }


    public static Service_List_Array get_service_List_Array(Context context) {

        ComplexPreferences complexPreferences = ComplexPreferences.getComplexPreferences(context, "my_service_List_Array", context.MODE_PRIVATE);
        Service_List_Array service_List_Array = complexPreferences.getObject("value_service_List_Array", Service_List_Array.class);
        return service_List_Array;

    }


    public static void clear_service_List_Array(Context context) {

        ComplexPreferences complexPreferences = ComplexPreferences.getComplexPreferences(context, "my_service_List_Array", context.MODE_PRIVATE);
        complexPreferences.clearObject();
        complexPreferences.commit();

    }






    /* ------------ Service_List_Array  ------------------------------- */

    public static void set_openCalls_List_Array(OpenCalls_List_Array openCalls_List_Array, Context context) {

        ComplexPreferences complexPreferences = ComplexPreferences.getComplexPreferences(context, "my_openCalls_List_Array", context.MODE_PRIVATE);
        complexPreferences.putObject("value_openCalls_List_Array", openCalls_List_Array);
        complexPreferences.commit();

    }


    public static OpenCalls_List_Array get_openCalls_List_Array(Context context) {

        ComplexPreferences complexPreferences = ComplexPreferences.getComplexPreferences(context, "my_openCalls_List_Array", context.MODE_PRIVATE);
        OpenCalls_List_Array openCalls_List_Array = complexPreferences.getObject("value_openCalls_List_Array", OpenCalls_List_Array.class);
        return openCalls_List_Array;

    }


    public static void clear_openCalls_List_Array(Context context) {

        ComplexPreferences complexPreferences = ComplexPreferences.getComplexPreferences(context, "my_openCalls_List_Array", context.MODE_PRIVATE);
        complexPreferences.clearObject();
        complexPreferences.commit();

    }





    /* ------------ Notification_List_Array  ------------------------------- */

    public static void set_notification_List_Array(Notification_List_Array notification_List_Array, Context context) {

        ComplexPreferences complexPreferences = ComplexPreferences.getComplexPreferences(context, "my_notification_List_Array", context.MODE_PRIVATE);
        complexPreferences.putObject("value_notification_List_Array", notification_List_Array);
        complexPreferences.commit();

    }


    public static Notification_List_Array get_notification_List_Array(Context context) {

        ComplexPreferences complexPreferences = ComplexPreferences.getComplexPreferences(context, "my_notification_List_Array", context.MODE_PRIVATE);
        Notification_List_Array notification_List_Array = complexPreferences.getObject("value_notification_List_Array", Notification_List_Array.class);
        return notification_List_Array;

    }


    public static void clear_notification_List_Array(Context context) {

        ComplexPreferences complexPreferences = ComplexPreferences.getComplexPreferences(context, "my_notification_List_Array", context.MODE_PRIVATE);
        complexPreferences.clearObject();
        complexPreferences.commit();

    }




    /* ------------ User_Id  ------------------------------- */


    public static void set_User_Id(String User_Id, Context ctx) {

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(ctx);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("User_Id", User_Id);
        editor.commit();
    }


    public static String gete_User_Id(Context ctx) {

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(ctx);

        String User_Id = preferences.getString("User_Id", "");
        return User_Id;
    }



    /* ------------ Employee_IDF  ------------------------------- */


    public static void set_Employee_IDF(String Employee_IDF, Context ctx) {

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(ctx);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("Employee_IDF", Employee_IDF);
        editor.commit();
    }


    public static String gete_Employee_IDF(Context ctx) {

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(ctx);

        String Employee_IDF = preferences.getString("Employee_IDF", "");
        return Employee_IDF;
    }



    /* ------------ RoleId  ------------------------------- */


    public static void set_RoleId(String RoleId, Context ctx) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(ctx);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("RoleId", RoleId);
        editor.commit();
    }


    public static String gete_RoleId(Context ctx) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(ctx);
        String RoleId = preferences.getString("RoleId", "");
        return RoleId;
    }



    /* ------------ Created_By  ------------------------------- */


    public static void set_Created_By(String Created_By, Context ctx) {

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(ctx);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("Created_By", Created_By);
        editor.commit();
    }


    public static String gete_Created_By(Context ctx) {

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(ctx);

        String Created_By = preferences.getString("Created_By", "");
        return Created_By;
    }



    /* ------------ Amc_List_Array  ------------------------------- */

    public static void set_amc_List_Array(Amc_List_Array amc_List_Array, Context context) {

        ComplexPreferences complexPreferences = ComplexPreferences.getComplexPreferences(context, "my_amc_List_Array", context.MODE_PRIVATE);
        complexPreferences.putObject("value_service_List_Array", amc_List_Array);
        complexPreferences.commit();

    }


    public static Amc_List_Array get_amc_List_Array(Context context) {

        ComplexPreferences complexPreferences = ComplexPreferences.getComplexPreferences(context, "my_amc_List_Array", context.MODE_PRIVATE);
        Amc_List_Array amc_List_Array = complexPreferences.getObject("value_service_List_Array", Amc_List_Array.class);
        return amc_List_Array;

    }


    public static void clear_amc_List_Array(Context context) {

        ComplexPreferences complexPreferences = ComplexPreferences.getComplexPreferences(context, "my_amc_List_Array", context.MODE_PRIVATE);
        complexPreferences.clearObject();
        complexPreferences.commit();

    }







    /* ------------ Callid_List_Array  ------------------------------- */

    public static void set_callid_List_Array(Callid_List_Array callid_List_Array, Context context) {

        ComplexPreferences complexPreferences = ComplexPreferences.getComplexPreferences(context, "my_callid_List_Array", context.MODE_PRIVATE);
        complexPreferences.putObject("value_callid_List_Array", callid_List_Array);
        complexPreferences.commit();

    }


    public static Callid_List_Array get_callid_List_Array(Context context) {

        ComplexPreferences complexPreferences = ComplexPreferences.getComplexPreferences(context, "my_callid_List_Array", context.MODE_PRIVATE);
        Callid_List_Array callid_List_Array = complexPreferences.getObject("value_callid_List_Array", Callid_List_Array.class);
        return callid_List_Array;

    }


    public static void clear_callid_List_Array(Context context) {

        ComplexPreferences complexPreferences = ComplexPreferences.getComplexPreferences(context, "my_callid_List_Array", context.MODE_PRIVATE);
        complexPreferences.clearObject();
        complexPreferences.commit();

    }






    /* ------------ DoItTour_List_Array  ------------------------------- */

    public static void set_doItTour_List_Array(DoItTour_List_Array doItTour_List_Array, Context context) {

        ComplexPreferences complexPreferences = ComplexPreferences.getComplexPreferences(context, "my_doItTour_List_Array", context.MODE_PRIVATE);
        complexPreferences.putObject("value_doItTour_List_Array", doItTour_List_Array);
        complexPreferences.commit();

    }


    public static DoItTour_List_Array get_doItTour_List_Array(Context context) {

        ComplexPreferences complexPreferences = ComplexPreferences.getComplexPreferences(context, "my_notification_List_Array", context.MODE_PRIVATE);
        DoItTour_List_Array doItTour_List_Array = complexPreferences.getObject("value_doItTour_List_Array", DoItTour_List_Array.class);
        return doItTour_List_Array;

    }


    public static void clear_doItTour_List_Array(Context context) {

        ComplexPreferences complexPreferences = ComplexPreferences.getComplexPreferences(context, "my_doItTour_List_Array", context.MODE_PRIVATE);
        complexPreferences.clearObject();
        complexPreferences.commit();

    }







    /* ------------ Enquiry1_List_Array  ------------------------------- */

    public static void set_Enquiry1_List_Array(Enquiry1_List_Array enquiry1_List_Array, Context context) {

        ComplexPreferences complexPreferences = ComplexPreferences.getComplexPreferences(context, "my_Enquiry1_List_Array", context.MODE_PRIVATE);
        complexPreferences.putObject("value_enquiry1_List_Array", enquiry1_List_Array);
        complexPreferences.commit();

    }


    public static Enquiry1_List_Array get_Enquiry1_List_Array(Context context) {

        ComplexPreferences complexPreferences = ComplexPreferences.getComplexPreferences(context, "my_Enquiry1_List_Array", context.MODE_PRIVATE);
        Enquiry1_List_Array enquiry1_List_Array = complexPreferences.getObject("value_enquiry1_List_Array", Enquiry1_List_Array.class);
        return enquiry1_List_Array;

    }


    public static void clear_Enquiry1_List_Array(Context context) {

        ComplexPreferences complexPreferences = ComplexPreferences.getComplexPreferences(context, "my_Enquiry1_List_Array", context.MODE_PRIVATE);
        complexPreferences.clearObject();
        complexPreferences.commit();

    }








    /* ------------ Custmar_List_Array  ------------------------------- */

    public static void set_custmar_List_Array(Custmar_List_Array custmar_List_Array, Context context) {

        ComplexPreferences complexPreferences = ComplexPreferences.getComplexPreferences(context, "my_custmar_List_Array", context.MODE_PRIVATE);
        complexPreferences.putObject("value_custmar_List_Array", custmar_List_Array);
        complexPreferences.commit();

    }


    public static Custmar_List_Array get_custmar_List_Array(Context context) {

        ComplexPreferences complexPreferences = ComplexPreferences.getComplexPreferences(context, "my_custmar_List_Array", context.MODE_PRIVATE);
        Custmar_List_Array custmar_List_Array = complexPreferences.getObject("value_custmar_List_Array", Custmar_List_Array.class);
        return custmar_List_Array;

    }


    public static void clear_custmar_List_Array(Context context) {

        ComplexPreferences complexPreferences = ComplexPreferences.getComplexPreferences(context, "my_custmar_List_Array", context.MODE_PRIVATE);
        complexPreferences.clearObject();
        complexPreferences.commit();

    }



    /* ------------ RM_List_Array  ------------------------------- */

    public static void set_rm_List_Array(RM_List_Array rm_List_Array, Context context) {

        ComplexPreferences complexPreferences = ComplexPreferences.getComplexPreferences(context, "my_rm_List_Array", context.MODE_PRIVATE);
        complexPreferences.putObject("value_rm_List_Array", rm_List_Array);
        complexPreferences.commit();

    }


    public static RM_List_Array get_rm_List_Array(Context context) {

        ComplexPreferences complexPreferences = ComplexPreferences.getComplexPreferences(context, "my_rm_List_Array", context.MODE_PRIVATE);
        RM_List_Array rm_List_Array = complexPreferences.getObject("value_rm_List_Array", RM_List_Array.class);
        return rm_List_Array;

    }


    public static void clear_rm_List_Array(Context context) {

        ComplexPreferences complexPreferences = ComplexPreferences.getComplexPreferences(context, "my_rm_List_Array", context.MODE_PRIVATE);
        complexPreferences.clearObject();
        complexPreferences.commit();

    }








    /* ------------ Enquiry_type_List_Array  ------------------------------- */

    public static void set_enquiry_type_List_Array(Enquiry_type_List_Array enquiry_type_List_Array, Context context) {
        ComplexPreferences complexPreferences = ComplexPreferences.getComplexPreferences(context, "my_enquiry_type_List_Array", context.MODE_PRIVATE);
        complexPreferences.putObject("value_enquiry_type_List_Array", enquiry_type_List_Array);
        complexPreferences.commit();

    }


    public static Enquiry_type_List_Array get_enquiry_type_List_Array(Context context) {

        ComplexPreferences complexPreferences = ComplexPreferences.getComplexPreferences(context, "my_enquiry_type_List_Array", context.MODE_PRIVATE);
        Enquiry_type_List_Array rm_List_Array = complexPreferences.getObject("value_enquiry_type_List_Array", Enquiry_type_List_Array.class);
        return rm_List_Array;

    }


    public static void clear_enquiry_type_List_Array(Context context) {

        ComplexPreferences complexPreferences = ComplexPreferences.getComplexPreferences(context, "my_enquiry_type_List_Array", context.MODE_PRIVATE);
        complexPreferences.clearObject();
        complexPreferences.commit();

    }





    /* ------------ Ms_List_Array  ------------------------------- */

    public static void set_ms_List_Array(Ms_List_Array ms_List_Array, Context context) {

        ComplexPreferences complexPreferences = ComplexPreferences.getComplexPreferences(context, "my_ms_List_Array", context.MODE_PRIVATE);
        complexPreferences.putObject("value_ms_List_Array", ms_List_Array);
        complexPreferences.commit();

    }


    public static Ms_List_Array get_ms_List_Array(Context context) {

        ComplexPreferences complexPreferences = ComplexPreferences.getComplexPreferences(context, "my_ms_List_Array", context.MODE_PRIVATE);
        Ms_List_Array rm_List_Array = complexPreferences.getObject("value_ms_List_Array", Ms_List_Array.class);
        return rm_List_Array;

    }


    public static void clear_ms_List_Array(Context context) {

        ComplexPreferences complexPreferences = ComplexPreferences.getComplexPreferences(context, "my_ms_List_Array", context.MODE_PRIVATE);
        complexPreferences.clearObject();
        complexPreferences.commit();

    }








    /* ------------ MachineDetailList_List_Array  ------------------------------- */

    public static void set_machineDetailList_List_Array(MachineDetailList_List_Array machineDetailList_List_Array, Context context) {

        ComplexPreferences complexPreferences = ComplexPreferences.getComplexPreferences(context, "my_machineDetailList_List_Array", context.MODE_PRIVATE);
        complexPreferences.putObject("value_machineDetailList_List_Array", machineDetailList_List_Array);
        complexPreferences.commit();

    }


    public static MachineDetailList_List_Array get_machineDetailList_List_Array(Context context) {

        ComplexPreferences complexPreferences = ComplexPreferences.getComplexPreferences(context, "my_machineDetailList_List_Array", context.MODE_PRIVATE);
        MachineDetailList_List_Array service_List_Array = complexPreferences.getObject("value_machineDetailList_List_Array", MachineDetailList_List_Array.class);
        return service_List_Array;

    }


    public static void clear_machineDetailList_List_Array(Context context) {

        ComplexPreferences complexPreferences = ComplexPreferences.getComplexPreferences(context, "my_machineDetailList_List_Array", context.MODE_PRIVATE);
        complexPreferences.clearObject();
        complexPreferences.commit();

    }








    /* ------------ Problem_type_List_Array  ------------------------------- */

    public static void set_problem_type_List_Array(Problem_type_List_Array problem_type_List_Array, Context context) {

        ComplexPreferences complexPreferences = ComplexPreferences.getComplexPreferences(context, "my_problem_type_List_Array", context.MODE_PRIVATE);
        complexPreferences.putObject("value_problem_type_List_Array", problem_type_List_Array);
        complexPreferences.commit();

    }


    public static Problem_type_List_Array get_problem_type_List_Array(Context context) {

        ComplexPreferences complexPreferences = ComplexPreferences.getComplexPreferences(context, "my_problem_type_List_Array", context.MODE_PRIVATE);
        Problem_type_List_Array problem_type_List_Array = complexPreferences.getObject("value_problem_type_List_Array", Problem_type_List_Array.class);
        return problem_type_List_Array;

    }


    public static void clear_problem_type_List_Array(Context context) {

        ComplexPreferences complexPreferences = ComplexPreferences.getComplexPreferences(context, "my_problem_type_List_Array", context.MODE_PRIVATE);
        complexPreferences.clearObject();
        complexPreferences.commit();

    }






    /* ------------ Sub_type_List_Array  ------------------------------- */

    public static void set_sub_type_List_Array(Sub_type_List_Array sub_type_List_Array, Context context) {

        ComplexPreferences complexPreferences = ComplexPreferences.getComplexPreferences(context, "my_sub_type_List_Array", context.MODE_PRIVATE);
        complexPreferences.putObject("value_sub_type_List_Array", sub_type_List_Array);
        complexPreferences.commit();

    }


    public static Sub_type_List_Array get_sub_type_List_Array(Context context) {

        ComplexPreferences complexPreferences = ComplexPreferences.getComplexPreferences(context, "my_sub_type_List_Array", context.MODE_PRIVATE);
        Sub_type_List_Array sub_type_List_Array = complexPreferences.getObject("value_sub_type_List_Array", Sub_type_List_Array.class);
        return sub_type_List_Array;

    }


    public static void clear_sub_type_List_Array(Context context) {

        ComplexPreferences complexPreferences = ComplexPreferences.getComplexPreferences(context, "my_sub_type_List_Array", context.MODE_PRIVATE);
        complexPreferences.clearObject();
        complexPreferences.commit();

    }


    /* ------------ RoleID  ------------------------------- */


    public static void set_RoleID(String RoleID, Context ctx) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(ctx);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("RoleID", RoleID);
        editor.commit();
    }


    public static String gete_RoleID(Context ctx) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(ctx);
        String RoleID = preferences.getString("RoleID", "");
        return RoleID;
    }


    public static void setRoleName(String RoleID, Context ctx) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(ctx);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("RoleName", RoleID);
        editor.commit();
    }


    public static String getRoleName(Context ctx) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(ctx);
        return preferences.getString("RoleName", "");
    }



}






