package myapp.com.doit;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import myapp.com.doit.dashboard.ui.AddNewEnquiry;
import myapp.com.doit.dashboard.ui.CallStatusReportListAct;
import myapp.com.doit.dashboard.ui.EnquiryListAct;
import myapp.com.doit.dashboard.ui.MyProduct;
import myapp.com.doit.prelogin.ui.Login;
import myapp.com.doit.utils.PrefUtils;
import myapp.com.doit.rest.RetrfitInterface;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedByteArray;

public class AdminDashboardActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    String t;
    LinearLayout main_1, main_2, lay1, lay2, lay3, lay4, lay5;
    ImageView layouts;
    CardView card1, card2, canav_call_statusrd2, card3, card4, card5;
    String status, message;
    private ProgressDialog loading;
    TextView Service_count, Complaintenq_count, Opencall_count, Close_count, Notification_count, u_name;
    String ID;
    FrameLayout ff1, ff2, ff3, ff4, ff5;
    LinearLayout Add_inq;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        u_name = (TextView) findViewById(R.id.u_name);

        ID = PrefUtils.gete_Employee_IDF(AdminDashboardActivity.this);

        Service_count = (TextView) findViewById(R.id.Service_count);
        Complaintenq_count = (TextView) findViewById(R.id.Complaintenq_count);
        Opencall_count = (TextView) findViewById(R.id.Opencall_count);
        Close_count = (TextView) findViewById(R.id.Close_count);
        Notification_count = (TextView) findViewById(R.id.Notification_count);


        Add_inq = (LinearLayout) findViewById(R.id.Add_inq);
        Add_inq.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Intent it = new Intent(AdminDashboardActivity.this, Addnew_Enqury.class);
                Intent it = new Intent(AdminDashboardActivity.this, AddNewEnquiry.class);
                it.putExtra("keyName", "0");
                startActivity(it);
            }
        });


        t = "0";

        main_1 = (LinearLayout) findViewById(R.id.main_1);
        main_2 = (LinearLayout) findViewById(R.id.main_2);

        lay1 = (LinearLayout) findViewById(R.id.lay1);
        lay2 = (LinearLayout) findViewById(R.id.lay2);
        lay3 = (LinearLayout) findViewById(R.id.lay3);
        lay4 = (LinearLayout) findViewById(R.id.lay4);
        lay5 = (LinearLayout) findViewById(R.id.lay5);

        card1 = (CardView) findViewById(R.id.card1);
        card2 = (CardView) findViewById(R.id.card2);
        card3 = (CardView) findViewById(R.id.card3);
        card4 = (CardView) findViewById(R.id.card4);
        card5 = (CardView) findViewById(R.id.card5);


        ff1 = (FrameLayout) findViewById(R.id.ff1);
        ff2 = (FrameLayout) findViewById(R.id.ff2);
        ff3 = (FrameLayout) findViewById(R.id.ff3);
        ff4 = (FrameLayout) findViewById(R.id.ff4);
        ff5 = (FrameLayout) findViewById(R.id.ff5);


        ff1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                openv();

            }
        });


        lay1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openv();
            }
        });

        card1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                openv();

            }
        });


        lay2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openv2();
            }
        });

        card2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                openv2();

            }
        });

        ff2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                openv2();

            }
        });


        lay3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openv3();
            }
        });

        card3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                openv3();

            }
        });

        ff3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                openv3();

            }
        });


        lay4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openv4();
            }
        });

        card4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                openv4();

            }
        });

        ff4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                openv4();

            }
        });


        lay5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openv5();
            }
        });

        card5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                openv5();

            }
        });

        ff5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                openv5();

            }
        });


        layouts = (ImageView) findViewById(R.id.layouts);

        layouts.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //  Toast.makeText(AdminDashboardActivity.this,"hello",Toast.LENGTH_LONG).show();


                if (t.equalsIgnoreCase("0")) {

                    main_1.setVisibility(View.GONE);
                    main_2.setVisibility(View.VISIBLE);

                    t = "1";
                } else if (t.equalsIgnoreCase("1")) {

                    main_1.setVisibility(View.VISIBLE);
                    main_2.setVisibility(View.GONE);

                    t = "0";
                }


            }
        });


        main_1.setVisibility(View.GONE);
        main_2.setVisibility(View.VISIBLE);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);


        Get_ServiceCount();
    }

    private void openv() {


        Intent it = new Intent(AdminDashboardActivity.this, Second.class);
        it.putExtra("keyName", "0");
        startActivity(it);
    }

    private void openv2() {


        Intent it = new Intent(AdminDashboardActivity.this, Complaint_Activity.class);
        it.putExtra("keyName", "0");
        startActivity(it);
    }

    private void openv3() {


        Intent it = new Intent(AdminDashboardActivity.this, OpenCall_Activity.class);
        it.putExtra("keyName", "0");
        startActivity(it);
    }

    private void openv4() {


        Intent it = new Intent(AdminDashboardActivity.this, CloseCall_Activity.class);
        it.putExtra("keyName", "0");
        startActivity(it);
    }

    private void openv5() {


        Intent it = new Intent(AdminDashboardActivity.this, Notification_Activity.class);
        it.putExtra("keyName", "0");
        startActivity(it);
    }


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            new AlertDialog.Builder(this)
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .setTitle("Closing Activity")
                    .setMessage("Are you sure you want to close this activity?")
                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            Intent homeIntent = new Intent(Intent.ACTION_MAIN);
                            homeIntent.addCategory(Intent.CATEGORY_HOME);
                            homeIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(homeIntent);
                        }

                    })
                    .setNegativeButton("No", null)
                    .show();
        }
    }

//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.main, menu);
//        return true;
//    }
//
//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        // Handle action bar item clicks here. The action bar will
//        // automatically handle clicks on the Home/Up button, so long
//        // as you specify a parent activity in AndroidManifest.xml.
//        int id = item.getItemId();
//
//        //noinspection SimplifiableIfStatement
//
//
//        if(t==0){
//
//            main_1.setVisibility(View.GONE);
//            main_2.setVisibility(View.VISIBLE);
//
//            t = 1;
//        }
//        if(t==1){
//
//            main_1.setVisibility(View.VISIBLE);
//            main_2.setVisibility(View.GONE);
//
//            t = 0;
//        }
//        if (id == R.id.action_settings) {
//
//
//
//
//            return true;
//        }
//
//        return super.onOptionsItemSelected(item);
//    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_camera) {
            // Handle the camera action

            Intent it = new Intent(AdminDashboardActivity.this, My_Profile.class);
            it.putExtra("keyName", "0");
            startActivity(it);

        } else if (id == R.id.nav_gallery) {

            Intent it = new Intent(AdminDashboardActivity.this, MyProduct.class);
            it.putExtra("keyName", "0");
            startActivity(it);

        } else if (id == R.id.nav_slideshow) {

            Intent it = new Intent(AdminDashboardActivity.this, EnquiryListAct.class);
            it.putExtra("keyName", "0");
            startActivity(it);

        } else if (id == R.id.nav_call_status) {
            Intent it = new Intent(AdminDashboardActivity.this, CallStatusReportListAct.class);
            it.putExtra("keyName", "0");
            startActivity(it);
        } else if (id == R.id.nav_manage) {

            Intent it = new Intent(AdminDashboardActivity.this, Feedback.class);
            it.putExtra("keyName", "0");
            startActivity(it);

        } else if (id == R.id.nav_DoItTour) {

            Intent it = new Intent(AdminDashboardActivity.this, DoItTour_Activity.class);
            it.putExtra("keyName", "0");
            startActivity(it);

        } else if (id == R.id.nav_about_us) {

            Intent it = new Intent(AdminDashboardActivity.this, Aboutus.class);
            it.putExtra("keyName", "0");
            startActivity(it);


        } else if (id == R.id.nav_contact_us) {

            Intent it = new Intent(AdminDashboardActivity.this, Contus.class);
            it.putExtra("keyName", "0");
            startActivity(it);


        } else if (id == R.id.nav_logout) {

            AlertDialog.Builder alertDialog = new AlertDialog.Builder(AdminDashboardActivity.this);

            alertDialog.setTitle("Logout"); // Sets title for your alertbox

            alertDialog.setMessage("Are you sure you want to Logout ?"); // Message to be displayed on alertbox

            alertDialog.setIcon(R.drawable.power); // Icon for your alertbox

            /* When positive (yes/ok) is clicked */
            alertDialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {

                    PrefUtils.set_User_Id("", AdminDashboardActivity.this);

                    /*Intent homeIntent = new Intent(Intent.ACTION_MAIN);
                    homeIntent.addCategory(Intent.CATEGORY_HOME);
                    homeIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(homeIntent);

                    android.os.Process.killProcess(android.os.Process.myPid());
                    setResult(RESULT_OK, new Intent().putExtra("EXIT", true));
                    Intent a = new Intent(Intent.ACTION_MAIN);
                    a.addCategory(Intent.CATEGORY_HOME);
                    a.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(a);
                    AdminDashboardActivity.super.onBackPressed();*/
                    startActivity(new Intent(AdminDashboardActivity.this, Login.class));
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                        finishAffinity();
                    }else
                        finish();
                    Toast.makeText(AdminDashboardActivity.this, "Successfully Logged Out", Toast.LENGTH_LONG).show();
                }
            });

            /* When negative (No/cancel) button is clicked*/
            alertDialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                }
            });
            alertDialog.show();

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


    private void Get_ServiceCount() {


        RestAdapter adapter = new RestAdapter.Builder().setEndpoint(RetrfitInterface.url).build();

        try {
            loading = new ProgressDialog(AdminDashboardActivity.this);
            loading.setMessage("Please Wait Loading data ....");
            loading.show();
            loading.setCancelable(false);

        } catch (Exception e) {

        }

        //Creating Rest Services
        final RetrfitInterface restInterface = adapter.create(RetrfitInterface.class);
        Log.d("status&&&", "stat1" + restInterface);


//        Log.e("status&&&", "stat1:====  " + PrefUtils.get_Login_List_Array(AdminDashboardActivity.this).getTncsub_list());


        restInterface.Get_ServiceCount(ID, new Callback<Response>()

        {

            @Override
            public void success(Response findChildArray, Response response) {

                Log.e("status", new String(((TypedByteArray) findChildArray.getBody()).getBytes()));

                String result1 = new String(((TypedByteArray) findChildArray.getBody()).getBytes());

                JSONObject jObj = null;
                try {
                    jObj = new JSONObject(result1);

                    Log.e("Size", "" + jObj.length());


                    JSONArray values = jObj.getJSONArray("result");

                    if (jObj.length() > 0) {


                        for (int i = 0; i < values.length(); i++) {

                            JSONObject jsonObj = values.getJSONObject(i);

                            String ServiceEnquiry = jsonObj.getString("ServiceEnquiry");

                            // Toast.makeText(AdminDashboardActivity.this, "Incorrect: =====  "+ServiceEnquiry, Toast.LENGTH_SHORT).show();
                            Service_count.setText("" + ServiceEnquiry);

                        }

                    } else {


                        Toast.makeText(AdminDashboardActivity.this, "Username or Password Incorrect", Toast.LENGTH_SHORT).show();

                    }

                    Get_Complaintenq();

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                // loading.dismiss();

            }

            @Override
            public void failure(RetrofitError error) {

                String merror = error.getMessage();
                Log.d("error", merror);
                Toast.makeText(AdminDashboardActivity.this, merror, Toast.LENGTH_LONG).show();

            }
        });


    }

    private void Get_Complaintenq() {


        RestAdapter adapter = new RestAdapter.Builder().setEndpoint(RetrfitInterface.url).build();

        try {
//            loading = new ProgressDialog(AdminDashboardActivity.this);
//            loading.setMessage("Please Wait Loading data ....");
//            loading.show();
//            loading.setCancelable(false);

        } catch (Exception e) {

        }

        //Creating Rest Services
        final RetrfitInterface restInterface = adapter.create(RetrfitInterface.class);
        Log.d("status&&&", "stat1" + restInterface);


//        Log.e("status&&&", "stat1:====  " + PrefUtils.get_Login_List_Array(AdminDashboardActivity.this).getTncsub_list());


        restInterface.Get_GetComplaintCount(ID, new Callback<Response>()

        {

            @Override
            public void success(Response findChildArray, Response response) {

                Log.e("status", new String(((TypedByteArray) findChildArray.getBody()).getBytes()));

                String result1 = new String(((TypedByteArray) findChildArray.getBody()).getBytes());

                JSONObject jObj = null;
                try {
                    jObj = new JSONObject(result1);

                    Log.e("Size", "" + jObj.length());


                    JSONArray values = jObj.getJSONArray("result");

                    if (jObj.length() > 0) {


                        for (int i = 0; i < values.length(); i++) {

                            JSONObject jsonObj = values.getJSONObject(i);

                            String ComplaintEnquiry = jsonObj.getString("ComplaintEnquiry");

                            // Toast.makeText(AdminDashboardActivity.this, "Incorrect: =====  "+ServiceEnquiry, Toast.LENGTH_SHORT).show();
                            Complaintenq_count.setText("" + ComplaintEnquiry);

                        }

                    } else {


                        Toast.makeText(AdminDashboardActivity.this, "Username or Password Incorrect", Toast.LENGTH_SHORT).show();

                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }


                Get_Opencall_count();
                // loading.dismiss();

            }

            @Override
            public void failure(RetrofitError error) {

                String merror = error.getMessage();
                Log.d("error", merror);
                Toast.makeText(AdminDashboardActivity.this, merror, Toast.LENGTH_LONG).show();

            }
        });


    }

    private void Get_Opencall_count() {


        RestAdapter adapter = new RestAdapter.Builder().setEndpoint(RetrfitInterface.url).build();

        try {
//            loading = new ProgressDialog(AdminDashboardActivity.this);
//            loading.setMessage("Please Wait Loading data ....");
//            loading.show();
//            loading.setCancelable(false);

        } catch (Exception e) {

        }

        //Creating Rest Services
        final RetrfitInterface restInterface = adapter.create(RetrfitInterface.class);
        Log.d("status&&&", "stat1" + restInterface);


//        Log.e("status&&&", "stat1:====  " + PrefUtils.get_Login_List_Array(AdminDashboardActivity.this).getTncsub_list());


        restInterface.Get_GetOpenCallsCount(ID, PrefUtils.gete_RoleId(AdminDashboardActivity.this), new Callback<Response>()

        {

            @Override
            public void success(Response findChildArray, Response response) {

                Log.e("status", new String(((TypedByteArray) findChildArray.getBody()).getBytes()));

                String result1 = new String(((TypedByteArray) findChildArray.getBody()).getBytes());

                JSONObject jObj = null;
                try {
                    jObj = new JSONObject(result1);

                    Log.e("Size", "" + jObj.length());


                    JSONArray values = jObj.getJSONArray("result");

                    if (jObj.length() > 0) {


                        for (int i = 0; i < values.length(); i++) {

                            JSONObject jsonObj = values.getJSONObject(i);

                            String OpenCalls = jsonObj.getString("OpenCalls");

                            // Toast.makeText(AdminDashboardActivity.this, "Incorrect: =====  "+ServiceEnquiry, Toast.LENGTH_SHORT).show();
                            Opencall_count.setText("" + OpenCalls);

                        }

                    } else {


                        Toast.makeText(AdminDashboardActivity.this, "Username or Password Incorrect", Toast.LENGTH_SHORT).show();

                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }

                GetCloseCallsCount();

            }

            @Override
            public void failure(RetrofitError error) {

                String merror = error.getMessage();
                Log.d("error", merror);
                Toast.makeText(AdminDashboardActivity.this, merror, Toast.LENGTH_LONG).show();

            }
        });


    }

    private void GetCloseCallsCount() {


        RestAdapter adapter = new RestAdapter.Builder().setEndpoint(RetrfitInterface.url).build();

        try {
//            loading = new ProgressDialog(AdminDashboardActivity.this);
//            loading.setMessage("Please Wait Loading data ....");
//            loading.show();
//            loading.setCancelable(false);

        } catch (Exception e) {

        }

        //Creating Rest Services
        final RetrfitInterface restInterface = adapter.create(RetrfitInterface.class);
        Log.d("status&&&", "stat1" + restInterface);


//        Log.e("status&&&", "stat1:====  " + PrefUtils.get_Login_List_Array(AdminDashboardActivity.this).getTncsub_list());


        restInterface.GetCloseCallsCount(ID, PrefUtils.gete_RoleId(AdminDashboardActivity.this), new Callback<Response>()

        {

            @Override
            public void success(Response findChildArray, Response response) {

                Log.e("status", new String(((TypedByteArray) findChildArray.getBody()).getBytes()));

                String result1 = new String(((TypedByteArray) findChildArray.getBody()).getBytes());

                JSONObject jObj = null;
                try {
                    jObj = new JSONObject(result1);

                    Log.e("Size", "" + jObj.length());


                    JSONArray values = jObj.getJSONArray("result");

                    if (jObj.length() > 0) {


                        for (int i = 0; i < values.length(); i++) {

                            JSONObject jsonObj = values.getJSONObject(i);

                            String CloseCalls = jsonObj.getString("CloseCalls");

                            // Toast.makeText(AdminDashboardActivity.this, "Incorrect: =====  "+ServiceEnquiry, Toast.LENGTH_SHORT).show();
                            Close_count.setText("" + CloseCalls);

                        }

                    } else {


                        Toast.makeText(AdminDashboardActivity.this, "Username or Password Incorrect", Toast.LENGTH_SHORT).show();

                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }

                GetNotificationCount();

            }

            @Override
            public void failure(RetrofitError error) {

                String merror = error.getMessage();
                Log.d("error", merror);
                Toast.makeText(AdminDashboardActivity.this, merror, Toast.LENGTH_LONG).show();

            }
        });


    }

    private void GetNotificationCount() {


        RestAdapter adapter = new RestAdapter.Builder().setEndpoint(RetrfitInterface.url).build();

        try {
//            loading = new ProgressDialog(AdminDashboardActivity.this);
//            loading.setMessage("Please Wait Loading data ....");
//            loading.show();
//            loading.setCancelable(false);

        } catch (Exception e) {

        }

        //Creating Rest Services
        final RetrfitInterface restInterface = adapter.create(RetrfitInterface.class);
        Log.d("status&&&", "stat1" + restInterface);


//        Log.e("status&&&", "stat1:====  " + PrefUtils.get_Login_List_Array(AdminDashboardActivity.this).getTncsub_list());


        restInterface.Get_NotificationCount(ID, new Callback<Response>()

        {

            @Override
            public void success(Response findChildArray, Response response) {

                Log.e("status", new String(((TypedByteArray) findChildArray.getBody()).getBytes()));

                String result1 = new String(((TypedByteArray) findChildArray.getBody()).getBytes());

                JSONObject jObj = null;
                try {
                    jObj = new JSONObject(result1);

                    Log.e("Size", "" + jObj.length());


                    JSONArray values = jObj.getJSONArray("result");

                    if (jObj.length() > 0) {


                        for (int i = 0; i < values.length(); i++) {

                            JSONObject jsonObj = values.getJSONObject(i);

                            String NotificationCount = jsonObj.getString("NotificationCount");

                            // Toast.makeText(AdminDashboardActivity.this, "Incorrect: =====  "+ServiceEnquiry, Toast.LENGTH_SHORT).show();
                            Notification_count.setText("" + NotificationCount);

                        }

                    } else {


                        Toast.makeText(AdminDashboardActivity.this, "Username or Password Incorrect", Toast.LENGTH_SHORT).show();

                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }

                loading.dismiss();

            }

            @Override
            public void failure(RetrofitError error) {

                String merror = error.getMessage();
                Log.d("error", merror);
                Toast.makeText(AdminDashboardActivity.this, merror, Toast.LENGTH_LONG).show();

            }
        });


    }


}
