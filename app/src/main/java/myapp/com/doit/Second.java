package myapp.com.doit;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.GsonBuilder;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import myapp.com.doit.utils.PrefUtils;
import myapp.com.doit.rest.RetrfitInterface;
import myapp.com.doit.helper.Service_List;
import myapp.com.doit.helper.Service_List_Array;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedByteArray;


public class Second extends AppCompatActivity {

    LinearLayout btn_Login, login;
    String status, message;
    private ProgressDialog loading;
    ListView list_view;
    ImageView left_arrow;
    TextView title_;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.second);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        list_view  = (ListView) findViewById(R.id.list_view);


        title_ = (TextView) findViewById(R.id.title_);
        title_.setText("Service");


        left_arrow = (ImageView) findViewById(R.id.left_arrow);
        left_arrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                Intent it = new Intent(Second.this, AdminDashboardActivity.class);
                it.putExtra("keyName", "0");
                startActivity(it);

            }
        });


        Get_ServiceList();
    }

//    @Override
//    public void onBackPressed() {
//
//        moveTaskToBack(true);
//        android.os.Process.killProcess(android.os.Process.myPid());
//        System.exit(1);
//
//        finish();
//
//    }


    private void Get_ServiceList() {


        RestAdapter adapter = new RestAdapter.Builder().setEndpoint(RetrfitInterface.url).build();

        try {
            loading = new ProgressDialog(Second.this);
            loading.setMessage("Please Wait Loading data ....");
            loading.show();
            loading.setCancelable(false);

        } catch (Exception e) {

        }

        //Creating Rest Services
        final RetrfitInterface restInterface = adapter.create(RetrfitInterface.class);
        //Log.d("status&&&", "stat1" + restInterface);


       // Log.e("status&&&", "stat1:====  " + PrefUtils.get_Login_List_Array(Second.this).getTncsub_list());


        restInterface.Get_ServiceList(PrefUtils.gete_Employee_IDF(Second.this), new Callback<Response>()

        {

            @Override
            public void success(Response findChildArray, Response response) {

                Log.e("status", new String(((TypedByteArray) findChildArray.getBody()).getBytes()));

                String result1 = new String(((TypedByteArray) findChildArray.getBody()).getBytes());

                JSONObject jObj = null;
                try {
                    jObj = new JSONObject(result1);

                    Log.e("Size", "" + jObj.length());


                    if (jObj.length() > 0) {

                        JSONArray values = jObj.getJSONArray("result");


                        Service_List_Array service_List_Array = new GsonBuilder().create().fromJson(result1, Service_List_Array.class);
                        PrefUtils.set_service_List_Array(service_List_Array, Second.this);


                        Adapter_Service adapter_Service = new Adapter_Service(Second.this, service_List_Array.getResult());

                        list_view.setAdapter(adapter_Service);


                    } else {


                        Toast.makeText(Second.this, "Username or Password Incorrect", Toast.LENGTH_SHORT).show();

                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }

                loading.dismiss();

            }

            @Override
            public void failure(RetrofitError error) {

                String merror = error.getMessage();
                Log.d("error", merror);
                Toast.makeText(Second.this, merror, Toast.LENGTH_LONG).show();

            }
        });


    }


    public class Adapter_Service extends BaseAdapter {


        ArrayList<Service_List> result;
        Context context;

        private LayoutInflater inflater = null;

        public Adapter_Service(Second second, ArrayList<Service_List> details) {
            // TODO Auto-generated constructor stub
            result = details;
            context = second;

        }




        /*private view holder class*/
        private class ViewHolder {

            TextView txt_Code, txt_Date,txt_ItemDesc;



        }

        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder holder = null;

            LayoutInflater mInflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);

            if (convertView == null) {
                convertView = mInflater.inflate(R.layout.service_row, null);
                holder = new ViewHolder();

                holder.txt_Code = (TextView) convertView.findViewById(R.id.txt_Code);
                holder.txt_Date = (TextView) convertView.findViewById(R.id.txt_Date);
                holder.txt_ItemDesc = (TextView) convertView.findViewById(R.id.txt_ItemDesc);



                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }


            holder.txt_Code.setText(result.get(position).getEnqNo());
            holder.txt_Date.setText(result.get(position).getEnqdate());
            holder.txt_ItemDesc.setText(result.get(position).getItemDesc());




            return convertView;
        }

        @Override
        public int getCount() {
            return result.size();
        }

        @Override
        public Object getItem(int position) {
            return result.get(position);
        }

        @Override
        public long getItemId(int position) {
            return result.indexOf(getItem(position));
        }
    }


}
