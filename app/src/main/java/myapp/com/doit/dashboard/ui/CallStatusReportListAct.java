package myapp.com.doit.dashboard.ui;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.GsonBuilder;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;

import myapp.com.doit.Addnew_Enqury;
import myapp.com.doit.R;
import myapp.com.doit.base.BaseActivity;
import myapp.com.doit.dashboard.adapter.CallStatusListAdapter;
import myapp.com.doit.dashboard.adapter.EnquiryListAdapter;
import myapp.com.doit.dashboard.model.CallStatusListPojo;
import myapp.com.doit.databinding.ActCallStatusReportBinding;
import myapp.com.doit.helper.Enquiry1_List;
import myapp.com.doit.helper.Enquiry1_List_Array;
import myapp.com.doit.utils.PrefUtils;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedByteArray;


public class CallStatusReportListAct extends BaseActivity {

    private ProgressDialog loading;
    ActCallStatusReportBinding mBinding;
    private static final String TAG = "CallStatusReportListAct";

    @Override
    protected void initView() {
        mBinding.flDateFrom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openDatePicker(0);
            }
        });
        mBinding.flDateTo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openDatePicker(1);
            }
        });
        mBinding.btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(TextUtils.isEmpty(mBinding.tvDateFrom.getText().toString()) || TextUtils.isEmpty(mBinding.tvDateFrom.getText().toString())){
                    Toast.makeText(CallStatusReportListAct.this, "Select Date", Toast.LENGTH_SHORT).show();
                    return;
                }
                Get_ServiceList();
            }
        });
        mBinding.leftArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.act_call_status_report);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        initView();
    }


    private void Get_ServiceList() {
        try {
            loading = new ProgressDialog(CallStatusReportListAct.this);
            loading.setMessage("Please Wait Loading data ....");
            loading.show();
            loading.setCancelable(false);
        } catch (Exception e) { }

        Log.e(TAG, "Idf code "+PrefUtils.gete_Employee_IDF(CallStatusReportListAct.this));

        getRestrofitInterface().GetCallststus(""+mBinding.tvDateFrom.getText().toString(),
                ""+mBinding.tvDateTo.getText().toString(),
                ""+ PrefUtils.gete_Employee_IDF(CallStatusReportListAct.this), new Callback<Response>() {
            @Override
            public void success(Response findChildArray, Response response) {
                String result1 = new String(((TypedByteArray) findChildArray.getBody()).getBytes());
                Log.e("Response", ""+result1);
                CallStatusListPojo callStatusListPojo = new GsonBuilder().create().fromJson(result1, CallStatusListPojo.class);

                CallStatusListAdapter callStatusListAdapter = new CallStatusListAdapter(getApplicationContext(), callStatusListPojo.getResult());
                mBinding.listView.setAdapter(callStatusListAdapter);

                loading.dismiss();
            }

            @Override
            public void failure(RetrofitError error) {
                loading.dismiss();
                String merror = error.getMessage();
                Log.d("error", merror);
                Toast.makeText(CallStatusReportListAct.this, merror, Toast.LENGTH_LONG).show();
            }
        });
    }

    private void openDatePicker(final int type) {
        Calendar calendar = Calendar.getInstance();
        DatePickerDialog dialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int year, int monthOfYear, int dayOfMonth) {
                int month = monthOfYear + 1;
                //String mDate = dayOfMonth + "/" + month + "/" + year;
                String mDate = month + "/" + dayOfMonth + "/" + year;
                if(type == 0) {
                    //mBinding.tvDateFrom.setText(DateUtilz.changeDateFormat(DateUtilz.SERVER_SEND_DATE_FORMAT, DateUtilz.SERVER_SEND_DATE_FORMAT, mDate));
                    mBinding.tvDateFrom.setText(mDate);
                } else {
                    //mBinding.tvDateTo.setText(DateUtilz.changeDateFormat(DateUtilz.SERVER_SEND_DATE_FORMAT, DateUtilz.SERVER_SEND_DATE_FORMAT, mDate));
                    mBinding.tvDateTo.setText(mDate);
                }
            }
        }, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));

        //dialog.getDatePicker().setMaxDate(System.currentTimeMillis());
        if (type == 0 && !TextUtils.isEmpty(mBinding.tvDateFrom.getText().toString()))
            dialog.updateDate(Integer.parseInt(mBinding.tvDateFrom.getText().toString().split("/")[2]),
                    Integer.parseInt(mBinding.tvDateFrom.getText().toString().split("/")[1]) - 1,
                    Integer.parseInt(mBinding.tvDateFrom.getText().toString().split("/")[0]));
        else if (type == 1 && !TextUtils.isEmpty(mBinding.tvDateTo.getText().toString()))
            dialog.updateDate(Integer.parseInt(mBinding.tvDateTo.getText().toString().split("/")[2]),
                    Integer.parseInt(mBinding.tvDateTo.getText().toString().split("/")[1]) - 1,
                    Integer.parseInt(mBinding.tvDateTo.getText().toString().split("/")[0]));
        dialog.show();
    }


}
