package myapp.com.doit.dashboard.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import myapp.com.doit.R;
import myapp.com.doit.dashboard.model.CallStatusListPojo;
import myapp.com.doit.dashboard.ui.CallStatusReportListAct;
import myapp.com.doit.dashboard.ui.EnquiryListAct;
import myapp.com.doit.helper.Enquiry1_List;

public class CallStatusListAdapter extends BaseAdapter {

        ArrayList<CallStatusListPojo.CallStatus> result;
        Context context;

        private LayoutInflater inflater = null;

        public CallStatusListAdapter(Context second, ArrayList<CallStatusListPojo.CallStatus> details) {
            result = details;
            context = second;
        }

        /*private view holder class*/
        private class ViewHolder {
            TextView txt_CustName,
                    txtEnqDesc,
                    txtSerNo,
                    txtEnqName,
                    txtCallStatus,
                    txtCallAssignDate,
                    txtRemark,
                    txtCallComplete;
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder holder = null;

            LayoutInflater mInflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);

            if (convertView == null) {
                convertView = mInflater.inflate(R.layout.row_call_status, null);
                holder = new ViewHolder();

                holder.txt_CustName = (TextView) convertView.findViewById(R.id.txt_CustName);
                holder.txtEnqDesc = (TextView) convertView.findViewById(R.id.txt_enq_desc);
                holder.txtSerNo = (TextView) convertView.findViewById(R.id.txt_serial);
                holder.txtEnqName = (TextView) convertView.findViewById(R.id.txt_eng_name);
                holder.txtCallStatus = (TextView) convertView.findViewById(R.id.txt_call_status);
                holder.txtCallAssignDate = (TextView) convertView.findViewById(R.id.txt_call_assign_date);
                holder.txtRemark = (TextView) convertView.findViewById(R.id.txt_eng_remark);
                holder.txtCallComplete = (TextView) convertView.findViewById(R.id.txt_call_cpltdate);

                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }

            holder.txt_CustName.setText(result.get(position).getCustName());
            holder.txtEnqDesc.setText(result.get(position).getEnqDesc());
            holder.txtSerNo.setText(result.get(position).getSerial());
            holder.txtEnqName.setText(result.get(position).getEngName());
            holder.txtCallStatus.setText(result.get(position).getCallStatuss());
            holder.txtCallAssignDate.setText(result.get(position).getCallCompleteDt());
            holder.txtRemark.setText(result.get(position).getEngRemark());
            holder.txtCallComplete.setText(result.get(position).getCallCompleteDt());

            return convertView;
        }

        @Override
        public int getCount() {
            return result.size();
        }

        @Override
        public Object getItem(int position) {
            return result.get(position);
        }

        @Override
        public long getItemId(int position) {
            return result.indexOf(getItem(position));
        }
    }