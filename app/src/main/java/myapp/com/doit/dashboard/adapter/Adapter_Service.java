package myapp.com.doit.dashboard.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import java.util.ArrayList;
import myapp.com.doit.R;
import myapp.com.doit.dashboard.ui.MyProduct;
import myapp.com.doit.helper.Amc_List;

public class Adapter_Service extends BaseAdapter {

        ArrayList<Amc_List> result;
        Context context;

        private LayoutInflater inflater = null;

        public Adapter_Service(MyProduct second, ArrayList<Amc_List> details) {
            result = details;
            context = second;
        }

        /*private view holder class*/
        private class ViewHolder {
            TextView txt_Itemname,txt_SerialNo,txt_war_exp, txt_amcexp, txt_upcoming;
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder holder = null;

            LayoutInflater mInflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);

            if (convertView == null) {
                convertView = mInflater.inflate(R.layout.myprow_row, null);
                holder = new ViewHolder();

                holder.txt_Itemname = (TextView) convertView.findViewById(R.id.txt_Itemname);
                holder.txt_SerialNo = (TextView) convertView.findViewById(R.id.txt_SerialNo);
                holder.txt_war_exp = (TextView) convertView.findViewById(R.id.txt_war_exp);
                holder.txt_amcexp = (TextView) convertView.findViewById(R.id.txt_amcexp);
                holder.txt_upcoming = (TextView) convertView.findViewById(R.id.txt_upcoming);

                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }

            holder.txt_Itemname.setText(result.get(position).getItemname());
            holder.txt_SerialNo.setText(result.get(position).getSerialNo());
            holder.txt_war_exp.setText(result.get(position).getWarrantyExpiredate());
            holder.txt_amcexp.setText(result.get(position).getAMCExpireDate());
            holder.txt_upcoming.setText(result.get(position).getUpcomingDate());

            return convertView;
        }

        @Override
        public int getCount() {
            return result.size();
        }

        @Override
        public Object getItem(int position) {
            return result.get(position);
        }

        @Override
        public long getItemId(int position) {
            return result.indexOf(getItem(position));
        }
    }