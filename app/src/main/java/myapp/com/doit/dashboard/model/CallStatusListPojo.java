package myapp.com.doit.dashboard.model;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CallStatusListPojo {

    @SerializedName("result")
    @Expose
    private ArrayList<CallStatus> result = null;

    public ArrayList<CallStatus> getResult() {
        return result;
    }

    public void setResult(ArrayList<CallStatus> result) {
        this.result = result;
    }

    public class CallStatus {

        @SerializedName("CallId")
        @Expose
        private Integer callId;
        @SerializedName("CallRepNo")
        @Expose
        private String callRepNo;
        @SerializedName("CallRepDt")
        @Expose
        private String callRepDt;
        @SerializedName("CustId")
        @Expose
        private Integer custId;
        @SerializedName("CustName")
        @Expose
        private String custName;
        @SerializedName("ItemId")
        @Expose
        private Integer itemId;
        @SerializedName("ItemDesc")
        @Expose
        private String itemDesc;
        @SerializedName("Serial")
        @Expose
        private String serial;
        @SerializedName("EngId")
        @Expose
        private Integer engId;
        @SerializedName("EngName")
        @Expose
        private String engName;
        @SerializedName("CallAssignDt")
        @Expose
        private String callAssignDt;
        @SerializedName("CallStatuss")
        @Expose
        private String callStatuss;
        @SerializedName("EngRemark")
        @Expose
        private String engRemark;
        @SerializedName("CallCompleteDt")
        @Expose
        private String callCompleteDt;
        @SerializedName("Enquiry")
        @Expose
        private String enquiry;
        @SerializedName("Problem_Name")
        @Expose
        private Object problemName;
        @SerializedName("EnqDesc")
        @Expose
        private String enqDesc;

        public Integer getCallId() {
            return callId;
        }

        public void setCallId(Integer callId) {
            this.callId = callId;
        }

        public String getCallRepNo() {
            return callRepNo;
        }

        public void setCallRepNo(String callRepNo) {
            this.callRepNo = callRepNo;
        }

        public String getCallRepDt() {
            return callRepDt;
        }

        public void setCallRepDt(String callRepDt) {
            this.callRepDt = callRepDt;
        }

        public Integer getCustId() {
            return custId;
        }

        public void setCustId(Integer custId) {
            this.custId = custId;
        }

        public String getCustName() {
            return custName;
        }

        public void setCustName(String custName) {
            this.custName = custName;
        }

        public Integer getItemId() {
            return itemId;
        }

        public void setItemId(Integer itemId) {
            this.itemId = itemId;
        }

        public String getItemDesc() {
            return itemDesc;
        }

        public void setItemDesc(String itemDesc) {
            this.itemDesc = itemDesc;
        }

        public String getSerial() {
            return serial;
        }

        public void setSerial(String serial) {
            this.serial = serial;
        }

        public Integer getEngId() {
            return engId;
        }

        public void setEngId(Integer engId) {
            this.engId = engId;
        }

        public String getEngName() {
            return engName;
        }

        public void setEngName(String engName) {
            this.engName = engName;
        }

        public String getCallAssignDt() {
            return callAssignDt;
        }

        public void setCallAssignDt(String callAssignDt) {
            this.callAssignDt = callAssignDt;
        }

        public String getCallStatuss() {
            return callStatuss;
        }

        public void setCallStatuss(String callStatuss) {
            this.callStatuss = callStatuss;
        }

        public String getEngRemark() {
            return engRemark;
        }

        public void setEngRemark(String engRemark) {
            this.engRemark = engRemark;
        }

        public String getCallCompleteDt() {
            return callCompleteDt;
        }

        public void setCallCompleteDt(String callCompleteDt) {
            this.callCompleteDt = callCompleteDt;
        }

        public String getEnquiry() {
            return enquiry;
        }

        public void setEnquiry(String enquiry) {
            this.enquiry = enquiry;
        }

        public Object getProblemName() {
            return problemName;
        }

        public void setProblemName(Object problemName) {
            this.problemName = problemName;
        }

        public String getEnqDesc() {
            return enqDesc;
        }

        public void setEnqDesc(String enqDesc) {
            this.enqDesc = enqDesc;
        }

    }
}


