package myapp.com.doit.dashboard.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import myapp.com.doit.R;
import myapp.com.doit.dashboard.ui.EnquiryListAct;
import myapp.com.doit.helper.Enquiry1_List;

public class EnquiryListAdapter extends BaseAdapter {

        ArrayList<Enquiry1_List> result;
        Context context;

        private LayoutInflater inflater = null;

        public EnquiryListAdapter(EnquiryListAct second, ArrayList<Enquiry1_List> details) {
            result = details;
            context = second;
        }

        /*private view holder class*/
        private class ViewHolder {
            TextView txt_CustName,txt_EnqDesc,txt_EnqDate,txt_EnqNo,txt_Enquiry,txt_Role_name,txt_IsApproved_YN;
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder holder = null;

            LayoutInflater mInflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);

            if (convertView == null) {
                convertView = mInflater.inflate(R.layout.enq_list, null);
                holder = new ViewHolder();

                holder.txt_CustName = (TextView) convertView.findViewById(R.id.txt_CustName);
                holder.txt_EnqDesc = (TextView) convertView.findViewById(R.id.txt_EnqDesc);
                holder.txt_EnqDate = (TextView) convertView.findViewById(R.id.txt_EnqDate);
                holder.txt_EnqNo = (TextView) convertView.findViewById(R.id.txt_EnqNo);
                holder.txt_Enquiry = (TextView) convertView.findViewById(R.id.txt_Enquiry);
                holder.txt_Role_name = (TextView) convertView.findViewById(R.id.txt_Role_name);
                holder.txt_IsApproved_YN = (TextView) convertView.findViewById(R.id.txt_IsApproved_YN);



                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }



            holder.txt_CustName.setText(result.get(position).getCustName());
            holder.txt_EnqDesc.setText(result.get(position).getEnqDesc());
            holder.txt_EnqDate.setText(result.get(position).getEnqDate());
            holder.txt_EnqNo.setText(result.get(position).getEnqNo());
            holder.txt_Enquiry.setText(result.get(position).getEnquiry());
            holder.txt_Role_name.setText(result.get(position).getRole_name());
            holder.txt_IsApproved_YN.setText(result.get(position).getIsApproved_YN());




            return convertView;
        }

        @Override
        public int getCount() {
            return result.size();
        }

        @Override
        public Object getItem(int position) {
            return result.get(position);
        }

        @Override
        public long getItemId(int position) {
            return result.indexOf(getItem(position));
        }
    }