package myapp.com.doit.dashboard.ui;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import myapp.com.doit.R;
import myapp.com.doit.base.BaseActivity;
import myapp.com.doit.dashboard.adapter.EnquiryListAdapter;
import myapp.com.doit.helper.Enquiry1_List;
import myapp.com.doit.helper.Enquiry1_List_Array;
import myapp.com.doit.utils.PrefUtils;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedByteArray;


public class EnquiryListAct extends BaseActivity {

    LinearLayout btn_Login, login;
    String status, message;
    private ProgressDialog loading;
    ListView list_view;
    ImageView left_arrow;
    TextView title_;
    LinearLayout Add_inq;


    @Override
    protected void initView() {
        list_view  = (ListView) findViewById(R.id.list_view);
        Add_inq = (LinearLayout) findViewById(R.id.Add_inq);
        title_ = (TextView) findViewById(R.id.title_);
        left_arrow = (ImageView) findViewById(R.id.left_arrow);

        title_.setText("Enquiry");
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.myenqury);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        initView();

        Add_inq.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent it = new Intent(EnquiryListAct.this, AddNewEnquiry.class);
                it.putExtra("keyName", "0");
                startActivity(it);
            }
        });

        left_arrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               /* Intent it = new Intent(EnquiryListAct.this, AdminDashboardActivity.class);
                it.putExtra("keyName", "0");
                startActivity(it);*/
                onBackPressed();
            }
        });

        Get_ServiceList();
    }


    private void Get_ServiceList() {
        try {
            loading = new ProgressDialog(EnquiryListAct.this);
            loading.setMessage("Please Wait Loading data ....");
            loading.show();
            loading.setCancelable(false);

        } catch (Exception e) {

        }

        getRestrofitInterface().Get_Enquiry_get("", PrefUtils.gete_RoleId(EnquiryListAct.this),
                PrefUtils.gete_Employee_IDF(EnquiryListAct.this), new Callback<Response>() {
            @Override
            public void success(Response findChildArray, Response response) {

                Log.e("status", new String(((TypedByteArray) findChildArray.getBody()).getBytes()));

                String result1 = new String(((TypedByteArray) findChildArray.getBody()).getBytes());

                JSONArray jObj = null;
                try {
                    jObj = new JSONArray(result1);

                    Log.e("Size", "" + jObj.length());


                    if (jObj.length() > 0) {
                        Enquiry1_List_Array enquiry1_List_Array = new Enquiry1_List_Array();
                        enquiry1_List_Array.category_List = new ArrayList<Enquiry1_List>();

                        for (int i = 0; i < jObj.length(); i++) {

                            JSONObject jsonObj = jObj.getJSONObject(i);

                            String EnqId = jsonObj.getString("EnqId");
                            String CustName = jsonObj.getString("CustName");
                            String EnqDesc = jsonObj.getString("EnqDesc");
                            String EnqDate = jsonObj.getString("EnqDate");
                            String EnqNo = jsonObj.getString("EnqNo");
                            String Enquiry = jsonObj.getString("Enquiry");
                            String Role_name = jsonObj.getString("Role_name");
                            String IsApproved_YN = jsonObj.getString("IsApproved_YN");

                           /* if (PrefUtils.get_Enquiry1_List_Array(EnquiryListAct.this) != null) {

                                Enquiry1_List_Array enquiry1_List_Array = PrefUtils.get_Enquiry1_List_Array(EnquiryListAct.this);
                                Enquiry1_List enquiry1_List = new Enquiry1_List(EnqId,CustName,EnqDesc,EnqDate,EnqNo,Enquiry,Role_name,IsApproved_YN);
                                enquiry1_List_Array.getCategory_List().add(enquiry1_List);
                                PrefUtils.set_Enquiry1_List_Array(enquiry1_List_Array, EnquiryListAct.this);

                            } else {
                                Enquiry1_List_Array enquiry1_List_Array = new Enquiry1_List_Array();
                                enquiry1_List_Array.category_List = new ArrayList<Enquiry1_List>();
                                Enquiry1_List enquiry1_List = new Enquiry1_List(EnqId,CustName,EnqDesc,EnqDate,EnqNo,Enquiry,Role_name,IsApproved_YN);
                                enquiry1_List_Array.category_List.add(enquiry1_List);
                                PrefUtils.set_Enquiry1_List_Array(enquiry1_List_Array, EnquiryListAct.this);

                            }*/
                            Enquiry1_List enquiry1_List = new Enquiry1_List(EnqId,CustName,EnqDesc,EnqDate,EnqNo,Enquiry,Role_name,IsApproved_YN);
                            enquiry1_List_Array.category_List.add(enquiry1_List);
                        }

                        //Enquiry1_List_Array enquiry1_List_Array = PrefUtils.get_Enquiry1_List_Array(EnquiryListAct.this);
                        EnquiryListAdapter adapter_Service = new EnquiryListAdapter(EnquiryListAct.this, enquiry1_List_Array.getCategory_List());
                        list_view.setAdapter(adapter_Service);
                    } else {
                        Toast.makeText(EnquiryListAct.this, "Username or Password Incorrect", Toast.LENGTH_SHORT).show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    loading.dismiss();
                }

                loading.dismiss();
            }

            @Override
            public void failure(RetrofitError error) {
                loading.dismiss();
                String merror = error.getMessage();
                Log.d("error", merror);
                Toast.makeText(EnquiryListAct.this, merror, Toast.LENGTH_LONG).show();
            }
        });
    }

}
