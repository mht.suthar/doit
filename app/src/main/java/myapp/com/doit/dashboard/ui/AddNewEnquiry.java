package myapp.com.doit.dashboard.ui;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.GsonBuilder;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.RequestBody;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;

import myapp.com.doit.R;
import myapp.com.doit.base.BaseActivity;
import myapp.com.doit.dashboard.adapter.Adapter_Mslist;
import myapp.com.doit.dashboard.adapter.Adapter_Problem_Type;
import myapp.com.doit.dashboard.adapter.Adapter_stype;
import myapp.com.doit.databinding.AddNewEnquiryBinding;
import myapp.com.doit.helper.Enquiry_type_List;
import myapp.com.doit.helper.Enquiry_type_List_Array;
import myapp.com.doit.helper.MachineDetailList_List_Array;
import myapp.com.doit.helper.Ms_List_Array;
import myapp.com.doit.helper.Mslist_List;
import myapp.com.doit.helper.Problem_type_List;
import myapp.com.doit.helper.Problem_type_List_Array;
import myapp.com.doit.helper.Sub_type_List;
import myapp.com.doit.helper.Sub_type_List_Array;
import myapp.com.doit.utils.PrefUtils;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedByteArray;
import retrofit.mime.TypedFile;


public class AddNewEnquiry extends BaseActivity {

    String status, message, customer_ID, RM_ID, EnquiryType_ID = "", Machine_ID = "", Problem_Type_ID = "", subtype_ID = "", f_enq_desc = "", f_Problem_Type_ID = "", f_subtype_ID;
    private ProgressDialog loading;
    Dialog dialogcat, dialogrm, dilogselect_EnquiryType, dilog_malist, dilog_Problem_Type, dilog_sType;
    private ListView list_view;
    private AddNewEnquiryBinding mBinding;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.add_new_enquiry);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        mBinding.title.setText("Add New Enquiry");

        long date = System.currentTimeMillis();
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
        SimpleDateFormat timeonly = new SimpleDateFormat("hh:mm");
        String dateString = sdf.format(date);
        String timeString = timeonly.format(date);
        //show present date
        mBinding.selectDate.setText(dateString);

        mBinding.btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                proceedSaveEntry();
            }
        });

        mBinding.linPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openCamera();
            }
        });

        mBinding.selectEnquiryType.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dilogselect_EnquiryType = new Dialog(AddNewEnquiry.this);
                dilogselect_EnquiryType.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dilogselect_EnquiryType.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                dilogselect_EnquiryType.setContentView(R.layout.dialogenqtyp_option);

                list_view = (ListView) dilogselect_EnquiryType.findViewById(R.id.lis_emp);

                Enquiry_type_List_Array enquiry_type_List_Array = PrefUtils.get_enquiry_type_List_Array(AddNewEnquiry.this);

                Adapter_Servicenq adapter_Servicenq = new Adapter_Servicenq(AddNewEnquiry.this, enquiry_type_List_Array.getCategory_List());

                list_view.setAdapter(adapter_Servicenq);

                list_view.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                        Enquiry_type_List_Array enquiry_type_List_Array = PrefUtils.get_enquiry_type_List_Array(AddNewEnquiry.this);
                        mBinding.selectEnquiryType.setText("" + enquiry_type_List_Array.getCategory_List().get(i).getEnquiry());
                        //  Toast.makeText(Inquire_Activity.this, ""+state_list_array.getStateArrayList().get(i).getStateName(), Toast.LENGTH_LONG).show();

                        EnquiryType_ID = "" + enquiry_type_List_Array.getCategory_List().get(i).getID();

                        String EnquiryType_IDname = "" + enquiry_type_List_Array.getCategory_List().get(i).getEnquiry();

                        dilogselect_EnquiryType.dismiss();


                        mBinding.selectMachineName.setText("");
                        Machine_ID = "";

                        mBinding.modelNo.setText("");
                        mBinding.serialNo.setText("");
                        mBinding.msdesc.setText("");

                        if (EnquiryType_IDname.equals("General")) {
                            mBinding.rmLay.setVisibility(View.GONE);
                            mBinding.SerialNoLay.setVisibility(View.GONE);
                            mBinding.msdescLay.setVisibility(View.VISIBLE);
                            mBinding.ProblemTypeLay.setVisibility(View.GONE);
                            mBinding.ProblemSTypeLay.setVisibility(View.GONE);
                            mBinding.linPhoto.setVisibility(View.GONE);
                        } else if (EnquiryType_IDname.equals("Complaint")) {
                            mBinding.rmLay.setVisibility(View.VISIBLE);
                            mBinding.SerialNoLay.setVisibility(View.VISIBLE);
                            mBinding.ProblemTypeLay.setVisibility(View.VISIBLE);
                            mBinding.linPhoto.setVisibility(View.VISIBLE);
                            mBinding.msdescLay.setVisibility(View.GONE);
                            mBinding.ProblemSTypeLay.setVisibility(View.GONE);
                        } else {
                            mBinding.rmLay.setVisibility(View.VISIBLE);
                            mBinding.SerialNoLay.setVisibility(View.VISIBLE);
                            mBinding.msdescLay.setVisibility(View.GONE);
                            mBinding.ProblemTypeLay.setVisibility(View.GONE);
                            mBinding.ProblemSTypeLay.setVisibility(View.GONE);
                            mBinding.linPhoto.setVisibility(View.GONE);
                        }

                        GetMachineList();
                    }
                });

                dilogselect_EnquiryType.show();
            }
        });


        mBinding.selectMachineName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dilog_malist = new Dialog(AddNewEnquiry.this);
                dilog_malist.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dilog_malist.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                dilog_malist.setContentView(R.layout.dialog_mslist);

                list_view = (ListView) dilog_malist.findViewById(R.id.lis_emp);

                Ms_List_Array ms_List_Array = PrefUtils.get_ms_List_Array(AddNewEnquiry.this);

                Adapter_Mslist adapter_Mslist = new Adapter_Mslist(AddNewEnquiry.this, ms_List_Array.getCategory_List());

                list_view.setAdapter(adapter_Mslist);


                list_view.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                        Ms_List_Array ms_List_Array = PrefUtils.get_ms_List_Array(AddNewEnquiry.this);

                        mBinding.selectMachineName.setText("" + ms_List_Array.getCategory_List().get(i).getItemDesc());
                        //  Toast.makeText(Inquire_Activity.this, ""+state_list_array.getStateArrayList().get(i).getStateName(), Toast.LENGTH_LONG).show();
                        Machine_ID = "" + ms_List_Array.getCategory_List().get(i).getItemID();
                        // String   EnquiryType_IDname = "" + enquiry_type_List_Array.getCategory_List().get(i).getEnquiry();
                        dilog_malist.dismiss();
                        GetMachineDetail();
                    }
                });

                dilog_malist.show();
            }
        });


        mBinding.selectProblemType.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dilog_Problem_Type = new Dialog(AddNewEnquiry.this);
                dilog_Problem_Type.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dilog_Problem_Type.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                dilog_Problem_Type.setContentView(R.layout.dialog_problemtype);

                list_view = (ListView) dilog_Problem_Type.findViewById(R.id.lis_emp);

                Problem_type_List_Array problem_type_List_Array = PrefUtils.get_problem_type_List_Array(AddNewEnquiry.this);

                Adapter_Problem_Type adapter_Problem_Type = new Adapter_Problem_Type(AddNewEnquiry.this, problem_type_List_Array.getCategory_List());

                list_view.setAdapter(adapter_Problem_Type);


                list_view.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                        Problem_type_List_Array problem_type_List_Array = PrefUtils.get_problem_type_List_Array(AddNewEnquiry.this);

                        mBinding.selectProblemType.setText("" + problem_type_List_Array.getCategory_List().get(i).getProblem());
                        //  Toast.makeText(Inquire_Activity.this, ""+state_list_array.getStateArrayList().get(i).getStateName(), Toast.LENGTH_LONG).show();

                        Problem_Type_ID = "" + problem_type_List_Array.getCategory_List().get(i).getID();

                        // String   EnquiryType_IDname = "" + enquiry_type_List_Array.getCategory_List().get(i).getEnquiry();

                        dilog_Problem_Type.dismiss();


                        if (Problem_Type_ID.equalsIgnoreCase("1")) {
                            mBinding.problemSname.setText("Problem :");
                            mBinding.selectProblemSType.setText("Select Problem");
                            GetMechanicalList();

                        } else if (Problem_Type_ID.equalsIgnoreCase("2")) {
                            mBinding.problemSname.setText("Chemical :");
                            mBinding.selectProblemSType.setText("Select Chemical");
                            GetChemicalList();
                        }
                        mBinding.ProblemSTypeLay.setVisibility(View.VISIBLE);
                    }
                });

                dilog_Problem_Type.show();
            }
        });


        mBinding.selectProblemSType.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dilog_sType = new Dialog(AddNewEnquiry.this);
                dilog_sType.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dilog_sType.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                dilog_sType.setContentView(R.layout.dialog_stype);

                list_view = (ListView) dilog_sType.findViewById(R.id.lis_emp);

                Sub_type_List_Array sub_type_List_Array = PrefUtils.get_sub_type_List_Array(AddNewEnquiry.this);

                Adapter_stype adapter_stype = new Adapter_stype(AddNewEnquiry.this, sub_type_List_Array.getCategory_List());

                list_view.setAdapter(adapter_stype);


                list_view.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                        Sub_type_List_Array sub_type_List_Array = PrefUtils.get_sub_type_List_Array(AddNewEnquiry.this);
                        mBinding.selectProblemSType.setText("" + sub_type_List_Array.getCategory_List().get(i).getProblem_Name());
                        //  Toast.makeText(Inquire_Activity.this, ""+state_list_array.getStateArrayList().get(i).getStateName(), Toast.LENGTH_LONG).show();
                        subtype_ID = "" + sub_type_List_Array.getCategory_List().get(i).getProblem_ID();
                        // String   EnquiryType_IDname = "" + enquiry_type_List_Array.getCategory_List().get(i).getEnquiry();
                        dilog_sType.dismiss();
                    }
                });

                dilog_sType.show();
            }
        });

        mBinding.leftArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });


        GetCustomerData();

        Get_EnquiryTypeList();
    }

    private void openCamera() {
        CropImage.activity()
                .setGuidelines(CropImageView.Guidelines.ON)
                .start(this);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                Uri resultUri = result.getUri();
                File file = new File(resultUri.getPath());
                mBinding.txtFilePath.setText(file.getAbsolutePath());
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
            }
        }
    }

    private void proceedSaveEntry() {
        if (customer_ID.equalsIgnoreCase("")) {
            Toast.makeText(AddNewEnquiry.this, "Following fields are compulsory !\n" +
                    "Select Customer .\n" +
                    "Select RM .\n" +
                    "Select Machine .\n" +
                    "Select Enquiry Type .", Toast.LENGTH_LONG).show();

        } else if (Machine_ID.equalsIgnoreCase("")) {
            Toast.makeText(AddNewEnquiry.this, "Following fields are compulsory !\n" +
                    "Select Machine .\n" +
                    "SerialNo Compulsary.", Toast.LENGTH_LONG).show();
        } else {
            if (EnquiryType_ID.equalsIgnoreCase("3")) {
                f_enq_desc = mBinding.enqDesc.getText().toString();
                f_Problem_Type_ID = "0"; //Problem_Type_ID;
                f_subtype_ID = "0";   //subtype_ID;

                Get_Save_Enquiry();
            } else if (EnquiryType_ID.equalsIgnoreCase("2")) {
                if (Problem_Type_ID.equalsIgnoreCase("")) {
                    Toast.makeText(AddNewEnquiry.this, "Please Select Problem type ", Toast.LENGTH_LONG).show();
                } else if (Problem_Type_ID.equalsIgnoreCase("1")) {
                    if (subtype_ID.equalsIgnoreCase("")) {
                        Toast.makeText(AddNewEnquiry.this, "Please Select Mechanical Problem  ", Toast.LENGTH_LONG).show();
                    } else {
                        f_enq_desc = mBinding.enqDesc.getText().toString();
                        f_Problem_Type_ID = Problem_Type_ID;
                        f_subtype_ID = subtype_ID;

                        Get_Save_Enquiry();
                    }
                } else if (Problem_Type_ID.equalsIgnoreCase("2")) {
                    if (subtype_ID.equalsIgnoreCase("")) {
                        Toast.makeText(AddNewEnquiry.this, "Please Select Chemical Problem ", Toast.LENGTH_LONG).show();
                    } else {
                        f_enq_desc = mBinding.enqDesc.getText().toString();
                        f_Problem_Type_ID = Problem_Type_ID;
                        f_subtype_ID = subtype_ID;

                        Get_Save_Enquiry();
                    }
                }
            } else if (EnquiryType_ID.equalsIgnoreCase("1")) {
                f_enq_desc = mBinding.enqDesc.getText().toString();
                f_Problem_Type_ID = "0"; //Problem_Type_ID;
                f_subtype_ID = "0";   //subtype_ID;

                Get_Save_Enquiry();
            }
        }
    }

    private void GetCustomerData() {
        PrefUtils.clear_custmar_List_Array(AddNewEnquiry.this);
        showLoader();
        getRestrofitInterface().GetCustomerDetail("" + PrefUtils.gete_Employee_IDF(AddNewEnquiry.this),
                "" + PrefUtils.gete_RoleId(AddNewEnquiry.this), new Callback<Response>() {
                    @Override
                    public void success(Response findChildArray, Response response) {
                        hideLoader();
                        Log.e("status", new String(((TypedByteArray) findChildArray.getBody()).getBytes()));

                        String result1 = new String(((TypedByteArray) findChildArray.getBody()).getBytes());

                        JSONObject jObj = null;
                        try {
                            jObj = new JSONObject(result1);
                            JSONObject jsonObj = jObj.getJSONObject("result");
                            customer_ID = "" + jsonObj.getInt("CustId");
                            RM_ID = "" + jsonObj.getInt("RMId");
                            mBinding.selectCustomer.setText(jsonObj.getString("CustName"));
                            mBinding.contactPerson.setText(jsonObj.getString("CustPerson"));
                            // mBinding.designation.setText(jsonObj.getString("CustName"));
                            mBinding.address.setText(jsonObj.getString("CustAddr"));
                            mBinding.mobileno.setText(jsonObj.getString("CustContact"));
                            mBinding.selectrm.setText(jsonObj.getString("RMName"));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        hideLoader();
                        String merror = error.getMessage();
                        Log.d("error", merror);
                        Toast.makeText(AddNewEnquiry.this, merror, Toast.LENGTH_LONG).show();
                    }
                });
    }

    @Override
    protected void initView() {

    }

    private void Get_EnquiryTypeList() {
        PrefUtils.clear_enquiry_type_List_Array(AddNewEnquiry.this);
        getRestrofitInterface().Get_EnquiryTypeList(new Callback<Response>() {
            @Override
            public void success(Response findChildArray, Response response) {

                Log.e("status", new String(((TypedByteArray) findChildArray.getBody()).getBytes()));

                String result1 = new String(((TypedByteArray) findChildArray.getBody()).getBytes());

                JSONArray jObj = null;
                try {
                    jObj = new JSONArray(result1);
                    Log.e("Enquiry List Size", "" + jObj.length());
                    if (jObj.length() > 0) {
                        for (int i = 0; i < jObj.length(); i++) {

                            JSONObject jsonObj = jObj.getJSONObject(i);

                            String ID = jsonObj.getString("ID");
                            String Enquiry = jsonObj.getString("Enquiry");

                            if (PrefUtils.get_enquiry_type_List_Array(AddNewEnquiry.this) != null) {
                                Enquiry_type_List_Array enquiry_type_List_Array = PrefUtils.get_enquiry_type_List_Array(AddNewEnquiry.this);
                                Enquiry_type_List enquiry_type_List = new Enquiry_type_List(ID, Enquiry);
                                enquiry_type_List_Array.getCategory_List().add(enquiry_type_List);
                                PrefUtils.set_enquiry_type_List_Array(enquiry_type_List_Array, AddNewEnquiry.this);
                            } else {
                                Enquiry_type_List_Array enquiry_type_List_Array = new Enquiry_type_List_Array();
                                enquiry_type_List_Array.category_List = new ArrayList<Enquiry_type_List>();
                                Enquiry_type_List enquiry_type_List = new Enquiry_type_List(ID, Enquiry);
                                enquiry_type_List_Array.category_List.add(enquiry_type_List);
                                PrefUtils.set_enquiry_type_List_Array(enquiry_type_List_Array, AddNewEnquiry.this);
                            }
                        }
                    } else {
                        Toast.makeText(AddNewEnquiry.this, "Username or Password Incorrect", Toast.LENGTH_SHORT).show();
                    }

                    Get_Problem_Type();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void failure(RetrofitError error) {
                String merror = error.getMessage();
                Log.d("error", merror);
                Toast.makeText(AddNewEnquiry.this, merror, Toast.LENGTH_LONG).show();
            }
        });
    }


    public class Adapter_Servicenq extends BaseAdapter {


        ArrayList<Enquiry_type_List> result;
        Context context;

        private LayoutInflater inflater = null;

        public Adapter_Servicenq(AddNewEnquiry second, ArrayList<Enquiry_type_List> details) {
            result = details;
            context = second;
        }


        /*private view holder class*/
        private class ViewHolder {

            TextView txt_name;


        }

        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder holder = null;

            LayoutInflater mInflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);

            if (convertView == null) {
                convertView = mInflater.inflate(R.layout.customar_row, null);
                holder = new ViewHolder();

                holder.txt_name = (TextView) convertView.findViewById(R.id.txt_name);


                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }


            holder.txt_name.setText(result.get(position).getEnquiry());
            holder.txt_name.setTag(result.get(position).getID());


            return convertView;
        }

        @Override
        public int getCount() {
            return result.size();
        }

        @Override
        public Object getItem(int position) {
            return result.get(position);
        }

        @Override
        public long getItemId(int position) {
            return result.indexOf(getItem(position));
        }
    }


    private void GetMachineList() {
        showLoader();
        PrefUtils.clear_ms_List_Array(AddNewEnquiry.this);
        getRestrofitInterface().Get_ItemMstList(PrefUtils.gete_Employee_IDF(AddNewEnquiry.this), EnquiryType_ID, new Callback<Response>() {
            @Override
            public void success(Response findChildArray, Response response) {
                hideLoader();
                Log.e("status", new String(((TypedByteArray) findChildArray.getBody()).getBytes()));

                String result1 = new String(((TypedByteArray) findChildArray.getBody()).getBytes());

                JSONArray jObj = null;
                try {
                    jObj = new JSONArray(result1);

                    Log.e("GetMachineList Size", "" + jObj.length());

                    if (jObj.length() > 0) {
                        for (int i = 0; i < jObj.length(); i++) {

                            JSONObject jsonObj = jObj.getJSONObject(i);

                            String ItemID = jsonObj.getString("ItemID");
                            String ItemDesc = jsonObj.getString("ItemDesc");

                            if (PrefUtils.get_ms_List_Array(AddNewEnquiry.this) != null) {
                                Ms_List_Array ms_List_Array = PrefUtils.get_ms_List_Array(AddNewEnquiry.this);
                                Mslist_List mslist_List = new Mslist_List(ItemID, ItemDesc);
                                ms_List_Array.getCategory_List().add(mslist_List);
                                PrefUtils.set_ms_List_Array(ms_List_Array, AddNewEnquiry.this);
                            } else {
                                Ms_List_Array ms_List_Array = new Ms_List_Array();
                                ms_List_Array.category_List = new ArrayList<Mslist_List>();
                                Mslist_List mslist_List = new Mslist_List(ItemID, ItemDesc);
                                ms_List_Array.category_List.add(mslist_List);
                                PrefUtils.set_ms_List_Array(ms_List_Array, AddNewEnquiry.this);
                            }
                        }
                    } else {
                        Toast.makeText(AddNewEnquiry.this, "Username or Password Incorrect", Toast.LENGTH_SHORT).show();
                    }
                    //  Get_EnquiryTypeList();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                // loading.dismiss();
            }

            @Override
            public void failure(RetrofitError error) {
                hideLoader();
                String merror = error.getMessage();
                Log.d("error", merror);
                Toast.makeText(AddNewEnquiry.this, merror, Toast.LENGTH_LONG).show();
            }
        });
    }


    private void GetMachineDetail() {
        showLoader();
        //PrefUtils.clear_rm_List_Array(Addnew_Enqury.this);
        getRestrofitInterface().Get_ItemMsDetail(Machine_ID, PrefUtils.gete_Employee_IDF(AddNewEnquiry.this), EnquiryType_ID, new Callback<Response>() {
            @Override
            public void success(Response findChildArray, Response response) {
                hideLoader();
                Log.e("status", new String(((TypedByteArray) findChildArray.getBody()).getBytes()));

                String result1 = new String(((TypedByteArray) findChildArray.getBody()).getBytes());

                JSONObject jObj = null;
                try {
                    jObj = new JSONObject(result1);
                    Log.e("Size", "" + jObj.length());

                    if (jObj.length() > 0) {
                        JSONArray values = jObj.getJSONArray("result");

                        MachineDetailList_List_Array machineDetailList_List_Array = new GsonBuilder().create().fromJson(result1, MachineDetailList_List_Array.class);
                        PrefUtils.set_machineDetailList_List_Array(machineDetailList_List_Array, AddNewEnquiry.this);

                        mBinding.modelNo.setText(PrefUtils.get_machineDetailList_List_Array(AddNewEnquiry.this).getResult().get(0).getItemModel());
                        mBinding.serialNo.setText(PrefUtils.get_machineDetailList_List_Array(AddNewEnquiry.this).getResult().get(0).getSerNo());
                        mBinding.msdesc.setText(PrefUtils.get_machineDetailList_List_Array(AddNewEnquiry.this).getResult().get(0).getItemPrerequest());
                        if (machineDetailList_List_Array.getResult().get(0).getWarrStatus().equalsIgnoreCase("Your Machine in Under Warranty")) {
                            mBinding.wortext.setText("Your Machine in Under Warranty");
                        }
                        if (machineDetailList_List_Array.getResult().get(0).getWarrStatus().equalsIgnoreCase("Your Machine in NON AMC &amp; Out of Warranty")) {
                            //mBinding.wortext.setText("Your Machine in NON AMC & Out of Warranty");
                            mBinding.wortext.setText("" + machineDetailList_List_Array.getResult().get(0).getAmcStatus());
                           /* AlertDialog.Builder builder = new AlertDialog.Builder(AddNewEnquiry.this);
                            // builder.setTitle("Confirm");
                            builder.setMessage("Your Machine in NON AMC & Out of Warranty\n" +
                                    "Charges (1500) and Travel & Accommodation : Not Applicable\n" +
                                    "Do you Agree(Yes/No)");

                            builder.setPositiveButton("YES", new DialogInterface.OnClickListener() {

                                public void onClick(DialogInterface dialog, int which) {
                                    mBinding.wortext.setText("Your Machine in NON AMC & Out of Warranty \n Agree For Charges");
                                    dialog.dismiss();
                                }
                            });

                            builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {

                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    mBinding.wortext.setText("Your Machine in NON AMC & Out of Warranty \n Not Agree For Charges");
                                    dialog.dismiss();
                                }
                            });

                            AlertDialog alert = builder.create();
                            alert.show();*/
                        } else if (machineDetailList_List_Array.getResult().get(0).getAmcStatus().equalsIgnoreCase("Your Machine in NON AMC &amp; Out of Warranty")) {
                            mBinding.wortext.setText("" + machineDetailList_List_Array.getResult().get(0).getWarrStatus());
                        }
                    } else {
                        Toast.makeText(AddNewEnquiry.this, "Username or Password Incorrect", Toast.LENGTH_SHORT).show();
                    }
                    //Get_EnquiryTypeList();

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void failure(RetrofitError error) {
                hideLoader();
                String merror = error.getMessage();
                Log.d("error", merror);
                Toast.makeText(AddNewEnquiry.this, merror, Toast.LENGTH_LONG).show();
            }
        });
    }


    private void Get_Problem_Type() {
        PrefUtils.clear_problem_type_List_Array(AddNewEnquiry.this);
        getRestrofitInterface().Get_ProblemTypeList(new Callback<Response>() {
            @Override
            public void success(Response findChildArray, Response response) {

                Log.e("status", new String(((TypedByteArray) findChildArray.getBody()).getBytes()));

                String result1 = new String(((TypedByteArray) findChildArray.getBody()).getBytes());

                JSONArray jObj = null;
                try {
                    jObj = new JSONArray(result1);
                    Log.e("Problen Size", "" + jObj.length());
                    if (jObj.length() > 0) {
                        for (int i = 0; i < jObj.length(); i++) {

                            JSONObject jsonObj = jObj.getJSONObject(i);

                            String ID = jsonObj.getString("ID");
                            String Problem = jsonObj.getString("Problem");

                            if (PrefUtils.get_problem_type_List_Array(AddNewEnquiry.this) != null) {
                                Problem_type_List_Array problem_type_List_Array = PrefUtils.get_problem_type_List_Array(AddNewEnquiry.this);
                                Problem_type_List problem_type_List = new Problem_type_List(ID, Problem);
                                problem_type_List_Array.getCategory_List().add(problem_type_List);
                                PrefUtils.set_problem_type_List_Array(problem_type_List_Array, AddNewEnquiry.this);
                            } else {
                                Problem_type_List_Array problem_type_List_Array = new Problem_type_List_Array();
                                problem_type_List_Array.category_List = new ArrayList<Problem_type_List>();
                                Problem_type_List problem_type_List = new Problem_type_List(ID, Problem);
                                problem_type_List_Array.category_List.add(problem_type_List);
                                PrefUtils.set_problem_type_List_Array(problem_type_List_Array, AddNewEnquiry.this);
                            }
                        }
                    } else {
                        Toast.makeText(AddNewEnquiry.this, "Username or Password Incorrect", Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void failure(RetrofitError error) {
                String merror = error.getMessage();
                Log.d("error", merror);
                Toast.makeText(AddNewEnquiry.this, merror, Toast.LENGTH_LONG).show();
            }
        });
    }


    private void GetMechanicalList() {
        showLoader();
        PrefUtils.clear_sub_type_List_Array(AddNewEnquiry.this);
        getRestrofitInterface().Get_GetMechanicalList(new Callback<Response>() {
            @Override
            public void success(Response findChildArray, Response response) {

                Log.e("status", new String(((TypedByteArray) findChildArray.getBody()).getBytes()));

                String result1 = new String(((TypedByteArray) findChildArray.getBody()).getBytes());

                JSONArray jObj = null;
                try {
                    jObj = new JSONArray(result1);

                    Log.e("Get_GetMechanicalList Size", "" + jObj.length());

                    if (jObj.length() > 0) {
                        for (int i = 0; i < jObj.length(); i++) {

                            JSONObject jsonObj = jObj.getJSONObject(i);

                            String Problem_ID = jsonObj.getString("Problem_ID");
                            String Problem_Name = jsonObj.getString("Problem_Name");

                            if (PrefUtils.get_sub_type_List_Array(AddNewEnquiry.this) != null) {
                                Sub_type_List_Array sub_type_List_Array = PrefUtils.get_sub_type_List_Array(AddNewEnquiry.this);
                                Sub_type_List sub_type_List = new Sub_type_List(Problem_ID, Problem_Name);
                                sub_type_List_Array.getCategory_List().add(sub_type_List);
                                PrefUtils.set_sub_type_List_Array(sub_type_List_Array, AddNewEnquiry.this);
                            } else {
                                Sub_type_List_Array sub_type_List_Array = new Sub_type_List_Array();
                                sub_type_List_Array.category_List = new ArrayList<Sub_type_List>();
                                Sub_type_List sub_type_List = new Sub_type_List(Problem_ID, Problem_Name);
                                sub_type_List_Array.category_List.add(sub_type_List);
                                PrefUtils.set_sub_type_List_Array(sub_type_List_Array, AddNewEnquiry.this);
                            }
                        }
                    } else {
                        Toast.makeText(AddNewEnquiry.this, "Username or Password Incorrect", Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                hideLoader();
            }

            @Override
            public void failure(RetrofitError error) {
                hideLoader();
                String merror = error.getMessage();
                Log.d("error", merror);
                Toast.makeText(AddNewEnquiry.this, merror, Toast.LENGTH_LONG).show();
            }
        });
    }

    private void GetChemicalList() {
        showLoader();
        PrefUtils.clear_sub_type_List_Array(AddNewEnquiry.this);
        getRestrofitInterface().Get_ChemicalList(new Callback<Response>() {
            @Override
            public void success(Response findChildArray, Response response) {

                Log.e("status", new String(((TypedByteArray) findChildArray.getBody()).getBytes()));

                String result1 = new String(((TypedByteArray) findChildArray.getBody()).getBytes());

                JSONArray jObj = null;
                try {
                    jObj = new JSONArray(result1);

                    Log.e("Chemical List Size", "" + jObj.length());

                    if (jObj.length() > 0) {
                        for (int i = 0; i < jObj.length(); i++) {

                            JSONObject jsonObj = jObj.getJSONObject(i);

                            String Problem_ID = jsonObj.getString("Problem_ID");
                            String Problem_Name = jsonObj.getString("Problem_Name");

                            if (PrefUtils.get_sub_type_List_Array(AddNewEnquiry.this) != null) {
                                Sub_type_List_Array sub_type_List_Array = PrefUtils.get_sub_type_List_Array(AddNewEnquiry.this);
                                Sub_type_List sub_type_List = new Sub_type_List(Problem_ID, Problem_Name);
                                sub_type_List_Array.getCategory_List().add(sub_type_List);
                                PrefUtils.set_sub_type_List_Array(sub_type_List_Array, AddNewEnquiry.this);
                            } else {
                                Sub_type_List_Array sub_type_List_Array = new Sub_type_List_Array();
                                sub_type_List_Array.category_List = new ArrayList<Sub_type_List>();
                                Sub_type_List sub_type_List = new Sub_type_List(Problem_ID, Problem_Name);
                                sub_type_List_Array.category_List.add(sub_type_List);
                                PrefUtils.set_sub_type_List_Array(sub_type_List_Array, AddNewEnquiry.this);
                            }
                        }
                    } else {
                        Toast.makeText(AddNewEnquiry.this, "Username or Password Incorrect", Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                hideLoader();
            }

            @Override
            public void failure(RetrofitError error) {
                hideLoader();
                String merror = error.getMessage();
                Log.d("error", merror);
                Toast.makeText(AddNewEnquiry.this, merror, Toast.LENGTH_LONG).show();
            }
        });
    }

    private void Get_Save_Enquiry() {
        PrefUtils.clear_sub_type_List_Array(AddNewEnquiry.this);
        showLoader();

        HashMap map = new HashMap<String, RequestBody>();
        map.put("CustId", toRequestBody(PrefUtils.gete_Employee_IDF(AddNewEnquiry.this)));
        map.put("EnqDesc", toRequestBody(f_enq_desc));
        map.put("RMId", toRequestBody(RM_ID));
        map.put("EnquiryType", toRequestBody(EnquiryType_ID));
        map.put("ProblemType", toRequestBody(f_Problem_Type_ID));
        map.put("ProblemId", toRequestBody(f_subtype_ID));
        map.put("FAQID", toRequestBody("0"));
        map.put("ConfirmType", toRequestBody("NULL"));
        map.put("Created_By", toRequestBody("10014"));
        map.put("Role_ID", toRequestBody(PrefUtils.gete_RoleId(AddNewEnquiry.this)));
        map.put("Link", toRequestBody(""));

        if (!TextUtils.isEmpty(mBinding.txtFilePath.getText().toString())) {
            File file = new File(mBinding.txtFilePath.getText().toString());
            RequestBody fileBody = RequestBody.create(MediaType.parse("image/png"), file);
            map.put("myFile\"; filename=\"" + file.getName(), fileBody);
        }

        //@Field("") String CustId, @Field("") String EnqDesc, @Field("") String RMId,
        // @Field("") String EnquiryType, @Field("") String ProblemType,@Field("")
        // String ProblemId,@Field("")
        // String FAQID,@Field("") String ConfirmType,@Field("")
        // String Created_By,@Field("") String Role_ID

        TypedFile typedFile = null;
        if (!TextUtils.isEmpty(mBinding.txtFilePath.getText().toString())) {
            File file = new File(mBinding.txtFilePath.getText().toString());
            typedFile = new TypedFile("multipart/form-data", file);
        }


        //getRestrofitInterface().Get_SaveEnquiry(PrefUtils.gete_User_Id(AddNewEnquiry.this), f_enq_desc, RM_ID, EnquiryType_ID, f_Problem_Type_ID, f_subtype_ID, "0", "NULL", "10014", PrefUtils.gete_RoleId(AddNewEnquiry.this), new Callback<Response>() {
        getRestrofitInterface().saveEnquiry(PrefUtils.gete_Employee_IDF(AddNewEnquiry.this), f_enq_desc, RM_ID, EnquiryType_ID, f_Problem_Type_ID, f_subtype_ID, "0", "NULL", "10014", PrefUtils.gete_RoleId(AddNewEnquiry.this), Machine_ID, typedFile, new Callback<Response>() {
            //getRestrofitInterface().saveEnquiry(map, new Callback<Response>() {
            @Override
            public void success(Response findChildArray, Response response) {

                Log.e("status", new String(((TypedByteArray) findChildArray.getBody()).getBytes()));

                String result1 = new String(((TypedByteArray) findChildArray.getBody()).getBytes());

                JSONObject jObj = null;
                try {
                    jObj = new JSONObject(result1);

                    Log.e("Sae=ve Size", "" + jObj.length());

                    if (jObj.length() > 0) {
                        JSONObject obj = jObj.getJSONObject("result");
                        String IsSuccess = obj.getString("IsSuccess");
                        if (IsSuccess.equalsIgnoreCase("true")) {
                            Intent it = new Intent(AddNewEnquiry.this, EnquiryListAct.class);
                            it.putExtra("keyName", "0");
                            startActivity(it);
                            finish();
                        } else {
                            Toast.makeText(AddNewEnquiry.this, "" + obj.getString("ErrorMessage"), Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(AddNewEnquiry.this, "Username or Password Incorrect", Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                hideLoader();
            }

            @Override
            public void failure(RetrofitError error) {
                String merror = error.getMessage();
                Log.d("error", merror);
                Toast.makeText(AddNewEnquiry.this, merror, Toast.LENGTH_LONG).show();

            }
        });
    }

    private void showLoader() {
        try {
            loading = new ProgressDialog(AddNewEnquiry.this);
            loading.setMessage("Please Wait Loading data ....");
            loading.show();
            loading.setCancelable(false);
        } catch (Exception e) {
        }
    }

    private void hideLoader() {
        //loading.dismiss();
        if(loading != null) {
            loading.dismiss();
            //loading.cancel();
        }
    }
}
