package myapp.com.doit.dashboard.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import myapp.com.doit.R;
import myapp.com.doit.dashboard.ui.AddNewEnquiry;
import myapp.com.doit.helper.RM_List;

public class Adapter_Servicerm extends BaseAdapter {


        ArrayList<RM_List> result;
        Context context;

        private LayoutInflater inflater = null;

        public Adapter_Servicerm(AddNewEnquiry second, ArrayList<RM_List> details) {
            // TODO Auto-generated constructor stub
            result = details;
            context = second;

        }


        /*private view holder class*/
        private class ViewHolder {

            TextView txt_name;


        }

        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder holder = null;

            LayoutInflater mInflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);

            if (convertView == null) {
                convertView = mInflater.inflate(R.layout.customar_row, null);
                holder = new ViewHolder();

                holder.txt_name = (TextView) convertView.findViewById(R.id.txt_name);


                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }


            holder.txt_name.setText(result.get(position).getEngName());
            holder.txt_name.setTag(result.get(position).getEngId());


            return convertView;
        }

        @Override
        public int getCount() {
            return result.size();
        }

        @Override
        public Object getItem(int position) {
            return result.get(position);
        }

        @Override
        public long getItemId(int position) {
            return result.indexOf(getItem(position));
        }
    }