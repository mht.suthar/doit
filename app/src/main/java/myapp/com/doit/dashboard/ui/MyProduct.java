package myapp.com.doit.dashboard.ui;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.GsonBuilder;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import myapp.com.doit.R;
import myapp.com.doit.base.BaseActivity;
import myapp.com.doit.dashboard.adapter.Adapter_Service;
import myapp.com.doit.helper.Amc_List_Array;
import myapp.com.doit.utils.PrefUtils;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedByteArray;


public class MyProduct extends BaseActivity {

    LinearLayout btn_Login, login;
    String status, message;
    private ProgressDialog loading;
    ListView list_view;
    ImageView left_arrow;
    TextView title_;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.myproduct);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        initView();
        title_.setText("MyProduct");
        left_arrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /*Intent it = new Intent(MyProduct.this, AdminDashboardActivity.class);
                it.putExtra("keyName", "0");
                startActivity(it);*/
                onBackPressed();
            }
        });
        Get_ServiceList();
    }

    private void Get_ServiceList() {
        try {
            loading = new ProgressDialog(MyProduct.this);
            loading.setMessage("Please Wait Loading data ....");
            loading.show();
            loading.setCancelable(false);

        } catch (Exception e) {

        }

        getRestrofitInterface().Get_AMCWarrantyList(PrefUtils.gete_Employee_IDF(MyProduct.this), new Callback<Response>() {
            @Override
            public void success(Response findChildArray, Response response) {

                Log.e("status", new String(((TypedByteArray) findChildArray.getBody()).getBytes()));

                String result1 = new String(((TypedByteArray) findChildArray.getBody()).getBytes());

                JSONObject jObj = null;
                try {
                    jObj = new JSONObject(result1);

                    Log.e("Size", "" + jObj.length());
                    if (jObj.length() > 0) {

                        JSONArray values = jObj.getJSONArray("result");
                        Amc_List_Array amc_List_Array = new GsonBuilder().create().fromJson(result1, Amc_List_Array.class);
                        PrefUtils.set_amc_List_Array(amc_List_Array, MyProduct.this);
                        Adapter_Service adapter_Service = new Adapter_Service(MyProduct.this, amc_List_Array.getResult());
                        list_view.setAdapter(adapter_Service);
                    } else {


                        Toast.makeText(MyProduct.this, "Username or Password Incorrect", Toast.LENGTH_SHORT).show();

                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }

                loading.dismiss();

            }

            @Override
            public void failure(RetrofitError error) {

                String merror = error.getMessage();
                Log.d("error", merror);
                Toast.makeText(MyProduct.this, merror, Toast.LENGTH_LONG).show();

            }
        });
    }

    @Override
    protected void initView() {
        list_view  = (ListView) findViewById(R.id.list_view);
        title_ = (TextView) findViewById(R.id.title_);
        left_arrow = (ImageView) findViewById(R.id.left_arrow);
    }
}
