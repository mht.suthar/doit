package myapp.com.doit.base;

import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;

import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.RequestBody;

import java.util.concurrent.TimeUnit;

import myapp.com.doit.BuildConfig;
import myapp.com.doit.rest.RetrfitInterface;
import retrofit.RestAdapter;
import retrofit.client.OkClient;

public abstract class BaseActivity extends AppCompatActivity {

    protected abstract void initView();

    private RestAdapter getRestAdapter() {
        OkHttpClient httpClient= new OkHttpClient();
        //httpClient.setRetryOnConnectionFailure(true);
        httpClient.setConnectTimeout(5, TimeUnit.MINUTES);

        return new RestAdapter.Builder().
                setEndpoint(RetrfitInterface.url).
                setClient(new OkClient(httpClient)).
                setLogLevel(BuildConfig.DEBUG ? RestAdapter.LogLevel.FULL : RestAdapter.LogLevel.NONE).
                build();
    }

    protected RetrfitInterface getRestrofitInterface() {
        return getRestAdapter().create(RetrfitInterface.class);
    }

    protected RequestBody toRequestBody(String  value)  {
        return RequestBody.create(MediaType.parse("text/plain"), value);
    }

    protected SharedPreferences getPrefrence() {
        return PreferenceManager.getDefaultSharedPreferences(this);
    }
}
