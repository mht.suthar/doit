package myapp.com.doit.engineer.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.TextView

import java.util.ArrayList

import myapp.com.doit.R
import myapp.com.doit.engineer.model.OpenCallModel
import myapp.com.doit.engineer.model.ReasonModel


class UpdateCallStatusAdapter(private val mContext: Context, var list: ArrayList<ReasonModel>) : ArrayAdapter<ReasonModel>(mContext, 0, list) {

    @SuppressLint("SetTextI18n")
    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        var listItem = convertView
        if (listItem == null)
            listItem = LayoutInflater.from(mContext).inflate(R.layout.items_listview, parent, false)

        val data = list[position]

        val title = listItem!!.findViewById<View>(R.id.cat_name) as TextView
        title.text = data.reason

        return listItem
    }
}