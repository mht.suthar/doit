package myapp.com.doit.engineer.ui

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import kotlinx.android.synthetic.main.act_eng_dashboard_list.*
import myapp.com.doit.R
import myapp.com.doit.base.BaseActivity
import myapp.com.doit.engineer.adapter.EngineerCallOpenAdapter
import myapp.com.doit.engineer.model.EnginnerOpenCallModel
import myapp.com.doit.utils.PrefUtils
import org.json.JSONArray
import retrofit.Callback
import retrofit.RetrofitError
import retrofit.client.Response
import retrofit.mime.TypedByteArray

class EngineerCallUpdateCloseListAct : BaseActivity() {

    override fun initView() {
        txt_title.text = "Call Updation and Close"
        getList()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.act_eng_dashboard_list)
        initView()
    }

    fun onBack(view: View) {
        onBackPressed()
    }

    fun getList() {
        progressBar.visibility = View.VISIBLE
        restrofitInterface.GetCallEngineer("" + PrefUtils.gete_Employee_IDF(this@EngineerCallUpdateCloseListAct), PrefUtils.gete_Employee_IDF(this@EngineerCallUpdateCloseListAct), PrefUtils.gete_RoleId(this@EngineerCallUpdateCloseListAct), object : Callback<Response> {
            override fun success(findChildArray: Response, response: Response) {
                progressBar.visibility = View.GONE
                val res = String((findChildArray.body as TypedByteArray).bytes)

                var mList = ArrayList<EnginnerOpenCallModel>()
                var jArray = JSONArray(res)
                for (i in 0 until jArray.length()) {
                    var obj = jArray.getJSONObject(i)
                    var openCall = EnginnerOpenCallModel("" + obj.getString("CallId"),
                            "" + obj.getString("EngName"),
                            "" + obj.getString("CallRepNo"),
                            "" + obj.getString("CallRepDt"),
                            "" + obj.getString("CustName"),
                            "" + obj.getString("InstId"),
                            "" + obj.getString("CallStatus")
                    )
                    mList.add(openCall)
                    list_view.adapter = EngineerCallOpenAdapter(this@EngineerCallUpdateCloseListAct, mList)
                }
            }

            override fun failure(error: RetrofitError) {
                val merror = error.message
                Log.d("error", merror)
                progressBar.visibility = View.GONE
                Toast.makeText(this@EngineerCallUpdateCloseListAct, merror, Toast.LENGTH_LONG).show()
            }
        })
    }

    fun openUpdateEngineer(callId: String) {
        progressBar.visibility = View.VISIBLE
        restrofitInterface.GetUpdateCall(""+callId, object : Callback<Response> {
            override fun success(findChildArray: Response, response: Response) {
                progressBar.visibility = View.GONE
                val res = String((findChildArray.body as TypedByteArray).bytes)
//{"result":{"CallRepDetail":{"CallId":"8125","CallRepNo":"CP/307","CustId":"388","CustAddr":"near paper box, off mahakali road","CustName":"dummy client","CustPerson":"ali","CustCity":"Mumbai","CustPin":"400093","CustState":"maharastra","CustContact":"9987597056","LandLine":"42701234","PersonDesc":"","RMID":"20","CallUpdateDt":"4/2/2019 12:00:00 AM","ItemId":"2","ClientImage":"","SerNo":"","Status":"Your Machine in Under Warranty","AmcStatus":"Your Machine in NON AMC & Out of Warranty","ProblemType":"","ProblemId":"0","EnquiryType":"1","ItemModel":"DM BIG","ItemPrerequest":"SPACE FOR MACHINE INSTALLATION (NEED TO GIVE SIZE AND WEIGHT OF THE MACHINE)\r\nSINGLE PHASE POWER  SUPPLY { 5 AMP THREE PIN SOCKET ( EARTHING COMPULSARY)}\r\nMINERAL WATER AS PER REQUIREMENT\r\n\r\n","CallRepDt":"4/2/2019 12:00:00 AM","Pending":"False","EnquiryName":"New Machine","MachineName":"DEMINERALIZATION UNIT KIT BIG","MachineTypeID":"2","MachineTypeName":"New Installation","DemoDay":""}}}
                startActivity(Intent(this@EngineerCallUpdateCloseListAct,
                        UpdateEngineerEnquiry::class.java).putExtra("update", res))
            }

            override fun failure(error: RetrofitError) {
                progressBar.visibility = View.GONE
                val merror = error.message
                Log.d("error", merror)
                Toast.makeText(this@EngineerCallUpdateCloseListAct, merror, Toast.LENGTH_LONG).show()
            }
        })
    }
}