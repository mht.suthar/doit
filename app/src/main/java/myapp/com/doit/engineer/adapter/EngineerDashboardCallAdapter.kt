package myapp.com.doit.engineer.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.TextView

import java.util.ArrayList

import myapp.com.doit.R
import myapp.com.doit.engineer.model.OpenCallModel


class EngineerDashboardCallAdapter(private val mContext: Context, var list: ArrayList<OpenCallModel>) : ArrayAdapter<OpenCallModel>(mContext, 0, list) {

    @SuppressLint("SetTextI18n")
    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        var listItem = convertView
        if (listItem == null)
            listItem = LayoutInflater.from(mContext).inflate(R.layout.list_item_engineer_dashboard_call, parent, false)

        val data = list[position]

        val title = listItem!!.findViewById<View>(R.id.txt_title) as TextView
        val message = listItem.findViewById<View>(R.id.txt_message) as TextView
        val custname = listItem.findViewById<View>(R.id.txt_cust) as TextView

        title.text = "${data.title} : ${data.titleVal}"
        message.text = "${data.msg} : ${data.msgVal}"
        if(!TextUtils.isEmpty(data.custVal))
            custname.text = "${data.cust} : ${data.custVal}"
        else
            custname.visibility = View.GONE

        return listItem
    }
}