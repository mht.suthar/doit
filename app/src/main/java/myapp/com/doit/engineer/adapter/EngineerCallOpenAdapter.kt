package myapp.com.doit.engineer.adapter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.TextView

import java.util.ArrayList

import myapp.com.doit.R
import myapp.com.doit.engineer.model.EnginnerOpenCallModel
import myapp.com.doit.engineer.ui.EngineerCallUpdateCloseListAct


class EngineerCallOpenAdapter(private val mContext: EngineerCallUpdateCloseListAct, var list: ArrayList<EnginnerOpenCallModel>) : ArrayAdapter<EnginnerOpenCallModel>(mContext, 0, list) {

    @SuppressLint("SetTextI18n")
    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        var listItem = convertView
        if (listItem == null)
            listItem = LayoutInflater.from(mContext).inflate(R.layout.list_item_engineer_call_open, parent, false)

        val data = list[position]

        val callId = listItem!!.findViewById<View>(R.id.txt_call_id) as TextView
        val mengName = listItem.findViewById<View>(R.id.txt_eng_name) as TextView
        val custname = listItem.findViewById<View>(R.id.txt_cust) as TextView
        val repNp = listItem.findViewById<View>(R.id.txt_call_rep_no) as TextView
        val status = listItem.findViewById<View>(R.id.txt_status) as TextView

        callId.text = "Call Id : ${data.CallId}"
        mengName.text = "Eng Name : ${data.EngName}"
        custname.text = "Cust Name : ${data.CustName}"
        repNp.text = "Rep No : ${data.CallRepNo}"
        status.text = ""+data.CallStatus

        status.setOnClickListener {
            if(!data.CallStatus.equals("completed", true)){
                mContext.openUpdateEngineer(data.CallId)
            }
        }

        return listItem
    }
}