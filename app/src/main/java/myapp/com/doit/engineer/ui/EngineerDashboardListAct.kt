package myapp.com.doit.engineer.ui

import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import kotlinx.android.synthetic.main.act_eng_dashboard_list.*
import myapp.com.doit.R
import myapp.com.doit.base.BaseActivity
import myapp.com.doit.engineer.adapter.EngineerDashboardCallAdapter
import myapp.com.doit.engineer.model.OpenCallModel
import myapp.com.doit.utils.PrefUtils
import org.json.JSONException
import org.json.JSONObject
import retrofit.Callback
import retrofit.RetrofitError
import retrofit.client.Response
import retrofit.mime.TypedByteArray

class EngineerDashboardListAct : BaseActivity(), Callback<Response> {

    companion object {
        var Open_Call = 1
        var Call_Assign_To_Engineer = 2
        var Close_Call = 3
        var Call_Pending_For_Sparepart = 4
        var Call_Pending_For_NextLevelEngineer = 5
        var Total_Call_Assign_in_this_Month = 6
        var Total_Call_Resolve_in_this_Month = 7
    }

    override fun initView() {
        when (intent.getIntExtra("type", -1)) {
            Open_Call -> {
                txt_title.text = "Open Call"
                restrofitInterface.GetOpenCallList(PrefUtils.gete_Employee_IDF(this@EngineerDashboardListAct), PrefUtils.gete_RoleId(this@EngineerDashboardListAct), this)
            }
            Call_Assign_To_Engineer -> {
                txt_title.text = "Call Assign to Engineer"
                restrofitInterface.GetEngCallAssignList(PrefUtils.gete_Employee_IDF(this@EngineerDashboardListAct), PrefUtils.gete_RoleId(this@EngineerDashboardListAct), this)
            }
            Close_Call -> {
                txt_title.text = "Close call"
                restrofitInterface.Get_CloseCallsList(PrefUtils.gete_Employee_IDF(this@EngineerDashboardListAct), PrefUtils.gete_RoleId(this@EngineerDashboardListAct), this)
            }
            Call_Pending_For_Sparepart -> {
                txt_title.text = "Call Pending For Sparepart"
                restrofitInterface.GetCallSparePartList(PrefUtils.gete_Employee_IDF(this@EngineerDashboardListAct), PrefUtils.gete_RoleId(this@EngineerDashboardListAct), this)
            }
            Call_Pending_For_NextLevelEngineer -> {
                txt_title.text = "Call Pending For NextLevelEngineer"
                restrofitInterface.GetPendingNextLevelList(PrefUtils.gete_Employee_IDF(this@EngineerDashboardListAct), PrefUtils.gete_RoleId(this@EngineerDashboardListAct), this)
            }
            Total_Call_Assign_in_this_Month -> {
                txt_title.text = "Total Call Assign in this Month"
                restrofitInterface.GetTotalCallAssignList(PrefUtils.gete_Employee_IDF(this@EngineerDashboardListAct), PrefUtils.gete_RoleId(this@EngineerDashboardListAct), this)
            }
            Total_Call_Resolve_in_this_Month -> {
                txt_title.text = "Total Call Resolve in this Month"
                restrofitInterface.GetTotalCallResolveList(PrefUtils.gete_Employee_IDF(this@EngineerDashboardListAct), PrefUtils.gete_RoleId(this@EngineerDashboardListAct), this)
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.act_eng_dashboard_list)
        initView()
    }

    fun onBack(view: View){
        onBackPressed()
    }

    override fun success(findChildArray: Response?, response: Response?) {
        val response = String((findChildArray?.body as TypedByteArray).bytes)
        var jObject = JSONObject(response)
        when (intent.getIntExtra("type", -1)) {
            Open_Call -> {
                var jArray = jObject.getJSONArray("result")
                var mList = ArrayList<OpenCallModel>()
                for (i in 0 until jArray.length()) {
                    var obj = jArray.getJSONObject(i)
                    var openCall = OpenCallModel("CallRepNo", obj.getString("CallRepNo"), "CallRepDate", obj.getString("CallRepDate"))
                    mList.add(openCall)
                }
                setAdapter(mList)
            }
            Call_Assign_To_Engineer -> {
                var jArray = jObject.getJSONArray("result")
                var mList = ArrayList<OpenCallModel>()
                for (i in 0 until jArray.length()) {
                    var obj = jArray.getJSONObject(i)
                    var openCall = OpenCallModel("CallRepNo", obj.getString("CallRepNo"),
                            "CallUpdateDt", obj.getString("CallUpdateDt"),
                            "CustName", obj.getString("CustName"))
                    mList.add(openCall)
                }
                setAdapter(mList)
            }
            Call_Pending_For_Sparepart -> {
                var jArray = jObject.getJSONArray("result")
                var mList = ArrayList<OpenCallModel>()
                for (i in 0 until jArray.length()) {
                    var obj = jArray.getJSONObject(i)
                    var openCall = OpenCallModel("CallRepNo", obj.getString("CallRepNo"), "CallRepDate", "")
                    mList.add(openCall)
                }
                setAdapter(mList)
            }
            Call_Pending_For_NextLevelEngineer -> {
                var jArray = jObject.getJSONArray("result")
                var mList = ArrayList<OpenCallModel>()
                for (i in 0 until jArray.length()) {
                    var obj = jArray.getJSONObject(i)
                    var openCall = OpenCallModel("CallRepNo", obj.getString("CallRepNo"), "CallRepDate", obj.getString("CallRepDate"))
                    mList.add(openCall)
                }
                setAdapter(mList)
            }
            Total_Call_Assign_in_this_Month -> {
                var jArray = jObject.getJSONArray("result")
                var mList = ArrayList<OpenCallModel>()
                for (i in 0 until jArray.length()) {
                    var obj = jArray.getJSONObject(i)
                    var openCall = OpenCallModel("CallRepNo", obj.getString("CallRepNo"), "CallUpdateDt", obj.getString("CallUpdateDt"), "CustName", obj.getString("CustName"))
                    mList.add(openCall)
                }
                setAdapter(mList)
            }
            Total_Call_Resolve_in_this_Month -> {
                var jArray = jObject.getJSONArray("result")
                var mList = ArrayList<OpenCallModel>()
                for (i in 0 until jArray.length()) {
                    var obj = jArray.getJSONObject(i)
                    var openCall = OpenCallModel("CallRepNo", obj.getString("CallRepNo"), "CallRepDate", "")
                    mList.add(openCall)
                }
                setAdapter(mList)
            }
            Close_Call -> {
                var jArray = jObject.getJSONArray("result")
                var mList = ArrayList<OpenCallModel>()
                for (i in 0 until jArray.length()) {
                    var obj = jArray.getJSONObject(i)
                    var openCall = OpenCallModel("CallRepNo", obj.getString("CallRepNo"), "CallRepDate", obj.getString("CallRepDate"))
                    mList.add(openCall)
                }
                setAdapter(mList)
            }
        }
    }

    fun setAdapter(list : ArrayList<OpenCallModel>){
        list_view.adapter = EngineerDashboardCallAdapter(this, list = list)
    }

    override fun failure(error: RetrofitError?) {
        Toast.makeText(this@EngineerDashboardListAct, error?.message, Toast.LENGTH_LONG).show()
    }
}