package myapp.com.doit.engineer.model

data class UpdateEingineerModel(
    val result: Result
)

data class Result(
    val CallRepDetail: CallRepDetail
)

data class CallRepDetail(
    val AmcStatus: String,
    val CallId: String,
    val CallRepDt: String,
    val CallRepNo: String,
    val CallUpdateDt: String,
    val ClientImage: String,
    val CustAddr: String,
    val CustCity: String,
    val CustContact: String,
    val CustId: String,
    val CustName: String,
    val CustPerson: String,
    val CustPin: String,
    val CustState: String,
    val DemoDay: String,
    val EnquiryName: String,
    val EnquiryType: String,
    val ItemId: String,
    val ItemModel: String,
    val ItemPrerequest: String,
    val LandLine: String,
    val MachineName: String,
    val MachineTypeID: String,
    val MachineTypeName: String,
    val Pending: String,
    val PersonDesc: String,
    val ProblemId: String,
    val ProblemType: String,
    val RMID: String,
    val SerNo: String,
    val Status: String
)