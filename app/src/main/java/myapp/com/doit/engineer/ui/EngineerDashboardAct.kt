package myapp.com.doit.engineer.ui

import android.app.AlertDialog
import android.content.DialogInterface
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.support.design.widget.NavigationView
import android.support.v4.view.GravityCompat
import android.support.v4.widget.DrawerLayout
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.widget.Toolbar
import android.util.Log
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import kotlinx.android.synthetic.main.act_engineer_dashboard.*
import kotlinx.android.synthetic.main.app_bar_engineer.*
import kotlinx.android.synthetic.main.content_engineer.*
import myapp.com.doit.Aboutus
import myapp.com.doit.Contus
import myapp.com.doit.R
import myapp.com.doit.base.BaseActivity
import myapp.com.doit.engineer.ui.EngineerDashboardListAct.Companion.Call_Assign_To_Engineer
import myapp.com.doit.engineer.ui.EngineerDashboardListAct.Companion.Call_Pending_For_NextLevelEngineer
import myapp.com.doit.engineer.ui.EngineerDashboardListAct.Companion.Call_Pending_For_Sparepart
import myapp.com.doit.engineer.ui.EngineerDashboardListAct.Companion.Close_Call
import myapp.com.doit.engineer.ui.EngineerDashboardListAct.Companion.Open_Call
import myapp.com.doit.engineer.ui.EngineerDashboardListAct.Companion.Total_Call_Assign_in_this_Month
import myapp.com.doit.engineer.ui.EngineerDashboardListAct.Companion.Total_Call_Resolve_in_this_Month
import myapp.com.doit.prelogin.ui.Login
import myapp.com.doit.utils.PrefUtils
import org.json.JSONException
import org.json.JSONObject
import retrofit.Callback
import retrofit.RetrofitError
import retrofit.client.Response
import retrofit.mime.TypedByteArray

class EngineerDashboardAct : BaseActivity(), NavigationView.OnNavigationItemSelectedListener {

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.nav_updation -> {
                startActivity(Intent(this, EngineerCallUpdateCloseListAct::class.java))
            }

            R.id.nav_about_us -> {
                val it = Intent(this, Aboutus::class.java)
                it.putExtra("keyName", "0")
                startActivity(it)
            }

            R.id.nav_contact_us -> {
                val it = Intent(this, Contus::class.java)
                it.putExtra("keyName", "0")
                startActivity(it)
            }

            R.id.nav_logout -> {
                val alertDialog = AlertDialog.Builder(this)
                alertDialog.setTitle("Logout") // Sets title for your alertbox
                alertDialog.setMessage("Are you sure you want to Logout ?") // Message to be displayed on alertbox
                alertDialog.setIcon(R.drawable.power) // Icon for your alertbox
                /* When positive (yes/ok) is clicked */
                alertDialog.setPositiveButton("Yes") { dialog, which ->
                    PrefUtils.set_User_Id("", this@EngineerDashboardAct)
                    startActivity(Intent(this@EngineerDashboardAct, Login::class.java))
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                        finishAffinity()
                    } else
                        finish()
                    Toast.makeText(this@EngineerDashboardAct, "Successfully Logged Out", Toast.LENGTH_LONG).show()
                }

                /* When negative (No/cancel) button is clicked*/
                alertDialog.setNegativeButton("No") { dialog, which -> dialog.cancel() }
                alertDialog.show()

            }
        }
        drawer_layout.closeDrawer(GravityCompat.START)
        return false
    }

    override fun initView() {
        getAllCount()

        txt_open_call.setOnClickListener {
            startActivity(Intent(this, EngineerDashboardListAct::class.java).putExtra("type", Open_Call))
        }
        txt_close_call.setOnClickListener {
            startActivity(Intent(this, EngineerDashboardListAct::class.java).putExtra("type", Close_Call))
        }
        txt_call_assign_engineer.setOnClickListener {
            startActivity(Intent(this, EngineerDashboardListAct::class.java).putExtra("type", Call_Assign_To_Engineer))
        }
        txt_call_pending_spare.setOnClickListener {
            startActivity(Intent(this, EngineerDashboardListAct::class.java).putExtra("type", Call_Pending_For_Sparepart))
        }
        txt_call_next_lever_engineer.setOnClickListener {
            startActivity(Intent(this, EngineerDashboardListAct::class.java).putExtra("type", Call_Pending_For_NextLevelEngineer))
        }
        txt_total_call_month.setOnClickListener {
            startActivity(Intent(this, EngineerDashboardListAct::class.java).putExtra("type", Total_Call_Assign_in_this_Month))
        }
        txt_total_call_resolve_month.setOnClickListener {
            startActivity(Intent(this, EngineerDashboardListAct::class.java).putExtra("type", Total_Call_Resolve_in_this_Month))
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.act_engineer_dashboard)
        setSupportActionBar(toolbar)
        initDrawer()
        initView()
    }

    private fun initDrawer() {
        val toggle = ActionBarDrawerToggle(
                this, drawer_layout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close)
        drawer_layout.addDrawerListener(toggle)
        toggle.syncState()

        val navigationView = findViewById<View>(R.id.nav_view) as NavigationView
        navigationView.setNavigationItemSelectedListener(this)
    }

    private fun getAllCount() {
        restrofitInterface.GetOpenCallCount(PrefUtils.gete_Employee_IDF(this@EngineerDashboardAct), PrefUtils.gete_RoleId(this@EngineerDashboardAct), object : Callback<Response> {
            override fun success(findChildArray: Response, response: Response) {
                Log.e("res", String((findChildArray.body as TypedByteArray).bytes))
                val res = String((findChildArray.body as TypedByteArray).bytes)
                var jObj = JSONObject(res)
                var count = jObj.getJSONArray("result").getJSONObject(0).getString("OpenCalls")
                txt_open_call.text = "Open Call ($count)"
            }

            override fun failure(error: RetrofitError) {
                Toast.makeText(this@EngineerDashboardAct, error.message, Toast.LENGTH_LONG).show()
            }
        })

        restrofitInterface.GetEngCallAsignCount(PrefUtils.gete_Employee_IDF(this@EngineerDashboardAct), PrefUtils.gete_RoleId(this@EngineerDashboardAct), object : Callback<Response> {
            override fun success(findChildArray: Response, response: Response) {
                Log.e("res", String((findChildArray.body as TypedByteArray).bytes))
                val res = String((findChildArray.body as TypedByteArray).bytes)
                var jObj = JSONObject(res)
                var count = jObj.getString("result")
                txt_call_assign_engineer.text = "Call Assign To Engineer ($count)"
            }

            override fun failure(error: RetrofitError) {
                Log.d("error", error.message)
            }
        })

        restrofitInterface.GetCloseCallsCount(PrefUtils.gete_Employee_IDF(this@EngineerDashboardAct), PrefUtils.gete_RoleId(this@EngineerDashboardAct), object : Callback<Response> {
            override fun success(findChildArray: Response, response: Response) {
                Log.e("res", String((findChildArray.body as TypedByteArray).bytes))
                val res = String((findChildArray.body as TypedByteArray).bytes)
                var jObj = JSONObject(res)
                var count = jObj.getJSONArray("result").getJSONObject(0).getString("CloseCalls")
                txt_close_call.text = "Close Call ($count)"
            }

            override fun failure(error: RetrofitError) {
                Toast.makeText(this@EngineerDashboardAct, error.message, Toast.LENGTH_LONG).show()
            }
        })


        restrofitInterface.GetPendingSparePartCount(PrefUtils.gete_Employee_IDF(this@EngineerDashboardAct), PrefUtils.gete_RoleId(this@EngineerDashboardAct), object : Callback<Response> {
            override fun success(findChildArray: Response, response: Response) {
                Log.e("res", String((findChildArray.body as TypedByteArray).bytes))
                val res = String((findChildArray.body as TypedByteArray).bytes)
                var jObj = JSONObject(res)
                var count = jObj.getString("result")
                txt_call_pending_spare.text = "Call Pending For Sparepart ($count)"
            }

            override fun failure(error: RetrofitError) {
                Log.d("error", error.message)
            }
        })


        restrofitInterface.GetPendingNextLevelCount(PrefUtils.gete_Employee_IDF(this@EngineerDashboardAct), PrefUtils.gete_RoleId(this@EngineerDashboardAct), object : Callback<Response> {
            override fun success(findChildArray: Response, response: Response) {
                Log.e("res", String((findChildArray.body as TypedByteArray).bytes))
                val res = String((findChildArray.body as TypedByteArray).bytes)
                var jObj = JSONObject(res)
                var count = jObj.getString("result")
                txt_call_next_lever_engineer.text = "Call Pending For NextLevelEngineer ($count)"
            }

            override fun failure(error: RetrofitError) {
                Log.d("error", error.message)
            }
        })

        restrofitInterface.GetTotalCallAssignCount(PrefUtils.gete_Employee_IDF(this@EngineerDashboardAct), PrefUtils.gete_RoleId(this@EngineerDashboardAct), object : Callback<Response> {
            override fun success(findChildArray: Response, response: Response) {
                Log.e("res", String((findChildArray.body as TypedByteArray).bytes))
                val res = String((findChildArray.body as TypedByteArray).bytes)
                var jObj = JSONObject(res)
                var count = jObj.getString("result")
                txt_total_call_month.text = "Total Call Assign in this Month ($count)"
            }

            override fun failure(error: RetrofitError) {
                Log.d("error", error.message)
            }
        })

        restrofitInterface.GetTotalCallResolveCount(PrefUtils.gete_Employee_IDF(this@EngineerDashboardAct), PrefUtils.gete_RoleId(this@EngineerDashboardAct), object : Callback<Response> {
            override fun success(findChildArray: Response, response: Response) {
                Log.e("res", String((findChildArray.body as TypedByteArray).bytes))
                val res = String((findChildArray.body as TypedByteArray).bytes)
                var jObj = JSONObject(res)
                var count = jObj.getString("result")
                txt_total_call_resolve_month.text = "Total Call Resolve in this Month ($count)"
            }

            override fun failure(error: RetrofitError) {
                Log.d("error", error.message)
            }
        })
    }

    override fun onBackPressed() {
        if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
            drawer_layout.closeDrawer(GravityCompat.START)
        } else {
            AlertDialog.Builder(this)
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .setTitle("Closing Activity")
                    .setMessage("Are you sure you want to close this activity?")
                    .setPositiveButton("Yes") { dialog, which ->
                        finish()
                    }
                    .setNegativeButton("No", null)
                    .show()
        }
    }
}