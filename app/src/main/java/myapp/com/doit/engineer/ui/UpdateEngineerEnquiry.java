package myapp.com.doit.engineer.ui;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.DatePicker;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.GsonBuilder;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.RequestBody;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;

import myapp.com.doit.R;
import myapp.com.doit.base.BaseActivity;
import myapp.com.doit.dashboard.ui.EnquiryListAct;
import myapp.com.doit.databinding.ActUpdateEnquiryBinding;
import myapp.com.doit.engineer.adapter.UpdateCallStatusAdapter;
import myapp.com.doit.engineer.model.CallRepDetail;
import myapp.com.doit.engineer.model.ReasonModel;
import myapp.com.doit.engineer.model.UpdateEingineerModel;
import myapp.com.doit.utils.PrefUtils;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedByteArray;
import retrofit.mime.TypedFile;


public class UpdateEngineerEnquiry extends BaseActivity {

    String callStatusId = "", reasonId = "";
    private ProgressDialog loading;
    private ActUpdateEnquiryBinding mBinding;
    private UpdateEingineerModel updateEng;
    ArrayList<ReasonModel> mReasonList = new ArrayList<>();
    ArrayList<ReasonModel> mStateList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.act_update_enquiry);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        mBinding.title.setText("Update Enquiry");

        String response = getIntent().getExtras().getString("update");
        updateEng = new GsonBuilder().create().fromJson(response, UpdateEingineerModel.class);
        mBinding.setEng(updateEng.getResult().getCallRepDetail());

        long date = System.currentTimeMillis();
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
        String dateString = sdf.format(date);

        //show present date
        mBinding.selectDate.setText(dateString);

        mBinding.btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                proceedSaveEntry();
            }
        });

        mBinding.linPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openCamera();
            }
        });

        mBinding.txtReason.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openReasonDialoag();
            }
        });

        mBinding.status.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openStatusDialoag();
            }
        });

        mBinding.leftArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        mBinding.linWarrantyFrom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openDatePicker(0);
            }
        });

        mBinding.linWarrantyTo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openDatePicker(1);
            }
        });

        getStatusList();
        getPendingReasonList();
    }

    private void openCamera() {
        CropImage.activity()
                .setGuidelines(CropImageView.Guidelines.ON)
                .start(this);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                Uri resultUri = result.getUri();
                File file = new File(resultUri.getPath());
                mBinding.txtFilePath.setText(file.getAbsolutePath());
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
            }
        }
    }

    private void proceedSaveEntry() {
        CallRepDetail callRepDetail = updateEng.getResult().getCallRepDetail();


        if ((mBinding.status.getText().toString().equalsIgnoreCase("Completed")
                && callRepDetail.getEnquiryName().equalsIgnoreCase("new machine")
                && callRepDetail.getMachineTypeName().equalsIgnoreCase("demo"))
                ||
                (mBinding.status.getText().toString().equalsIgnoreCase("Completed")
                        && callRepDetail.getEnquiryName().equalsIgnoreCase("complaint")
                        && callRepDetail.getMachineTypeID().equalsIgnoreCase("0"))) {
            if (TextUtils.isEmpty(mBinding.edtUnique.getText().toString())) {
                Toast.makeText(this, "Enter Unique Code", Toast.LENGTH_SHORT).show();
            } else {
                updateCall(callStatusId, "" + callRepDetail.getEnquiryType(), "" + callRepDetail.getCallId(),
                        "" + mBinding.edtRemark.getText().toString(),
                        "" + mBinding.edtUnique.getText().toString(), "", "", "", "", "", "",
                        "", "", "");
            }
        } else if ((mBinding.status.getText().toString().equalsIgnoreCase("Pending")
                && callRepDetail.getEnquiryName().equalsIgnoreCase("new machine")
                && callRepDetail.getMachineTypeName().equalsIgnoreCase("demo"))
                ||
                (mBinding.status.getText().toString().equalsIgnoreCase("Pending")
                        && callRepDetail.getEnquiryName().equalsIgnoreCase("new machine")
                        && callRepDetail.getMachineTypeName().equalsIgnoreCase("new installation"))) {
            updateCall(callStatusId, "" + callRepDetail.getEnquiryType(), "" + callRepDetail.getCallId(),
                    "" + mBinding.edtRemark.getText().toString(), "", "", "", "", "", "", "",
                    "", "", "");
        } else if (mBinding.status.getText().toString().equalsIgnoreCase("Completed")
                && callRepDetail.getEnquiryName().equalsIgnoreCase("new machine")
                && callRepDetail.getMachineTypeName().equalsIgnoreCase("new installation")) {
            if (TextUtils.isEmpty(mBinding.edtUnique.getText().toString())) {
                Toast.makeText(this, "Enter Unique Codee", Toast.LENGTH_SHORT).show();
            } else if (TextUtils.isEmpty(mBinding.edtSerial.getText().toString())) {
                Toast.makeText(this, "Enter Serial No", Toast.LENGTH_SHORT).show();
            } else if (TextUtils.isEmpty(mBinding.txtWarrantyFrom.getText().toString())) {
                Toast.makeText(this, "Select Date From", Toast.LENGTH_SHORT).show();
            } else if (TextUtils.isEmpty(mBinding.txtWarrantyTo.getText().toString())) {
                Toast.makeText(this, "Select Date To", Toast.LENGTH_SHORT).show();
            } else {
                updateCall(callStatusId, "" + callRepDetail.getEnquiryType(), "" + callRepDetail.getCallId(),
                        "" + mBinding.edtRemark.getText().toString(),
                        "" + mBinding.edtUnique.getText().toString(), "", "" + mBinding.edtSerial.getText().toString(),
                        "" + mBinding.txtWarrantyFrom.getText().toString(), "" + mBinding.txtWarrantyTo.getText().toString(),
                        "", "",
                        "", "", "");
            }
        }else if (mBinding.status.getText().toString().equalsIgnoreCase("Completed")
                && callRepDetail.getEnquiryName().equalsIgnoreCase("service")
                && callRepDetail.getMachineTypeID().equalsIgnoreCase("0")) {

            if (TextUtils.isEmpty(mBinding.edtUnique.getText().toString())) {
                Toast.makeText(this, "Enter Unique Code", Toast.LENGTH_SHORT).show();
            } else if (TextUtils.isEmpty(mBinding.txtWarrantyFrom.getText().toString())) {
                Toast.makeText(this, "Enter Next Due Date", Toast.LENGTH_SHORT).show();
            } else {
                updateCall(callStatusId, "" + callRepDetail.getEnquiryType(), "" + callRepDetail.getCallId(),
                        "" + mBinding.edtRemark.getText().toString(),
                        "" + mBinding.edtUnique.getText().toString(), "", "", "", "",
                        "" + mBinding.txtWarrantyFrom.getText().toString(), "",
                        "", "", "");
                ;
            }

        } else if (mBinding.status.getText().toString().equalsIgnoreCase("Pending")
                && (callRepDetail.getEnquiryName().equalsIgnoreCase("complaint") || callRepDetail.getEnquiryName().equalsIgnoreCase("service"))
                && callRepDetail.getMachineTypeID().equalsIgnoreCase("0")) {
            if (TextUtils.isEmpty(mBinding.txtReason.getText().toString())) {
                Toast.makeText(this, "Select Reason", Toast.LENGTH_SHORT).show();
            } else {
                updateCall(callStatusId, "" + callRepDetail.getEnquiryType(), "" + callRepDetail.getCallId(),
                        "" + mBinding.edtRemark.getText().toString(),
                        "", ""+mBinding.edtRemarkCust.getText().toString(), "", "", "", "",
                        "" + reasonId, ""+mBinding.edtRemarkCust.getText().toString() + mBinding.txtReason.getText().toString(),
                        ""+mBinding.chkNext.isSelected(), "");
            }
        } else {
            Toast.makeText(this, "Select Status", Toast.LENGTH_SHORT).show();
        }
    }


    @Override
    protected void initView() {

    }

    private void updateCall(String callStatusId, String enqType, String callId, String engRemark,
                            String uniqCode, String callRemark, String serNo, String warrFrom,
                            String warrTo, String nextDueDt, String pendingId, String pendingReason,
                            String isNextLevel, String totalAmt) {
        showLoader();

        TypedFile typedFile = null;
        if (!TextUtils.isEmpty(mBinding.txtFilePath.getText().toString())) {
            File file = new File(mBinding.txtFilePath.getText().toString());
            typedFile = new TypedFile("multipart/form-data", file);
        }

        //getRestrofitInterface().Get_SaveEnquiry(PrefUtils.gete_User_Id(AddNewEnquiry.this), f_enq_desc, RM_ID, EnquiryType_ID, f_Problem_Type_ID, f_subtype_ID, "0", "NULL", "10014", PrefUtils.gete_RoleId(AddNewEnquiry.this), new Callback<Response>() {
        getRestrofitInterface().saveCallUpdate("" + callStatusId, "" + enqType, "" + callId,
                "" + engRemark, "" + uniqCode, "" + callRemark, "" + serNo,
                "" + warrFrom, "" + warrTo, "" + nextDueDt, "" + pendingId,
                "" + pendingReason, "" + isNextLevel, "" + totalAmt, "" + PrefUtils.gete_RoleId(UpdateEngineerEnquiry.this),
                typedFile, new Callback<Response>() {
                    //getRestrofitInterface().saveEnquiry(map, new Callback<Response>() {
                    @Override
                    public void success(Response findChildArray, Response response) {

                        Log.e("status", new String(((TypedByteArray) findChildArray.getBody()).getBytes()));

                        String result1 = new String(((TypedByteArray) findChildArray.getBody()).getBytes());

                        JSONObject jObj = null;
                        try {
                            jObj = new JSONObject(result1);
//{"result":{"EnqID":0,"IsSuccess":false,"ErrorMessage":"","Message":"Unique Code DoesNot Match"}}
                            if (jObj.length() > 0) {
                                JSONObject obj = jObj.getJSONObject("result");
                                String IsSuccess = obj.getString("IsSuccess");
                                if (IsSuccess.equalsIgnoreCase("true")) {
                                    Intent it = new Intent(UpdateEngineerEnquiry.this, EngineerCallUpdateCloseListAct.class);
                                    startActivity(it);
                                    finish();
                                } else {
                                    Toast.makeText(UpdateEngineerEnquiry.this, "" + obj.getString("Message"), Toast.LENGTH_SHORT).show();
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        hideLoader();
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        String merror = error.getMessage();
                        Log.d("error", merror);
                        Toast.makeText(UpdateEngineerEnquiry.this, merror, Toast.LENGTH_LONG).show();

                    }
                });
    }

    private void showLoader() {
        try {
            loading = new ProgressDialog(UpdateEngineerEnquiry.this);
            loading.setMessage("Please Wait Loading data ....");
            loading.show();
            loading.setCancelable(false);
        } catch (Exception e) {
        }
    }

    private void hideLoader() {
        //loading.dismiss();
        if (loading != null) {
            loading.dismiss();
            //loading.cancel();
        }
    }

    private void getPendingReasonList() {
        getRestrofitInterface().GetPendingReasonList(new Callback<Response>() {
            @Override
            public void success(Response findChildArray, Response response) {
                String result1 = new String(((TypedByteArray) findChildArray.getBody()).getBytes());
                Log.e("Response", "" + result1);
                try {
                    JSONArray jsonArray = new JSONArray(result1);
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject obj = jsonArray.getJSONObject(i);
                        mReasonList.add(new ReasonModel("" + obj.getInt("ID"), obj.getString("Reason")));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                //CallStatusListPojo callStatusListPojo = new GsonBuilder().create().fromJson(result1, CallStatusListPojo.class);
            }

            @Override
            public void failure(RetrofitError error) {
                loading.dismiss();
                String merror = error.getMessage();
                Log.d("error", merror);
                Toast.makeText(UpdateEngineerEnquiry.this, merror, Toast.LENGTH_LONG).show();
            }
        });
    }

    private void getStatusList() {
        getRestrofitInterface().GetCallStatusDropDownList(new Callback<Response>() {
            @Override
            public void success(Response findChildArray, Response response) {
                String result1 = new String(((TypedByteArray) findChildArray.getBody()).getBytes());
                Log.e("Response", "" + result1);
                try {
                    JSONArray jsonArray = new JSONArray(result1);
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject obj = jsonArray.getJSONObject(i);
                        mStateList.add(new ReasonModel("" + obj.getInt("ID"), obj.getString("Call_Status")));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void failure(RetrofitError error) {
                loading.dismiss();
                String merror = error.getMessage();
                Log.d("error", merror);
                Toast.makeText(UpdateEngineerEnquiry.this, merror, Toast.LENGTH_LONG).show();
            }
        });
    }

    private void openDatePicker(final int type) {
        Calendar calendar = Calendar.getInstance();
        DatePickerDialog dialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int year, int monthOfYear, int dayOfMonth) {
                int month = monthOfYear + 1;
                //String mDate = month + "/" + dayOfMonth + "/" + year;
                String mDate = year + "-" + month + "-" + dayOfMonth;
                if (type == 0) {
                    //mBinding.tvDateFrom.setText(DateUtilz.changeDateFormat(DateUtilz.SERVER_SEND_DATE_FORMAT, DateUtilz.SERVER_SEND_DATE_FORMAT, mDate));
                    mBinding.txtWarrantyFrom.setText(mDate);
                } else {
                    //mBinding.tvDateTo.setText(DateUtilz.changeDateFormat(DateUtilz.SERVER_SEND_DATE_FORMAT, DateUtilz.SERVER_SEND_DATE_FORMAT, mDate));
                    mBinding.txtWarrantyTo.setText(mDate);
                }
            }
        }, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));

        //dialog.getDatePicker().setMaxDate(System.currentTimeMillis());
        if (type == 0 && !TextUtils.isEmpty(mBinding.txtWarrantyFrom.getText().toString()))
            dialog.updateDate(Integer.parseInt(mBinding.txtWarrantyFrom.getText().toString().split("-")[0]),
                    Integer.parseInt(mBinding.txtWarrantyFrom.getText().toString().split("-")[1]) - 1,
                    Integer.parseInt(mBinding.txtWarrantyFrom.getText().toString().split("-")[2]));
        else if (type == 1 && !TextUtils.isEmpty(mBinding.txtWarrantyTo.getText().toString()))
            dialog.updateDate(Integer.parseInt(mBinding.txtWarrantyTo.getText().toString().split("-")[0]),
                    Integer.parseInt(mBinding.txtWarrantyTo.getText().toString().split("-")[1]) - 1,
                    Integer.parseInt(mBinding.txtWarrantyTo.getText().toString().split("-")[2]));
        dialog.show();
    }

    private void openStatusDialoag() {
        final Dialog dilogselect_EnquiryType = new Dialog(this);
        dilogselect_EnquiryType.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dilogselect_EnquiryType.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dilogselect_EnquiryType.setContentView(R.layout.dialogenqtyp_option);

        ListView list_view = (ListView) dilogselect_EnquiryType.findViewById(R.id.lis_emp);
        TextView txt = (TextView) dilogselect_EnquiryType.findViewById(R.id.txt_title);
        txt.setText("Select Status");
        list_view.setAdapter(new UpdateCallStatusAdapter(this, mStateList));

        list_view.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                dilogselect_EnquiryType.dismiss();
                mBinding.status.setText(mStateList.get(i).getReason());
                callStatusId = mStateList.get(i).getId();
                CallRepDetail callRepDetail = updateEng.getResult().getCallRepDetail();
                if ((mStateList.get(i).getReason().equalsIgnoreCase("Completed")
                        && callRepDetail.getEnquiryName().equalsIgnoreCase("new machine")
                        && callRepDetail.getMachineTypeName().equalsIgnoreCase("demo"))
                        ||
                        (mStateList.get(i).getReason().equalsIgnoreCase("Completed")
                                && callRepDetail.getEnquiryName().equalsIgnoreCase("complaint")
                                && callRepDetail.getMachineTypeID().equalsIgnoreCase("0"))) {
                    mBinding.linUniq.setVisibility(View.VISIBLE);
                    mBinding.linRemarkCustomer.setVisibility(View.VISIBLE);
                    mBinding.linEngineerRemark.setVisibility(View.VISIBLE);

                    mBinding.linSeriNo.setVisibility(View.GONE);
                    mBinding.linWarrantyFrom.setVisibility(View.GONE);
                    mBinding.linWarrantyTo.setVisibility(View.GONE);
                    mBinding.txtMachineWarranty.setVisibility(View.GONE);
                    mBinding.linPending.setVisibility(View.GONE);
                } else if ((mStateList.get(i).getReason().equalsIgnoreCase("Pending")
                        && callRepDetail.getEnquiryName().equalsIgnoreCase("new machine")
                        && callRepDetail.getMachineTypeName().equalsIgnoreCase("demo"))
                        ||
                        (mStateList.get(i).getReason().equalsIgnoreCase("Pending")
                                && callRepDetail.getEnquiryName().equalsIgnoreCase("new machine")
                                && callRepDetail.getMachineTypeName().equalsIgnoreCase("new installation"))) {
                    mBinding.linUniq.setVisibility(View.GONE);
                    mBinding.linSeriNo.setVisibility(View.GONE);
                    mBinding.linWarrantyFrom.setVisibility(View.GONE);
                    mBinding.linWarrantyTo.setVisibility(View.GONE);
                    mBinding.txtMachineWarranty.setVisibility(View.GONE);
                    mBinding.linRemarkCustomer.setVisibility(View.GONE);
                    mBinding.linEngineerRemark.setVisibility(View.VISIBLE);
                } else if (mStateList.get(i).getReason().equalsIgnoreCase("Completed")
                        && callRepDetail.getEnquiryName().equalsIgnoreCase("new machine")
                        && callRepDetail.getMachineTypeName().equalsIgnoreCase("new installation")) {
                    mBinding.linUniq.setVisibility(View.VISIBLE);
                    mBinding.linSeriNo.setVisibility(View.VISIBLE);
                    mBinding.linWarrantyFrom.setVisibility(View.VISIBLE);
                    mBinding.linWarrantyTo.setVisibility(View.VISIBLE);
                    mBinding.linRemarkCustomer.setVisibility(View.VISIBLE);
                    mBinding.linEngineerRemark.setVisibility(View.VISIBLE);

                    mBinding.linPending.setVisibility(View.GONE);
                    mBinding.txtMachineWarranty.setVisibility(View.GONE);
                } else if (mStateList.get(i).getReason().equalsIgnoreCase("Completed")
                        && callRepDetail.getEnquiryName().equalsIgnoreCase("service")
                        && callRepDetail.getMachineTypeID().equalsIgnoreCase("0")) {
                    mBinding.linUniq.setVisibility(View.VISIBLE);
                    mBinding.linWarrantyFrom.setVisibility(View.VISIBLE);
                    //mBinding.txtMachineWarranty.setVisibility(View.VISIBLE);
                    mBinding.txtWarrantyFromTitle.setText("Next Due Date : ");
                    mBinding.linRemarkCustomer.setVisibility(View.VISIBLE);
                    mBinding.linEngineerRemark.setVisibility(View.VISIBLE);


                    mBinding.linSeriNo.setVisibility(View.GONE);
                    mBinding.linWarrantyTo.setVisibility(View.GONE);
                    mBinding.linPending.setVisibility(View.GONE);
                } else if (mStateList.get(i).getReason().equalsIgnoreCase("Pending")
                        && (callRepDetail.getEnquiryName().equalsIgnoreCase("complaint") || callRepDetail.getEnquiryName().equalsIgnoreCase("service"))
                        && callRepDetail.getMachineTypeID().equalsIgnoreCase("0")) {
                    mBinding.linPending.setVisibility(View.VISIBLE);
                    mBinding.linRemarkCustomer.setVisibility(View.GONE);
                    mBinding.linEngineerRemark.setVisibility(View.VISIBLE);

                    mBinding.linUniq.setVisibility(View.GONE);
                    mBinding.linWarrantyFrom.setVisibility(View.GONE);
                    mBinding.txtMachineWarranty.setVisibility(View.GONE);
                    mBinding.linSeriNo.setVisibility(View.GONE);
                    mBinding.linWarrantyTo.setVisibility(View.GONE);
                } else {
                    mBinding.linPending.setVisibility(View.GONE);
                    mBinding.linUniq.setVisibility(View.GONE);
                    mBinding.linWarrantyFrom.setVisibility(View.GONE);
                    mBinding.txtMachineWarranty.setVisibility(View.GONE);
                    mBinding.linSeriNo.setVisibility(View.GONE);
                    mBinding.linWarrantyTo.setVisibility(View.GONE);
                    mBinding.linRemarkCustomer.setVisibility(View.GONE);
                    mBinding.linEngineerRemark.setVisibility(View.GONE);
                }
            }
        });

        dilogselect_EnquiryType.show();
    }

    private void openReasonDialoag() {
        final Dialog dilogselect_EnquiryType = new Dialog(this);
        dilogselect_EnquiryType.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dilogselect_EnquiryType.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dilogselect_EnquiryType.setContentView(R.layout.dialogenqtyp_option);

        ListView list_view = (ListView) dilogselect_EnquiryType.findViewById(R.id.lis_emp);
        TextView txt = (TextView) dilogselect_EnquiryType.findViewById(R.id.txt_title);
        txt.setText("Select Reason");
        list_view.setAdapter(new UpdateCallStatusAdapter(this, mReasonList));

        list_view.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                dilogselect_EnquiryType.cancel();
                mBinding.txtReason.setText(mReasonList.get(i).getReason());
                if (mReasonList.get(i).getReason().equalsIgnoreCase("Reason for Pending Call")) {
                    mBinding.linNextEnginner.setVisibility(View.VISIBLE);
                    mBinding.linRemarkCustomer.setVisibility(View.VISIBLE);
                    mBinding.linEngineerRemark.setVisibility(View.VISIBLE);
                    mBinding.txtRemarkCust.setText("Pending Reason");
                } else {
                    mBinding.linNextEnginner.setVisibility(View.GONE);
                }
                reasonId = mReasonList.get(i).getId();
            }
        });

        dilogselect_EnquiryType.show();
    }
}
