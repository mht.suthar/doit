package myapp.com.doit.helper;

import com.google.gson.annotations.SerializedName;

/**
 * Created by zero on 11/11/16.
 */
public class OpenCalls_List {

    @SerializedName("CallRepNo")
    private String CallRepNo;

    @SerializedName("CallRepDate")
    private String CallRepDate;

    public String getCallRepNo() {
        return CallRepNo;
    }

    public void setCallRepNo(String callRepNo) {
        CallRepNo = callRepNo;
    }

    public String getCallRepDate() {
        return CallRepDate;
    }

    public void setCallRepDate(String callRepDate) {
        CallRepDate = callRepDate;
    }
}
