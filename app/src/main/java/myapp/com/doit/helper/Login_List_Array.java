package myapp.com.doit.helper;

import java.util.ArrayList;

import myapp.com.doit.prelogin.model.Login_data;

/**
 * Created by zero on 11/11/16.
 */
public class Login_List_Array {

    public ArrayList<Login_data> tncsub_list;

    public ArrayList<Login_data> getTncsub_list() {
        return tncsub_list;
    }

    public void setTncsub_list(ArrayList<Login_data> tncsub_list) {
        this.tncsub_list = tncsub_list;
    }
}
