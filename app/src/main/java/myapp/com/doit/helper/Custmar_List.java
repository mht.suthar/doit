package myapp.com.doit.helper;

import com.google.gson.annotations.SerializedName;

/**
 * Created by zero on 11/11/16.
 */
public class Custmar_List {

    private String CustId;
    private String CustName;

    public Custmar_List(String custId, String custName) {
        CustId = custId;
        CustName = custName;
    }


    public String getCustId() {
        return CustId;
    }

    public void setCustId(String custId) {
        CustId = custId;
    }

    public String getCustName() {
        return CustName;
    }

    public void setCustName(String custName) {
        CustName = custName;
    }
}
