package myapp.com.doit.helper;

/**
 * Created by zero on 11/11/16.
 */
public class Sub_type_List {

    private String Problem_ID;
    private String Problem_Name;

    public Sub_type_List(String problem_ID, String problem_Name) {
        Problem_ID = problem_ID;
        Problem_Name = problem_Name;
    }

    public String getProblem_ID() {
        return Problem_ID;
    }

    public void setProblem_ID(String problem_ID) {
        Problem_ID = problem_ID;
    }

    public String getProblem_Name() {
        return Problem_Name;
    }

    public void setProblem_Name(String problem_Name) {
        Problem_Name = problem_Name;
    }
}
