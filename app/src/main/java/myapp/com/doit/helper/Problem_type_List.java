package myapp.com.doit.helper;

/**
 * Created by zero on 11/11/16.
 */
public class Problem_type_List {

    private String ID;
    private String Problem;

    public Problem_type_List(String ID, String problem) {
        this.ID = ID;
        Problem = problem;
    }

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getProblem() {
        return Problem;
    }

    public void setProblem(String problem) {
        Problem = problem;
    }
}
