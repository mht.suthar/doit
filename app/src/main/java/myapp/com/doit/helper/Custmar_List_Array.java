package myapp.com.doit.helper;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by zero on 11/11/16.
 */
public class Custmar_List_Array {

    public ArrayList<Custmar_List> category_List;

    public ArrayList<Custmar_List> getCategory_List() {
        return category_List;
    }

    public void setCategory_List(ArrayList<Custmar_List> category_List) {
        this.category_List = category_List;
    }
}
