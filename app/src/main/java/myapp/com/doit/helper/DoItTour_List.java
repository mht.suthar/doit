package myapp.com.doit.helper;

import com.google.gson.annotations.SerializedName;

/**
 * Created by zero on 11/11/16.
 */
public class DoItTour_List {


    private String FAQ_Name;

    private String Description;

    public DoItTour_List(String FAQ_Name, String description) {
        this.FAQ_Name = FAQ_Name;
        Description = description;
    }

    public String getFAQ_Name() {
        return FAQ_Name;
    }

    public void setFAQ_Name(String FAQ_Name) {
        this.FAQ_Name = FAQ_Name;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }
}
