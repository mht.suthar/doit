package myapp.com.doit.helper;

import java.util.ArrayList;

/**
 * Created by zero on 11/11/16.
 */
public class Problem_type_List_Array {

    public ArrayList<Problem_type_List> category_List;

    public ArrayList<Problem_type_List> getCategory_List() {
        return category_List;
    }

    public void setCategory_List(ArrayList<Problem_type_List> category_List) {
        this.category_List = category_List;
    }
}
