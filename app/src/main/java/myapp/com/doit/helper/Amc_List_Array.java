package myapp.com.doit.helper;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by zero on 11/11/16.
 */
public class Amc_List_Array {

    @SerializedName("result")
    private ArrayList<Amc_List> result;

    public ArrayList<Amc_List> getResult() {
        return result;
    }

    public void setResult(ArrayList<Amc_List> result) {
        this.result = result;
    }

}
