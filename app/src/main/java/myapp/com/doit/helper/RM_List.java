package myapp.com.doit.helper;

/**
 * Created by zero on 11/11/16.
 */
public class RM_List {

    private String EngId;
    private String EngName;

    public RM_List(String engId, String engName) {
        EngId = engId;
        EngName = engName;
    }

    public String getEngId() {
        return EngId;
    }

    public void setEngId(String engId) {
        EngId = engId;
    }

    public String getEngName() {
        return EngName;
    }

    public void setEngName(String engName) {
        EngName = engName;
    }
}
