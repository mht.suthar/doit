package myapp.com.doit.helper;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by zero on 11/11/16.
 */
public class Notification_List_Array {

    @SerializedName("result")
    private ArrayList<Notification_List> result;

    public ArrayList<Notification_List> getResult() {
        return result;
    }

    public void setResult(ArrayList<Notification_List> result) {
        this.result = result;
    }
}
