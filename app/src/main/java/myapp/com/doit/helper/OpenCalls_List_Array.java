package myapp.com.doit.helper;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by zero on 11/11/16.
 */
public class OpenCalls_List_Array {

    @SerializedName("result")
    private ArrayList<OpenCalls_List> result;

    public ArrayList<OpenCalls_List> getResult() {
        return result;
    }

    public void setResult(ArrayList<OpenCalls_List> result) {
        this.result = result;
    }
}
