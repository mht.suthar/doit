package myapp.com.doit.helper;

import com.google.gson.annotations.SerializedName;

/**
 * Created by zero on 11/11/16.
 */
public class Callid_List {


    private String CallId;

    private String CallRepNo;

    public Callid_List(String callId, String callRepNo) {
        CallId = callId;
        CallRepNo = callRepNo;
    }

    public String getCallId() {
        return CallId;
    }

    public void setCallId(String callId) {
        CallId = callId;
    }

    public String getCallRepNo() {
        return CallRepNo;
    }

    public void setCallRepNo(String callRepNo) {
        CallRepNo = callRepNo;
    }
}
