package myapp.com.doit.helper;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by zero on 11/11/16.
 */
public class MachineDetailList_List_Array {

    @SerializedName("result")
    private ArrayList<MachineDetailList_List> result;

    public ArrayList<MachineDetailList_List> getResult() {
        return result;
    }

    public void setResult(ArrayList<MachineDetailList_List> result) {
        this.result = result;
    }
}
