package myapp.com.doit.helper;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by zero on 11/11/16.
 */
public class Complaint_List_Array {

    @SerializedName("result")
    private ArrayList<Complaint_List> result;

    public ArrayList<Complaint_List> getResult() {
        return result;
    }

    public void setResult(ArrayList<Complaint_List> result) {
        this.result = result;
    }
}
