package myapp.com.doit.helper;

/**
 * Created by zero on 11/11/16.
 */
public class Enquiry_type_List {

    private String ID;
    private String Enquiry;

    public Enquiry_type_List(String ID, String enquiry) {
        this.ID = ID;
        Enquiry = enquiry;
    }

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getEnquiry() {
        return Enquiry;
    }

    public void setEnquiry(String enquiry) {
        Enquiry = enquiry;
    }
}
