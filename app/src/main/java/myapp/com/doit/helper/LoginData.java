package myapp.com.doit.helper;

/**
 * Created by zero on 4/7/16.
 */
public class LoginData {

    private String UserLoginName;
    private String password;
    private String RoleIDF;


    public LoginData(String userLoginName, String password, String roleIDF) {
        UserLoginName = userLoginName;
        this.password = password;
        RoleIDF = roleIDF;
    }

    public String getUserLoginName() {
        return UserLoginName;
    }

    public void setUserLoginName(String userLoginName) {
        UserLoginName = userLoginName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRoleIDF() {
        return RoleIDF;
    }

    public void setRoleIDF(String roleIDF) {
        RoleIDF = roleIDF;
    }
}
