package myapp.com.doit.helper;

import com.google.gson.annotations.SerializedName;

/**
 * Created by zero on 11/11/16.
 */
public class Notification_List {

    @SerializedName("Notification")
    private String Notification;

    @SerializedName("NotificationDate")
    private String NotificationDate;

    public String getNotification() {
        return Notification;
    }

    public void setNotification(String notification) {
        Notification = notification;
    }

    public String getNotificationDate() {
        return NotificationDate;
    }

    public void setNotificationDate(String notificationDate) {
        NotificationDate = notificationDate;
    }
}
