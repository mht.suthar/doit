package myapp.com.doit.helper;

import com.google.gson.annotations.SerializedName;

/**
 * Created by zero on 11/11/16.
 */
public class MachineDetailList_List {

    @SerializedName("ItemId")
    private String ItemId;

    @SerializedName("SerNo")
    private String SerNo;

    @SerializedName("WarrStatus")
    private String WarrStatus;

    @SerializedName("AmcStatus")
    private String AmcStatus;

    @SerializedName("ItemModel")
    private String ItemModel;

    @SerializedName("ItemPrerequest")
    private String ItemPrerequest;


    public String getItemId() {
        return ItemId;
    }

    public void setItemId(String itemId) {
        ItemId = itemId;
    }

    public String getSerNo() {
        return SerNo;
    }

    public void setSerNo(String serNo) {
        SerNo = serNo;
    }

    public String getWarrStatus() {
        return WarrStatus;
    }

    public void setWarrStatus(String warrStatus) {
        WarrStatus = warrStatus;
    }

    public String getAmcStatus() {
        return AmcStatus;
    }

    public void setAmcStatus(String amcStatus) {
        AmcStatus = amcStatus;
    }

    public String getItemModel() {
        return ItemModel;
    }

    public void setItemModel(String itemModel) {
        ItemModel = itemModel;
    }

    public String getItemPrerequest() {
        return ItemPrerequest;
    }

    public void setItemPrerequest(String itemPrerequest) {
        ItemPrerequest = itemPrerequest;
    }
}
