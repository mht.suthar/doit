package myapp.com.doit.helper;

import com.google.gson.annotations.SerializedName;

/**
 * Created by zero on 11/11/16.
 */
public class Amc_List {

    @SerializedName("ItemID")
    private String ItemID;

    @SerializedName("Itemname")
    private String Itemname;

    @SerializedName("SerialNo")
    private String SerialNo;

    @SerializedName("WarrantyExpiredate")
    private String WarrantyExpiredate;

    @SerializedName("AMCExpireDate")
    private String AMCExpireDate;

    @SerializedName("UpcomingDate")
    private String upcomingDate;

    public String getUpcomingDate() {
        return upcomingDate;
    }

    public void setUpcomingDate(String upcomingDate) {
        this.upcomingDate = upcomingDate;
    }

    public String getItemID() {
        return ItemID;
    }

    public void setItemID(String itemID) {
        ItemID = itemID;
    }

    public String getItemname() {
        return Itemname;
    }

    public void setItemname(String itemname) {
        Itemname = itemname;
    }

    public String getSerialNo() {
        return SerialNo;
    }

    public void setSerialNo(String serialNo) {
        SerialNo = serialNo;
    }

    public String getWarrantyExpiredate() {
        return WarrantyExpiredate;
    }

    public void setWarrantyExpiredate(String warrantyExpiredate) {
        WarrantyExpiredate = warrantyExpiredate;
    }

    public String getAMCExpireDate() {
        return AMCExpireDate;
    }

    public void setAMCExpireDate(String AMCExpireDate) {
        this.AMCExpireDate = AMCExpireDate;
    }
}
