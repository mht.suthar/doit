package myapp.com.doit.helper;

import com.google.gson.annotations.SerializedName;

/**
 * Created by zero on 11/11/16.
 */
public class Service_List {

    @SerializedName("EnqNo")
    private String EnqNo;

    @SerializedName("Enqdate")
    private String Enqdate;

    @SerializedName("ItemDesc")
    private String ItemDesc;


    public String getEnqNo() {
        return EnqNo;
    }

    public void setEnqNo(String enqNo) {
        EnqNo = enqNo;
    }

    public String getEnqdate() {
        return Enqdate;
    }

    public void setEnqdate(String enqdate) {
        Enqdate = enqdate;
    }

    public String getItemDesc() {
        return ItemDesc;
    }

    public void setItemDesc(String itemDesc) {
        ItemDesc = itemDesc;
    }
}
