package myapp.com.doit.helper;

/**
 * Created by zero on 11/11/16.
 */
public class Mslist_List {

    private String ItemID;
    private String ItemDesc;

    public Mslist_List(String itemID, String itemDesc) {
        ItemID = itemID;
        ItemDesc = itemDesc;
    }

    public String getItemID() {
        return ItemID;
    }

    public void setItemID(String itemID) {
        ItemID = itemID;
    }

    public String getItemDesc() {
        return ItemDesc;
    }

    public void setItemDesc(String itemDesc) {
        ItemDesc = itemDesc;
    }
}
