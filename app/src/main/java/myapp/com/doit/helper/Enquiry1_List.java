package myapp.com.doit.helper;

/**
 * Created by zero on 11/11/16.
 */
public class Enquiry1_List {


    private String EnqId;
    private String CustName;
    private String EnqDesc;
    private String EnqDate;
    private String EnqNo;
    private String Enquiry;
    private String Role_name;
    private String IsApproved_YN;


    public Enquiry1_List(String enqId, String custName, String enqDesc, String enqDate, String enqNo, String enquiry, String role_name, String isApproved_YN) {
        EnqId = enqId;
        CustName = custName;
        EnqDesc = enqDesc;
        EnqDate = enqDate;
        EnqNo = enqNo;
        Enquiry = enquiry;
        Role_name = role_name;
        IsApproved_YN = isApproved_YN;
    }


    public String getEnqId() {
        return EnqId;
    }

    public void setEnqId(String enqId) {
        EnqId = enqId;
    }

    public String getCustName() {
        return CustName;
    }

    public void setCustName(String custName) {
        CustName = custName;
    }

    public String getEnqDesc() {
        return EnqDesc;
    }

    public void setEnqDesc(String enqDesc) {
        EnqDesc = enqDesc;
    }

    public String getEnqDate() {
        return EnqDate;
    }

    public void setEnqDate(String enqDate) {
        EnqDate = enqDate;
    }

    public String getEnqNo() {
        return EnqNo;
    }

    public void setEnqNo(String enqNo) {
        EnqNo = enqNo;
    }

    public String getEnquiry() {
        return Enquiry;
    }

    public void setEnquiry(String enquiry) {
        Enquiry = enquiry;
    }

    public String getRole_name() {
        return Role_name;
    }

    public void setRole_name(String role_name) {
        Role_name = role_name;
    }

    public String getIsApproved_YN() {
        return IsApproved_YN;
    }

    public void setIsApproved_YN(String isApproved_YN) {
        IsApproved_YN = isApproved_YN;
    }
}
