package myapp.com.doit.rest;

import com.squareup.okhttp.RequestBody;

import java.util.Map;

import myapp.com.doit.helper.LoginData;
import retrofit.Callback;
import retrofit.client.Response;
import retrofit.http.Body;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.GET;
import retrofit.http.Headers;
import retrofit.http.Multipart;
import retrofit.http.POST;
import retrofit.http.Part;
import retrofit.http.PartMap;
import retrofit.http.Query;
import retrofit.mime.TypedFile;

/**
 * Created by gautam on 5/24/2016.
 */
public interface RetrfitInterface {

    String url = "http://apicrm.doitimpex.com:89/api";
    //String url = "http://apicrm.myprod.co.in/api";


    @Headers("Content-Type: application/json")
    @POST("/UserProfile/Login")
    void Get_normalLogin(@Body LoginData body, Callback<Response> callback);

    @GET("/Dashboard/GetServiceCount")
    void Get_ServiceCount(@Query("EmployeeIDF") String EmployeeIDF, Callback<Response> callback);

    @GET("/Dashboard/GetServiceList")
    void Get_ServiceList(@Query("EmployeeIDF") String EmployeeIDF, Callback<Response> callback);

    @GET("/Dashboard/GetComplaintCount")
    void Get_GetComplaintCount(@Query("EmployeeIDF") String EmployeeIDF, Callback<Response> callback);

    @GET("/Dashboard/GetComplaintList")
    void Get_ComplaintList(@Query("EmployeeIDF") String EmployeeIDF, Callback<Response> callback);

    @GET("/Dashboard/GetOpenCallsCount")
    void Get_GetOpenCallsCount(@Query("EmployeeIDF") String EmployeeIDF,@Query("RoleID") String RoleID, Callback<Response> callback);

    @GET("/Dashboard/GetOpenCallsList")
    void Get_OpenCallsList(@Query("EmployeeIDF") String EmployeeIDF,@Query("RoleID") String RoleID, Callback<Response> callback);

    @GET("/Dashboard/GetCloseCallsCount")
    void GetCloseCallsCount(@Query("EmployeeIDF") String EmployeeIDF,@Query("RoleID") String RoleID, Callback<Response> callback);

    @GET("/Dashboard/GetCloseCallsList")
    void Get_CloseCallsList(@Query("EmployeeIDF") String EmployeeIDF,@Query("RoleID") String RoleID, Callback<Response> callback);

    @GET("/Notification/GetNotificationCount")
    void Get_NotificationCount(@Query("EmployeeIDF") String EmployeeIDF, Callback<Response> callback);

    @GET("/Dashboard/GetNotificationList")
    void Get_NotificationList(@Query("EmployeeIDF") String EmployeeIDF, Callback<Response> callback);

    @GET("/UserProfile/MyProfile")
    void Get_MyProfile(@Query("EmployeeIDF") String EmployeeIDF, Callback<Response> callback);

    @GET("/Enquiry/GetCallReportList")
    void Get_CallReportList(@Query("EmployeeID") String EmployeeID, Callback<Response> callback);

    @GET("/Dashboard/GetAMCWarrantyList")
    void Get_AMCWarrantyList(@Query("EmployeeIDF") String EmployeeIDF, Callback<Response> callback);

    @FormUrlEncoded
    @POST("/Customer/SaveFeedback")
    void Get_Send_feedback(@Field("CallRepid") String CallRepid, @Field("CustomerRemark") String CustomerRemark, @Field("EngineerRating") String EngineerRating, Callback<Response> callback);

    @GET("/DoItTour/GetFAQList")
    void Get_DoItTour(Callback<Response> callback);

    @GET("/Enquiry/Get")
    void Get_Enquiry_get(@Query("Enqno") String Enqno, @Query("RoleID") String RoleID, @Query("EmployeeID") String EmployeeID, Callback<Response> callback);

    @GET("/Enquiry/GetCustList")
    void Get_GetCustList(@Query("EmployeeID") String EmployeeID, @Query("RoleID") String RoleID, Callback<Response> callback);

    @GET("/Enquiry/GetRMList")
    void Get_RMList(Callback<Response> callback);

    @GET("/Enquiry/GetEnquiryTypeList")
    void Get_EnquiryTypeList(Callback<Response> callback);

    @GET("/Enquiry/GetItemMstList")
    void Get_ItemMstList(@Query("custId") String custId, @Query("EnquiryType") String EnquiryType, Callback<Response> callback);

    @GET("/Enquiry/GetMachineDetailList")
    void Get_ItemMsDetail(@Query("ItemId") String ItemId, @Query("CustID") String CustID, @Query("Enqtype") String Enqtype, Callback<Response> callback);

    @GET("/Enquiry/GetProblemTypeList")
    void Get_ProblemTypeList(Callback<Response> callback);

    @GET("/Enquiry/GetMechanicalList")
    void Get_GetMechanicalList(Callback<Response> callback);

    @GET("/Enquiry/GetChemicalList")
    void Get_ChemicalList(Callback<Response> callback);

    @FormUrlEncoded
    @POST("/Enquiry/SaveEnquiry")
    void Get_SaveEnquiry(@Field("CustId") String CustId, @Field("EnqDesc") String EnqDesc, @Field("RMId") String RMId, @Field("EnquiryType") String EnquiryType, @Field("ProblemType") String ProblemType,@Field("ProblemId") String ProblemId,@Field("FAQID") String FAQID,@Field("ConfirmType") String ConfirmType,@Field("Created_By") String Created_By,@Field("Role_ID") String Role_ID, Callback<Response> callback);

    @Multipart
    @POST("/Enquiry/SaveEnquiry")
    void saveEnquiry(@Part("CustId") String CustId, @Part("EnqDesc") String EnqDesc, @Part("RMId") String RMId,
                     @Part("EnquiryType") String EnquiryType, @Part("ProblemType") String ProblemType,
                     @Part("ProblemId") String ProblemId, @Part("FAQID") String FAQID, @Part("ConfirmType") String ConfirmType,
                     @Part("Created_By") String Created_By, @Part("Role_ID") String Role_ID, @Part("ItemId") String itemId,
                     @Part("myfile") TypedFile file, Callback<Response> callback);

    @Multipart
    @POST("/Enquiry/SaveEnquiry")
    void saveEnquiry(@PartMap Map<String, RequestBody> params, Callback<Response> callback);

    @GET("/Enquiry/GetRoleList")
    void Get_RoleList(Callback<Response> callback);

    @GET("/Enquiry/GetUserRoleList")
    void GetUserRoleList(@Query("UserName") String userName, Callback<Response> callback);

    @GET("/Enquiry/AddEnquiry")
    void GetCustomerDetail(@Query("EmployeeID") String EmployeeID, @Query("RoleID") String RoleID, Callback<Response> callback);

    @GET("/Dashboard/GetCallStatusReportList")
    void GetCallststus(@Query("FromDate") String FromDate, @Query("ToDate") String ToDate, @Query("EmployeeIDF") String EmployeeIDF, Callback<Response> callback);

    @GET("/Dashboard/GetOpenCallsCount")
    void GetOpenCallCount(@Query("EmployeeIDF") String EmployeeID, @Query("RoleID") String RoleID, Callback<Response> callback);

    @GET("/Dashboard/GetOpenCallsList")
    void GetOpenCallList(@Query("EmployeeIDF") String EmployeeID, @Query("RoleID") String RoleID, Callback<Response> callback);

    @GET("/Dashboard/GetEngCallAsignCount")
    void GetEngCallAsignCount(@Query("EmployeeIDF") String EmployeeID, @Query("RoleID") String RoleID, Callback<Response> callback);

    @GET("/Dashboard/GetEngCallAssignList")
    void GetEngCallAssignList(@Query("EmployeeIDF") String EmployeeID, @Query("RoleID") String RoleID, Callback<Response> callback);

    @GET("/Dashboard/GetPendingSparePartCount")
    void GetPendingSparePartCount(@Query("EmployeeIDF") String EmployeeID, @Query("RoleID") String RoleID, Callback<Response> callback);

    @GET("/Dashboard/GetCallSparePartList")
    void GetCallSparePartList(@Query("EmployeeIDF") String EmployeeID, @Query("RoleID") String RoleID, Callback<Response> callback);

    @GET("/Dashboard/GetPendingNextLevelCount")
    void GetPendingNextLevelCount(@Query("EmployeeIDF") String EmployeeID, @Query("RoleID") String RoleID, Callback<Response> callback);

    @GET("/Dashboard/GetPendingNextLevelList")
    void GetPendingNextLevelList(@Query("EmployeeIDF") String EmployeeID, @Query("RoleID") String RoleID, Callback<Response> callback);

    @GET("/Dashboard/GetTotalCallAssignCount")
    void GetTotalCallAssignCount(@Query("EmployeeIDF") String EmployeeID, @Query("RoleID") String RoleID, Callback<Response> callback);

    @GET("/Dashboard/GetTotalCallAssignList")
    void GetTotalCallAssignList(@Query("EmployeeIDF") String EmployeeID, @Query("RoleID") String RoleID, Callback<Response> callback);

    @GET("/Dashboard/GetTotalCallResolveCount")
    void GetTotalCallResolveCount(@Query("EmployeeIDF") String EmployeeID, @Query("RoleID") String RoleID, Callback<Response> callback);

    @GET("/Dashboard/GetTotalCallResolveList")
    void GetTotalCallResolveList(@Query("EmployeeIDF") String EmployeeID, @Query("RoleID") String RoleID, Callback<Response> callback);

    @GET("/Enquiry/GetCall")
    void GetCallEngineer(@Query("EngID") String EngID, @Query("EmployeeID") String EmployeeID, @Query("RoleID") String RoleID, Callback<Response> callback);

    @GET("/Call/GetUpdateCall")
    void GetUpdateCall(@Query("CallID") String callId, Callback<Response> callback);

    @GET("/Enquiry/GetPendingReasonList")
    void GetPendingReasonList(Callback<Response> callback);

    @GET("/Enquiry/GetCallStatusDropDownList")
    void GetCallStatusDropDownList(Callback<Response> callback);

    @Multipart
    @POST("/CallReport/SaveCallUpdate")
    void saveCallUpdate(@Part("Call_Status_Id") String Call_Status_Id, @Part("EnquiryType") String EnquiryType, @Part("CallId") String CallId,
                     @Part("EngRemark") String EngRemark, @Part("UniqueCode") String UniqueCode,
                     @Part("CallRemark") String CallRemark, @Part("SerNo") String SerNo, @Part("WarrFromDate") String WarrFromDate,
                     @Part("WarrToDate") String WarrToDate, @Part("NextDueDt") String NextDueDt, @Part("Pending_Id") String Pending_Id,
                        @Part("PendingReason") String PendingReason,  @Part("IsNextLevel") String IsNextLevel,
                        @Part("Total_Amount") String Total_Amount,  @Part("RoleId") String RoleId,
                        @Part("myfile") TypedFile file, Callback<Response> callback);

}
