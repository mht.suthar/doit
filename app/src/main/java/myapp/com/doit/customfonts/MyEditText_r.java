package myapp.com.doit.customfonts;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.EditText;

public class MyEditText_r extends EditText {

    public MyEditText_r(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public MyEditText_r(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public MyEditText_r(Context context) {
        super(context);
        init();
    }

    private void init() {
        if (!isInEditMode()) {
            Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "fonts/Moby_Begonia.ttf");
            setTypeface(tf);
        }
    }

}