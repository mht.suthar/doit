package myapp.com.doit.customfonts;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;


public class MyTextView_r extends TextView {

    public MyTextView_r(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public MyTextView_r(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public MyTextView_r(Context context) {
        super(context);
        init();
    }

    private void init() {
        if (!isInEditMode()) {
            Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "fonts/Moby_Begonia.ttf");
            setTypeface(tf);
        }
    }

}